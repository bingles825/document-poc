# Document Management POC

Document Management POC

## Local Dev

- Configure `packages\app\src\bootstrap.tsx` to point to appropriate API urls
- Run build in watch mode `yarn wsapp build -w`
- To run Storybook `yarn storybook`
- To run app `yarn start`

## Vendor Document Data + ETL

Possible document types will be

- Chart Note
- Consent Form
- Hosp Consult
- Lab/Study/Test Reports
- Misc Correspondence
- EEG
- EKG
- External Medical Records
- Rx Refill
- Pathology Report
- CT Scan
- Allergy Testing

A 3rd party vendor will be providing PDF documents + .json files containing meta data that has been parsed from the documents via OCR / NLP. The json format will look something like:

```json
[
  {
    "documentId": "98765",
    "type": "documentType",
    "value": "Lab"
  },
  {
    "documentId": "98765",
    "type": "DOB",
    "value": "2008-03-01T00:00:00"
  },
  {
    "documentId": "98765",
    "type": "DOS",
    "value": "2020-03-01T00:00:00"
  },
  {
    "documentId": "98765",
    "type": "labName",
    "value": "Hemoglobin A1C"
  }
]
```

Possible `type` values will be:

```text
"documentType"
"DOB"
"DOS"
"labName"
"MRN"
"orderDate"
"orderingProvider"
"patientName"
"practiceName"
"referringProvider"
"resultDate"
"uncategorized"
```

Each `type` should correspond to a `MetaType` entity which we will have defined in our database:

```typescript
interface MetaType {
  id: string;
  name: string;
  dataType: DataType;
}
```

We will define the `MetaType` data in our db with the following names:

- Document Type
- DOB
- DOS
- Lab name
- MRN Number
- Order date
- Ordering Provider
- Patient name
- Practice name
- Referring Provider
- Rendering Provider
- Result date
- Uncategorized

The `dataType` field will just contain a string that can be one of the following:

```typescript
type DataType =
  | 'doctype'
  | 'mrn'
  | 'date'
  | 'number'
  | 'text'
  | 'provider'
  | 'location';
```

Whatever ETL process imports the `.json` files will need to be able to map each object to the appropriate `MetaType.id` and create `Meta` entries in the database.

The contract for `Meta` objects looks like:

```typescript
/**
 * There will be a list of Meta objects associated with a document.
 * We'll be receiving a JSON array containing objects like:
 * { documentId: string, type: string, value: string }
 * These will get copied into our database mapping "type" to our MetaType.id
 * values.
 */
interface Meta {
  id: string;
  documentId: string;
  typeId: string;
  value: string;
  score: number;
  isFlagged?: boolean;
}
```

We will also need a configuration object that can tell us some ids for particular `MetaType` entries in the db. Specficially we need a list of "suggested" types. We also need to know the id of the "Uncategorized" type.

The config object should look like this:

```typescript
/** Configuration for our meta types */
export interface MetaTypeConfig {
  uncategorizedTypeId: string;
  suggestedTypeIds: string[];
  types: MetaType[];
}
```

## REST API

```typescript
/** API Contracts **/

interface MetaTypeConfigAPI {
  /** This gives us all of the available MetaTypes + ids for suggested and uncategorized */
  get(): MetaTypeConfig;
}

/**
 * Document meta data API. This data will be for real patients so will need
 * to be Hipaa compliant.
 */
interface MetaAPI {
  /** Get meta data for a document */
  get(args: { documentId: string }): Promise<Meta[]>;

  /** Save meta data for a document */
  save(meta: Meta): Promise<Meta>;
}

/**
 * TODO: We need a way to serve the PDF documents. These will be for real patients,
 * so we have to do this in a Hipaa compliant way.
 * There's also probably some additional document data we need to store in the db.
 */
interface DocumentAPI {}

/**
 * TBD: Since the remaining APIs won't contain PHI, we may be able to mock some
 * of the data in the front-end
 * */

/** TBD: API for rules engine that assigns documents to work queues */
interface WorkQueueRulesAPI {}

/** TBD: API for assigning documents to workers + providers */
interface WorkQueueAPI {}

/** TBD: API for listing and creating users */
interface UserAPI {}
```

## Document Grid Columns

There will be a data grid that shows the list of all documents + assignments.

TBD: Ahmed sent this list for the grid, but not sure where this data will come from?

- Scan Date
- Filed Date
- Location
- Provider
- Specialty
- Document Number
- Batch Number
- Batch Description
- Document type
