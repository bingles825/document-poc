import {
  apiDecorator,
  loadingOverlayDecorator,
  memoryRouterDecorator,
  storyStylesDecorator,
  themeDecorator,
  userSessionDecorator,
} from '../src/decorators';

import '@fontsource/nunito-sans';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    // Disable controls by default
    disable: true,
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  // Hide actions panel by default
  options: { showPanel: false },
};

if (typeof global.process === 'undefined') {
  console.log('Clearing localStorage and starting mock service worker...');
  localStorage.clear();
  const { start } = require('../../app/src/mocks/browser');
  start();
}

export const decorators = [
  apiDecorator,
  memoryRouterDecorator,
  storyStylesDecorator,
  themeDecorator,
  loadingOverlayDecorator,
  userSessionDecorator,
];
