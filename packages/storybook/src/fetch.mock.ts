import { timeout } from '@wsh/core';

/**
 * Util for mocking an async fetch call with a given timeout.
 */
export async function mockFetch<T>(
  data: T,
  timeoutMs = 100 as number,
): Promise<T> {
  await timeout(timeoutMs);
  return data;
}
