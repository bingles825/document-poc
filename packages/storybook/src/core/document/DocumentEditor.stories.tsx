import { Story, Meta } from '@storybook/react';
import { defaultAPIRoot, DocumentEditor, DocumentEditorProps } from '@wsh/core';

export default {
  title: 'Core/Document/DocumentEditor',
  component: DocumentEditor,
  parameters: {
    options: {
      storyStyles: {
        display: 'flex',
        height: '100vh',
        padding: 0,
      },
    },
  },
} as Meta;

const Template: Story<DocumentEditorProps> = (args) => (
  <DocumentEditor {...args} />
);

export const DocumentEditorDefault = Template.bind({});
DocumentEditorDefault.args = {
  documentPath: `${defaultAPIRoot}/GetPDF/1`,
};
