export function randomName(): string {
  const i = Math.floor(Math.random() * names.length);
  return names[i];
}

const names = [
  'John',
  'Jones',
  'Smith',
  'Jenny',
  'Jeffery',
  'Ahmed',
  'Bill',
  'Lexi',
];
