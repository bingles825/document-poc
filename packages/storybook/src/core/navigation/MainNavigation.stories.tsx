import { Story, Meta } from '@storybook/react';
import { MainNavigation, MainNavigationProps } from '@wsh/core';

export default {
  title: 'Core/Navigation/MainNavigation',
  component: MainNavigation,
} as Meta;

const props: MainNavigationProps = {
  itemProps: [
    {
      icon: 'mainnavhome',
      label: 'Home',
      to: '/',
      exact: true,
    },
    {
      icon: 'mainnavnodesetup',
      label: 'Config',
      to: '/config',
    },
  ],
};

const Template: Story<MainNavigationProps> = (args) => (
  <MainNavigation {...args} />
);

export const MainNavigationDefault = Template.bind({});
MainNavigationDefault.args = {
  ...props,
};
