import React from 'react';
import { Story, Meta } from '@storybook/react';
import { Route, useHistory } from 'react-router-dom';
import { LoginContainer, LoginContainerProps, LOGIN_URL } from '@wsh/core';

export default {
  title: 'Core/Login/LoginContainer',
  component: LoginContainer,
} as Meta;

const defaultProps: LoginContainerProps = {};

const Template: Story<LoginContainerProps> = (args) => {
  const history = useHistory();

  React.useEffect(() => {
    history.push(LOGIN_URL);
  }, [history]);

  return (
    <>
      <Route path="/" exact>
        Welcome!
      </Route>
      <Route path={LOGIN_URL}>
        <LoginContainer {...args} />
      </Route>
    </>
  );
};

export const LoginContainerDefault = Template.bind({});
LoginContainerDefault.args = {
  ...defaultProps,
};
