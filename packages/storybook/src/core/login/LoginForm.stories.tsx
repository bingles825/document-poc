import { Story, Meta } from '@storybook/react';
import { LoginForm, LoginFormProps } from '@wsh/core';

export default {
  title: 'Core/Login/LoginForm',
  component: LoginForm,
} as Meta;

const Template: Story<LoginFormProps> = (args) => <LoginForm {...args} />;

export const LoginFormDefault = Template.bind({});
LoginFormDefault.args = {
  onSubmit: async ({
    username,
    password,
    // eslint-disable-next-line @typescript-eslint/require-await
  }): Promise<true | { message: string }> => {
    if (username === 'johndoe' && password === 'pwd') {
      return true;
    }

    return { message: 'Invalid Login or Password' };
  },
};
