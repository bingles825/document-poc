import { Story, Meta } from '@storybook/react';
import { RuleEditorDialog, RuleEditorDialogProps } from '@wsh/core';
import { mockRules } from '../../../../app/src/mocks/rule.mock';

export default {
  title: 'Core/Rules/Rule Editor Dialog',
  component: RuleEditorDialog,
} as Meta;

const [rule] = mockRules();

const defaultProps: RuleEditorDialogProps = {
  ruleSetId: 'mock-ruleset-id',
  value: rule,
  onSave: console.log,
  onCancel: console.log,
};

const Template: Story<RuleEditorDialogProps> = (args) => (
  <RuleEditorDialog {...args} />
);

export const RuleEditorDefault = Template.bind({});
RuleEditorDefault.args = {
  ...defaultProps,
};
