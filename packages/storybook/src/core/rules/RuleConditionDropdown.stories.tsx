import { Story, Meta } from '@storybook/react';
import { RuleConditionDropdown, RuleConditionDropdownProps } from '@wsh/core';
import {
  mockRuleConditionOperands,
  mockRuleConditionOperators,
  mockRuleConditionValues,
} from '../../../../app/src/mocks/rule.mock';

export default {
  title: 'Core/Rules/RuleConditionDropdown',
  component: RuleConditionDropdown,
} as Meta;

const Template: Story<RuleConditionDropdownProps> = (args) => (
  <RuleConditionDropdown {...args} />
);

const operands = mockRuleConditionOperands();
const operators = mockRuleConditionOperators();
const values = mockRuleConditionValues('1', '');

export const RuleConditionDropdownOperand = Template.bind({});
RuleConditionDropdownOperand.args = {
  variant: 'operand',
  items: operands.map(({ id, display }) => ({
    value: id,
    display,
  })),
  value: operands[0]?.id,
  onChange: console.log,
};

export const RuleConditionDropdownOperator = Template.bind({});
RuleConditionDropdownOperator.args = {
  variant: 'operator',
  items: operators.map(({ id, display }) => ({
    value: id,
    display,
  })),
  value: operators[0]?.id,
  onChange: console.log,
};

export const RuleConditionDropdownValue = Template.bind({});
RuleConditionDropdownValue.args = {
  variant: 'value',
  items: values.map((value) => ({
    value,
    display: value,
  })),
  value: values[0],
  onChange: console.log,
};
