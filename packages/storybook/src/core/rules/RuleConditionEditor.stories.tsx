import { Story, Meta } from '@storybook/react';
import { RuleConditionEditor, RuleConditionEditorProps } from '@wsh/core';

export default {
  title: 'Core/Rules/Rule Condition Editor',
  component: RuleConditionEditor,
} as Meta;

const defaultProps: RuleConditionEditorProps = {
  ruleSetId: 'mock-ruleset-id',
  value: {
    operand: '1',
    operator: '1',
    value: '1',
  },
  onChange: console.log,
};

const Template: Story<RuleConditionEditorProps> = (args) => (
  <RuleConditionEditor {...args} />
);

export const RuleConditionEditorDefault = Template.bind({});
RuleConditionEditorDefault.args = {
  ...defaultProps,
};
