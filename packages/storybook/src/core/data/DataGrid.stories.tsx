import { Story, Meta } from '@storybook/react';
import { DataGrid, DataGridProps, range } from '@wsh/core';
import { randomName } from '../platform.mock';

export default {
  title: 'Core/Data/DataGrid',
  component: DataGrid,
} as Meta;

interface PersonData {
  id: number;
  firstName: string;
  lastName: string;
}

const ids = [...range(1, 100)];

const args: DataGridProps<PersonData> = {
  columns: [
    {
      field: 'firstName',
      display: 'First Name',
    },
    {
      field: 'lastName',
      display: 'Last Name',
    },
  ],
  data: ids.map((id) => ({
    id,
    firstName: randomName(),
    lastName: randomName(),
  })),
};

const Template: Story<DataGridProps<PersonData>> = (args) => (
  <DataGrid {...args} />
);

export const DataGridDefault = Template.bind({});
DataGridDefault.args = args;

export const DataGridSelectable = Template.bind({});
DataGridSelectable.args = {
  ...args,
  enableRowSelection: true,
};

export const DataGridCheckboxColumn = Template.bind({});
DataGridCheckboxColumn.args = {
  ...args,
  enableRowSelection: true,
  showCheckboxColumn: true,
};
