import { Story, Meta } from '@storybook/react';
import {
  ButtonNormal,
  LoadingOverlay,
  LoadingOverlayContainer,
  LoadingOverlayProps,
  useLoadingOverlay,
} from '@wsh/core';

export default {
  title: 'Core/Data/LoadingOverlay',
  component: LoadingOverlay,
  parameters: {
    options: {
      storyStyles: {
        height: '150vh',
        padding: 0,
      },
    },
  },
} as Meta;

const Template: Story<LoadingOverlayProps> = (...args) => (
  <LoadingOverlay {...args} />
);

export const LoadingOverlayDefault = Template.bind({});

export const LoadingOverlayContainerDefault: Story = () => {
  const { setIsLoading } = useLoadingOverlay();

  return (
    <div>
      <ButtonNormal onClick={() => setIsLoading(true)}>
        Show Overlay
      </ButtonNormal>
      <LoadingOverlayContainer />
    </div>
  );
};
