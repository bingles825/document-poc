import { Story, Meta } from '@storybook/react';
import { LoadingSpinner, LoadingSpinnerProps } from '@wsh/core';

export default {
  title: 'Core/Data/LoadingSpinner',
  component: LoadingSpinner,
} as Meta;

const defaultProps: LoadingSpinnerProps = {};

const Template: Story<LoadingSpinnerProps> = (args) => (
  <LoadingSpinner {...args} />
);

export const LoadingSpinnerDefault = Template.bind({});
LoadingSpinnerDefault.args = {
  ...defaultProps,
};
