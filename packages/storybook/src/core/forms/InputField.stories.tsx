import { Story, Meta } from '@storybook/react';
import { InputField, InputFieldProps } from '@wsh/core';

export default {
  title: 'Core/Forms/InputField',
  component: InputField,
} as Meta;

const Template: Story<InputFieldProps> = (args) => <InputField {...args} />;

export const InputDefault = Template.bind({});
InputDefault.args = {
  label: 'First Name',
  placeholder: 'Enter text...',
  defaultValue: 'Test',
};

export const InputError = Template.bind({});
InputError.args = {
  label: 'First Name',
  placeholder: 'Enter text...',
  defaultValue: 'Test',
  errorText: 'Some Error',
};
