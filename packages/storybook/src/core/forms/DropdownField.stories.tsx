import { Story, Meta } from '@storybook/react';
import { DropdownField, DropdownFieldProps } from '@wsh/core';

export default {
  title: 'Core/Forms/DropdownField',
  component: DropdownField,
} as Meta;

const Template: Story<DropdownFieldProps> = (args) => (
  <DropdownField {...args} />
);

export const DropdownFieldDefault = Template.bind({});
DropdownFieldDefault.args = {
  label: 'Ages',
  value: 10,
  items: [
    { value: '', display: <em>None</em> },
    { value: 10, display: 'Ten' },
    { value: 20, display: 'Twenty' },
    { value: 30, display: 'Thirty' },
  ],
};
