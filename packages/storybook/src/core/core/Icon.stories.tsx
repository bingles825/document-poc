import { Meta } from '@storybook/react';
import { makeStyles } from '@material-ui/core/styles';
import { Icon, IconID, iconMap } from '@wsh/core';

export default {
  title: 'Core/Core/Icon',
  component: Icon,
} as Meta;

const useStyles = makeStyles({
  labeledIcon: {
    display: 'flex',
    alignItems: 'center',
    padding: '4px 0',
    '& :not(:first-child)': {
      marginLeft: 6,
    },
  },
});

export const IconsShowcase = (): JSX.Element => {
  const classes = useStyles();
  const iconIds = Object.keys(iconMap).sort() as IconID[];

  return (
    <div>
      {iconIds.map((id) => (
        <div className={classes.labeledIcon} key={id}>
          <Icon icon={id} size={24} title={id} onClick={console.log} />
          <Icon icon={id} size={24} title={id} flipH onClick={console.log} />
          <span>{id}</span>
        </div>
      ))}
    </div>
  );
};
