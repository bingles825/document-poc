import React from 'react';
import { Story, Meta } from '@storybook/react';
import { makeStyles } from '@material-ui/core/styles';
import { ButtonNormal, useAlert, AlertProps } from '@wsh/core';

export default {
  title: 'Core/Core/Alert',
} as Meta;

const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexDirection: 'row',
    gap: 4,
  },
});

const Template: Story = () => {
  const classes = useStyles();

  const alerts = {
    success: useAlert(),
    info: useAlert(),
    warning: useAlert(),
    danger: useAlert(),
  };

  return (
    <div className={classes.root}>
      {Object.entries(alerts).map(([key, value]) => {
        return (
          <React.Fragment key={key}>
            <ButtonNormal onClick={() => value.showAlert()}>{key}</ButtonNormal>
            <value.Alert level={key as AlertProps['level']} message={key} />
          </React.Fragment>
        );
      })}
    </div>
  );
};

export const AlertDefault = Template.bind({});
AlertDefault.args = {};
