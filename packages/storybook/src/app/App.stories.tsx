import { Story, Meta } from '@storybook/react';
import { App, AppProps } from '@wsh/app';

export default {
  title: 'App',
  component: App,
  parameters: {
    options: {
      storyStyles: {
        padding: 0,
      },
    },
  },
} as Meta;

const Template: Story<AppProps> = (args) => <App {...args} />;

export const AppDefault = Template.bind({});
