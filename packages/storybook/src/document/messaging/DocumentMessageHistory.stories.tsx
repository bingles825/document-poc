import { Story, Meta } from '@storybook/react';
import addDays from 'date-fns/fp/addDays';
import { makeStyles } from '@material-ui/core/styles';
import {
  DocumentMessageHistory,
  DocumentMessageHistoryProps,
} from '@wsh/document';

export default {
  title: 'Document/Messaging/Document Message History',
  component: DocumentMessageHistory,
} as Meta;

const defaultProps: DocumentMessageHistoryProps = {
  messages: [
    {
      id: '1',
      documentId: '9',
      subject: 'Review Flagged Fields',
      toId: '10',
      fromId: '11',
      content:
        "Dr. Jones, please review the flagged fields. I'm not sure if they are correct or not. Thanks!",
      createdAt: addDays(-1, new Date()).toISOString(),
    },
    {
      id: '2',
      documentId: '9',
      subject: 'Complete',
      toId: '11',
      fromId: '10',
      content: 'Everything looks good to me.',
      createdAt: new Date().toISOString(),
    },
  ],
};

const useStyles = makeStyles({
  root: {
    width: 600,
  },
});

const Template: Story<DocumentMessageHistoryProps> = (args) => {
  const classes = useStyles();
  return <DocumentMessageHistory className={classes.root} {...args} />;
};

export const DocumentMessageHistoryDefault = Template.bind({});
DocumentMessageHistoryDefault.args = {
  ...defaultProps,
};
