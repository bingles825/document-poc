import { Story, Meta } from '@storybook/react';
import { makeStyles } from '@material-ui/core/styles';
import {
  ProcessingCenterContainer,
  ProcessingCenterContainerProps,
} from '@wsh/document';

export default {
  title: 'Document/Main/ProcessingCenterContainer',
  component: ProcessingCenterContainer,
  parameters: {
    options: {
      storyStyles: {
        display: 'flex',
        height: '100vh',
        padding: 0,
      },
    },
  },
} as Meta;

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
});

const Template: Story<ProcessingCenterContainerProps> = (args) => {
  const classes = useStyles();
  return <ProcessingCenterContainer className={classes.root} {...args} />;
};

export const ProcessingCenterContainerDefault = Template.bind({});
