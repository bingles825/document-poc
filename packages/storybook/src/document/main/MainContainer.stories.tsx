import { Story, Meta } from '@storybook/react';
import { makeStyles } from '@material-ui/core/styles';
import { MainContainer, MainContainerProps } from '@wsh/document';

export default {
  title: 'Document/Main/MainContainer',
  component: MainContainer,
  parameters: {
    options: {
      storyStyles: {
        display: 'flex',
        height: '100vh',
        padding: 0,
      },
    },
  },
} as Meta;

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
});

const Template: Story<MainContainerProps> = (args) => {
  const classes = useStyles();
  return <MainContainer className={classes.root} {...args} />;
};

export const MainContainerDefault = Template.bind({});
