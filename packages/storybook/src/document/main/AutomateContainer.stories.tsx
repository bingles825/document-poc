import { Story, Meta } from '@storybook/react';
import { AutomateContainer, AutomateContainerProps } from '@wsh/document';

export default {
  title: 'Document/Main/AutomateContainer',
  component: AutomateContainer,
} as Meta;

const defaultProps: AutomateContainerProps = {};

const Template: Story<AutomateContainerProps> = (args) => (
  <AutomateContainer {...args} />
);

export const AutomateContainerDefault = Template.bind({});
AutomateContainerDefault.args = {
  ...defaultProps,
};
