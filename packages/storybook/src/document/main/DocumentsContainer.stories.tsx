import { Story, Meta } from '@storybook/react';
import { DocumentsContainer, DocumentsContainerProps } from '@wsh/document';

export default {
  title: 'Document/Main/DocumentsContainer',
  component: DocumentsContainer,
} as Meta;

const defaultProps: DocumentsContainerProps = {};

const Template: Story<DocumentsContainerProps> = (args) => (
  <DocumentsContainer {...args} />
);

export const DocumentsContainerDefault = Template.bind({});
DocumentsContainerDefault.args = {
  ...defaultProps,
};
