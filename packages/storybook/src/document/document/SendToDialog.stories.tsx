import { Story, Meta } from '@storybook/react';
import { ButtonNormal, useModal } from '@wsh/core';
import { SendToDialog, SendToDialogProps } from '@wsh/document';
import { mockUsers } from '../../../../app/src/mocks/user.mock';

export default {
  title: 'Document/Dialogs/SendTo Dialog',
  component: SendToDialog,
} as Meta;

const [mockUser] = [...mockUsers(1)];

const defaultProps: SendToDialogProps = {
  onSend: console.log,
  onCancel: console.log,
};

const Template: Story<SendToDialogProps> = (args) => <SendToDialog {...args} />;

export const SendToDialogDefault = Template.bind({});
SendToDialogDefault.args = {
  ...defaultProps,
};

export const SendToDialogReturn = Template.bind({});
SendToDialogReturn.args = {
  ...defaultProps,
  returnToId: mockUser.id,
};

export const SendToDialogModal: Story<SendToDialogProps> = (args) => {
  const { Modal, openModal } = useModal(SendToDialog, ['onCancel', 'onSend']);

  return (
    <>
      <ButtonNormal onClick={openModal}>Open</ButtonNormal>
      <Modal {...args} />
    </>
  );
};
