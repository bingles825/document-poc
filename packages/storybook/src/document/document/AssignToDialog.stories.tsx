import { Story, Meta } from '@storybook/react';
import { ButtonNormal, useModal, useSortedUserList } from '@wsh/core';
import { AssignToDialog, AssignToDialogProps } from '@wsh/document';

export default {
  title: 'Document/Dialogs/AssignToDialog',
  component: AssignToDialog,
} as Meta;

const defaultProps: AssignToDialogProps = {
  title: 'Assign Thing(s) to',
  onSend: console.log,
  onCancel: console.log,
};

const Template: Story<AssignToDialogProps> = (args) => (
  <AssignToDialog {...args} />
);

export const AssignToDialogDefault = Template.bind({});
AssignToDialogDefault.args = {
  ...defaultProps,
};

export const AssignToDialogSelectedAssignee: Story<AssignToDialogProps> = (
  args,
) => {
  const users = useSortedUserList();
  const notFirstUser = users[2];

  return <AssignToDialog {...args} selectedAssigneeId={notFirstUser?.id} />;
};

export const AssignToDialogModal: Story<AssignToDialogProps> = (args) => {
  const { Modal, openModal } = useModal(AssignToDialog, ['onCancel', 'onSend']);

  return (
    <>
      <ButtonNormal onClick={openModal}>Open</ButtonNormal>
      <Modal {...args} />
    </>
  );
};
