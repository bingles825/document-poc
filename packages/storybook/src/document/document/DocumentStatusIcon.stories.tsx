import { Story, Meta } from '@storybook/react';
import { numericEnumValues } from '@wsh/core';
import {
  DocumentStatus,
  DocumentStatusIcon,
  DocumentStatusIconProps,
} from '@wsh/document';

export default {
  title: 'Document/Document/DocumentStatusIcon',
  component: DocumentStatusIcon,
} as Meta;

const statuses = numericEnumValues(DocumentStatus);

const Template: Story<DocumentStatusIconProps> = () => {
  return (
    <div>
      {statuses.map((status) => (
        <DocumentStatusIcon key={status} status={status} />
      ))}
    </div>
  );
};
export const DocumentStatusIconDefault = Template.bind({});
