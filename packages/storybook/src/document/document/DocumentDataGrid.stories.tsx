import { Story, Meta } from '@storybook/react';
import { DocumentDataGrid, DocumentDataGridProps } from '@wsh/document';
import { mockDocuments } from '../../../../app/src/mocks/document.mock';

export default {
  title: 'Document/Document/Document DataGrid',
  component: DocumentDataGrid,
} as Meta;

const defaultProps: DocumentDataGridProps = {
  items: [...mockDocuments(100)],
  getUserDisplayName: (userId: string) =>
    userId ? `User ${userId}` : 'Unassigned',
  onAssignDocuments: console.log,
};

const Template: Story<DocumentDataGridProps> = (args) => {
  return <DocumentDataGrid {...args} />;
};

export const DocumentDataGridDefault = Template.bind({});
DocumentDataGridDefault.args = {
  ...defaultProps,
};
