import { Story, Meta } from '@storybook/react';
import { timeout } from '@wsh/core';
import {
  DocumentMetaEditor,
  DocumentMetaEditorProps,
  DocumentStatus,
} from '@wsh/document';
import { mockDocuments } from '../../../../app/src/mocks/document.mock';
import {
  mockMeta,
  mockMetaTypeConfig,
} from '../../../../app/src/mocks/meta.mock';
import { mockUsers } from '../../../../app/src/mocks/user.mock';

export default {
  title: 'Document/Document/Meta Editor',
  component: DocumentMetaEditor,
  parameters: {
    options: {
      storyStyles: {
        display: 'flex',
        height: '100vh',
        padding: 0,
      },
    },
  },
} as Meta;

const [document] = mockDocuments(1);
const [fromUser, toUser] = [...mockUsers(2)];

const defaultProps: DocumentMetaEditorProps = {
  config: mockMetaTypeConfig(),
  document,
  messages: [],
  meta: mockMeta,
  onCreateDocumentMessage: () => timeout(1000),
  onSaveDocument: () => timeout(1000),
  onSaveMeta: () => timeout(1000),
};

const Template: Story<DocumentMetaEditorProps> = (args) => (
  <DocumentMetaEditor {...args} />
);

export const DocumentMetaEditorDefault = Template.bind({});
DocumentMetaEditorDefault.args = {
  ...defaultProps,
};

export const DocumentMetaEditorInReview = Template.bind({});
DocumentMetaEditorInReview.args = {
  ...defaultProps,
  document: {
    ...document,
    status: DocumentStatus['Needs Review'],
  },
  messages: [
    {
      id: '1',
      documentId: document.id,
      subject: 'Review Flagged Fields',
      toId: toUser.id,
      fromId: fromUser.id,
      content: 'Would you mind reviewing the flagged fields?',
      createdAt: new Date().toISOString(),
    },
  ],
};
