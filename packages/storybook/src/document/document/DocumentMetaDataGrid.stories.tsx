import { Story, Meta } from '@storybook/react';
import { useEditListState } from '@wsh/core';
import {
  buildMetaTypeMap,
  DocumentMetaDataGrid,
  DocumentMetaDataGridProps,
} from '@wsh/document';
import {
  mockMeta,
  mockMetaTypeConfig,
} from '../../../../app/src/mocks/meta.mock';

export default {
  title: 'Document/Document/Meta DataGrid',
  component: DocumentMetaDataGrid,
} as Meta;

const config = mockMetaTypeConfig();
const metaTypeMap = buildMetaTypeMap(config);
const meta = mockMeta;

const defaultProps: DocumentMetaDataGridProps = {
  meta,
  metaTypeMap,
  onMetaChanged: console.log,
};

const Template: Story<DocumentMetaDataGridProps> = ({ meta, ...args }) => {
  const { list, setItem } = useEditListState(meta);
  return <DocumentMetaDataGrid {...args} meta={list} onMetaChanged={setItem} />;
};

export const DocumentMetaDataGridDefault = Template.bind({});
DocumentMetaDataGridDefault.args = {
  ...defaultProps,
};

export const DocumentMetaDataGridReadonly = Template.bind({});
DocumentMetaDataGridReadonly.args = {
  ...defaultProps,
  isReadOnly: true,
};
