import { MemoryRouter } from 'react-router-dom';
import { makeStyles, ThemeProvider } from '@material-ui/core/styles';
import { StoryContext } from '@storybook/addons';
import {
  bodyText,
  composeProviders,
  defaultAPIRoot,
  LoadingOverlayContainer,
  LoadingOverlayContextProvider,
  RuleAPI,
  RuleAPIProvider,
  theme,
  UserAPI,
  UserAPIProvider,
  UserSessionContextProvider,
  UserSessionAPI,
  UserSessionService,
  UserSessionServiceProvider,
  ValuesType,
  defaultAuthAPIRoot,
} from '@wsh/core';
import {
  DocumentAPI,
  DocumentAPIProvider,
  DocumentMessageAPI,
  DocumentMessageAPIProvider,
  MetaAPI,
  MetaAPIProvider,
} from '@wsh/document';

const APIProvider = composeProviders(
  DocumentAPIProvider,
  DocumentMessageAPIProvider,
  MetaAPIProvider,
  RuleAPIProvider,
  UserAPIProvider,
  UserSessionServiceProvider,
);

const apiContextValues = [
  new DocumentAPI(defaultAPIRoot),
  new DocumentMessageAPI(defaultAPIRoot),
  new MetaAPI(defaultAPIRoot),
  new RuleAPI(defaultAPIRoot),
  new UserAPI(defaultAPIRoot),
  new UserSessionService(new UserSessionAPI(defaultAuthAPIRoot)),
] as ValuesType<typeof APIProvider>;

export const apiDecorator = (Story: () => JSX.Element): JSX.Element => (
  <APIProvider values={apiContextValues}>
    <Story />
    <LoadingOverlayContainer />
  </APIProvider>
);

export const loadingOverlayDecorator = (
  Story: () => JSX.Element,
): JSX.Element => (
  <LoadingOverlayContextProvider>
    <Story />
    <LoadingOverlayContainer />
  </LoadingOverlayContextProvider>
);

/**
 * This decorator allows us to control the container styles for stories. We are
 * changing the default body padding in preview-head to 0, and instead using that
 * as the default here. Stories can then explicitly set it via the
 * options.storyStyles parameter.
 */
export const storyStylesDecorator = (
  Story: () => JSX.Element,
  context: StoryContext,
): JSX.Element => {
  const storyStyles = context.parameters?.options?.storyStyles;

  return (
    <div style={storyStyles ?? { padding: '1rem' }}>
      <Story />
    </div>
  );
};

export const memoryRouterDecorator = (
  Story: () => JSX.Element,
): JSX.Element => (
  <MemoryRouter>
    <Story />
  </MemoryRouter>
);

// Use this to set global styles
const useStyles = makeStyles({
  '@global': {
    body: {
      ...bodyText(500),
    },
  },
});

export const themeDecorator = (Story: () => JSX.Element): JSX.Element => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  useStyles();

  return (
    <ThemeProvider theme={theme}>
      <Story />
    </ThemeProvider>
  );
};

export const userSessionDecorator = (Story: () => JSX.Element): JSX.Element => (
  <UserSessionContextProvider>
    <Story />
  </UserSessionContextProvider>
);
