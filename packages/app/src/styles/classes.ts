import { createClassName } from '@wsh/core';

/**
 * Create component css className string for app components.
 */
export const appClassName = createClassName('app');
