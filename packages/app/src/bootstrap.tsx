import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core';
import {
  composeProviders,
  defaultAPIRoot,
  defaultAuthAPIRoot,
  ensureSessionToken,
  getUserSessionToken,
  LoadingOverlayContextProvider,
  LOGIN_URL,
  RuleAPI,
  RuleAPIProvider,
  theme,
  UserAPI,
  UserAPIProvider,
  UserSessionAPI,
  UserSessionContextProvider,
  UserSessionService,
  UserSessionServiceProvider,
  ValuesType,
} from '@wsh/core';
import {
  DocumentAPI,
  DocumentAPIProvider,
  DocumentMessageAPI,
  DocumentMessageAPIProvider,
  MetaAPI,
  MetaAPIProvider,
} from '@wsh/document';
import { App } from '.';

import '@fontsource/nunito-sans';

const APIProvider = composeProviders(
  DocumentAPIProvider,
  DocumentMessageAPIProvider,
  MetaAPIProvider,
  RuleAPIProvider,
  UserAPIProvider,
  UserSessionServiceProvider,
);

const clientDataHost = 'https://wsc-devapp-doc-poc-api.azurewebsites.net';
// const clientDataHost = 'http://localhost:53236';
const clientDataAPIRoot = `${clientDataHost}${defaultAPIRoot}`;
const clientAuthAPIRoot = `${clientDataHost}${defaultAuthAPIRoot}`;
const mockAPIRoot = defaultAPIRoot;

const apiContextValues = [
  new DocumentAPI(clientDataAPIRoot),
  new DocumentMessageAPI(mockAPIRoot),
  new MetaAPI(clientDataAPIRoot),
  new RuleAPI(mockAPIRoot),
  new UserAPI(mockAPIRoot),
  new UserSessionService(new UserSessionAPI(clientAuthAPIRoot)),
] as ValuesType<typeof APIProvider>;

void import('./mocks/browser').then(async ({ start }) => {
  await start();

  ensureSessionToken();

  render(
    <Router>
      <ThemeProvider theme={theme}>
        <APIProvider values={apiContextValues}>
          <UserSessionContextProvider>
            <LoadingOverlayContextProvider>
              <App />
            </LoadingOverlayContextProvider>
          </UserSessionContextProvider>
        </APIProvider>
      </ThemeProvider>
    </Router>,
    document.getElementById('root'),
  );
});
