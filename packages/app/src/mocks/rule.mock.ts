import {
  range,
  RuleConditionOperand,
  RuleConditionOperator,
  RuleConditionValue,
  RuleConditionOperatorDisplay,
  withDisplayName,
  RuleAction,
  Rule,
  userDisplayName,
  RuleTarget,
} from '@wsh/core';
import { DocumentType, documentTypeOptions } from '@wsh/document';
import { isProvider, isTitle, mockUsers } from './user.mock';

const operands: RuleConditionOperand[] = [
  {
    id: '1',
    display: 'Document Type',
  },
  {
    id: '2',
    display: 'Rendering Provider',
  },
  {
    id: '3',
    display: 'Score',
  },
];

const operators: RuleConditionOperator[] = [
  '<',
  '>',
  '=',
  '!=',
  '>=',
  '<=',
  'is',
  'not',
  'contains',
].map((display, i) => ({
  id: String(i + 1),
  display: display as RuleConditionOperatorDisplay,
}));

// Map of operators by operand id
const operatorsMap: Record<
  RuleConditionOperand['id'],
  RuleConditionOperator[]
> = {
  1: operators.filter((op) => ['is', 'not'].includes(op.display)),
  2: operators.filter((op) => ['is', 'not'].includes(op.display)),
  3: operators.filter((op) => !/^[a-z]/.test(op.display)),
};

const users = [...mockUsers(20)].map(withDisplayName);
const providers = users.filter(isProvider);
const pas = users.filter(isTitle('PA'));
const mds = users.filter(isTitle('MD'));

// Map of values by operand id
const valuesMap: Record<RuleConditionOperand['id'], RuleConditionValue[]> = {
  // Document types
  1: documentTypeOptions.map(({ display, value }) => ({
    id: String(value),
    display,
  })),
  // Physicians (adding 100 to ids to keep distinct)
  2: providers.map(withDisplayName).map(({ id, displayName }) => ({
    id: String(Number(id) + 100),
    display: displayName,
  })),
  // Score values (adding 1,000 to ids to keep distinct)
  3: [...range('1', '100')].map((value) => ({
    id: String(Number(value) + 1000),
    display: value,
  })),
};

export function mockRuleConditionOperands(): RuleConditionOperand[] {
  return operands;
}

export function mockRuleConditionOperators(
  operandId: string,
): RuleConditionOperator[] {
  return operatorsMap[operandId] ?? operators;
}

export function mockRuleConditionValues(
  operandId: RuleConditionOperand['id'],
  _operatorId: RuleConditionOperator['id'],
): RuleConditionValue[] {
  return valuesMap[operandId] ?? [];
}

export function mockRuleActions(): RuleAction[] {
  return [
    {
      id: '1',
      display: 'Send as task to',
    },
    {
      id: '2',
      display: 'Send for processing to',
    },
    {
      id: '3',
      display: 'Send for Sign Off',
    },
    {
      id: '4',
      display: 'Send for PAQ Review',
    },
  ];
}

export function mockRuleTargets(actionId: string): RuleTarget[] {
  const userList = actionId === '3' ? pas : actionId === '4' ? mds : users;
  return userList.map(({ id, displayName }) => ({ id, display: displayName }));
}

export function mockRules(): Rule[] {
  const [operand1, operand2] = mockRuleConditionOperands();
  const [operator1] = mockRuleConditionOperators(operand1.id);
  const [operator2] = mockRuleConditionOperators(operand2.id);
  // const [value1] = mockRuleConditionValues(operand1.id, operator1.id);
  // const [value2] = mockRuleConditionValues(operand2.id, operator2.id);
  const [action1, action2] = mockRuleActions();
  const users = [...mockUsers(20)];
  const providers = users.filter(isProvider);
  const teamLeaders = users.filter(({ title }) => title === 'Team Leader');

  const targetUser1 = teamLeaders[0];
  const targetUser2 = teamLeaders[1];
  const renderingProvider1 = providers[0];

  return [
    {
      id: '1',
      isEnabled: true,
      name: "EKG's",
      description: `Send EKG's to ${targetUser1.firstName} ${targetUser1.lastName}`,
      conditions: {
        operator: 'And',
        conditions: [
          {
            operand: operand1.id,
            operator: operator1.id,
            value: String(DocumentType.EKG),
          },
        ],
      },
      actions: [{ id: action1.id, targetId: targetUser1.id }],
    },
    {
      id: '2',
      isEnabled: true,
      name: `${userDisplayName(renderingProvider1)}`,
      description: `Send to ${targetUser2.firstName} ${
        targetUser2.lastName
      } if rendering provider is ${userDisplayName(renderingProvider1)}`,
      conditions: {
        operator: 'And',
        conditions: [
          {
            operand: operand2.id,
            operator: operator2.id,
            value: renderingProvider1.id,
          },
        ],
      },
      actions: [{ id: action2.id, targetId: targetUser2.id }],
    },
    // {
    //   id: '3',
    //   isEnabled: false,
    //   name: 'Reassign',
    //   description: '',
    //   conditions: {
    //     operator: 'And',
    //     conditions: [
    //       {
    //         operand: operand1.id,
    //         operator: operator1.id,
    //         value: value1.id,
    //       },
    //       {
    //         operand: operand2.id,
    //         operator: operator2.id,
    //         value: value2.id,
    //       },
    //       {
    //         operator: 'Or',
    //         conditions: [
    //           {
    //             operand: operand1.id,
    //             operator: operator1.id,
    //             value: value1.id,
    //           },
    //           {
    //             operand: operand2.id,
    //             operator: operator2.id,
    //             value: value2.id,
    //           },
    //         ],
    //       },
    //     ],
    //   },
    //   actions: [{ id: action1.id }],
    // },
  ];
}
