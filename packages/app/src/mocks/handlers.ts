import { DefaultRequestBody, rest, RestHandler } from 'msw';
import {
  GetUserSessionTokenResponse,
  Rule,
  RuleConditionOperand,
  RuleTarget,
  User,
  UserSessionCredentials,
} from '@wsh/core';
import { Document, DocumentMessage, DocumentStatus, Meta } from '@wsh/document';
import { mockDocuments } from './document.mock';
import { mockMeta, mockMetaTypeConfig } from './meta.mock';
import {
  mockRuleActions,
  mockRuleConditionOperands,
  mockRuleConditionOperators,
  mockRuleConditionValues,
  mockRules,
  mockRuleTargets,
} from './rule.mock';
import { mockUsers } from './user.mock';

/**
 * In-memory cache with localStorage persistence.
 */
class MockDataCache {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private cache: Map<string, any> = new Map();

  private parseFromLocalStorage<TData>(key: string): TData | null {
    const raw = localStorage.getItem(key);
    return raw === null ? null : JSON.parse(raw);
  }

  has(key: string): boolean {
    return localStorage.getItem(key) !== null;
  }

  get<TData>(key: string, seed: () => TData): TData {
    // If we already have this in memory cache, just return it
    if (this.cache.has(key)) {
      return this.cache.get(key);
    }

    // check local storage for something more persistent
    const data = this.parseFromLocalStorage(key);

    // If it's also not in local storage create it from the seed function and
    // just return it.
    if (data === null) {
      return this.set(key, seed());
    }

    // If we did find in localStorage, set in memory cache
    this.cache.set(key, data);

    return data as TData;
  }

  set<TData>(key: string, data: TData): TData {
    localStorage.setItem(key, JSON.stringify(data));
    this.cache.set(key, data);
    return data;
  }
}

function seedDocumentList() {
  return Array.from(mockDocuments(100));
}

function seedMetaList() {
  return mockMeta;
}

function seedUserList() {
  return Array.from(mockUsers(20));
}

function seedDocumentMessageList(): DocumentMessage[] {
  return [];
}

function seedRuleOperandList(): RuleConditionOperand[] {
  return mockRuleConditionOperands();
}

function seedRuleList(): Rule[] {
  return mockRules();
}

/**
 * Replace or add an item to a cached list.
 */
function saveToCachedList<TData extends { id?: string }>(
  item: TData,
  cache: MockDataCache,
  key: string,
  seed: () => TData[],
) {
  const list = cache.get<TData[]>(key, seed);

  const i = list.findIndex(({ id }) => id === item.id);

  if (i >= 0) {
    list[i] = item;
  } else {
    list.push({ ...item, id: `000${list.length + 1}` });
  }

  cache.set(key, list);

  return item;
}

/**
 * Mock fetch data handlers used by mock service worker.
 * @param apiRoot
 * @returns
 */
export function createHandlers(
  apiRoot: string,
  authApiRoot: string,
): RestHandler[] {
  const cache = new MockDataCache();

  // DocumentAPI
  const documentHandlers: RestHandler[] = [
    rest.get(`${apiRoot}/GetDocumentList`, (_req, res, ctx) => {
      const documentList = cache.get<Document[]>(
        'documentList',
        seedDocumentList,
      );
      return res(
        ctx.json(
          documentList.filter(
            ({ status }) => status !== DocumentStatus.Completed,
          ),
        ),
      );
    }),

    rest.post<Document>(`${apiRoot}/SaveDocument`, ({ body }, res, ctx) => {
      saveToCachedList(body, cache, 'documentList', seedDocumentList);
      return res(ctx.json(body));
    }),
  ];

  // DocumentMessageAPI
  const documentMessageHandlers: RestHandler[] = [
    rest.get(
      `${apiRoot}/GetDocumentMessageList/:documentId`,
      (req, res, ctx) => {
        const messageList = cache.get<DocumentMessage[]>(
          'documentMessageList',
          seedDocumentMessageList,
        );

        return res(
          ctx.json(
            messageList?.filter((m) => m.documentId == req.params.documentId) ??
              [],
          ),
        );
      },
    ),

    rest.post<DocumentMessage>(
      `${apiRoot}/SaveDocumentMessage`,
      ({ body }, res, ctx) => {
        saveToCachedList(
          body,
          cache,
          'documentMessageList',
          seedDocumentMessageList,
        );
        return res(ctx.json(body));
      },
    ),
  ];

  // Document PDF API
  const documentPDFHandlers: RestHandler[] = [
    rest.get(`${apiRoot}/GetPDF/:documentId`, async (_req, res, ctx) => {
      const buffer = await ctx
        .fetch('/pdf/sample.pdf')
        .then((response) => response.arrayBuffer());

      // await timeout(2000);

      return res(
        ctx.set('Content-Length', buffer.byteLength.toString()),
        ctx.set('Content-Type', 'application/pdf'),
        ctx.body(buffer),
      );
    }),
  ];

  // UserAPI
  const userHandlers: RestHandler[] = [
    rest.get(`${apiRoot}/GetDocumentUser/:userId`, async (req, res, ctx) => {
      const userList = cache.get<Omit<User, 'displayName'>[]>(
        'userList',
        seedUserList,
      );

      const user = userList.find(({ id }) => id === req.params.userId);

      return res(ctx.json(user));
    }),

    rest.get(`${apiRoot}/GetDocumentUserList`, async (_req, res, ctx) => {
      const userList = cache.get<Omit<User, 'displayName'>[]>(
        'userList',
        seedUserList,
      );
      return res(ctx.json(userList));
    }),
  ];

  // UserSessionAPI
  const userSessionHandlers: RestHandler[] = [
    rest.post<UserSessionCredentials, GetUserSessionTokenResponse>(
      `${authApiRoot}/Login`,
      (req, res, ctx) => {
        if (req.body.username !== 'john.smith' || req.body.password !== 'pwd') {
          return res(ctx.json({ exception: 'Invalid Credentials' }));
        }

        return res(ctx.json({ token: 'mock.session.token' }));
      },
    ),
  ];

  // MetaAPI
  const metaHandlers: RestHandler[] = [
    rest.get(`${apiRoot}/GetMetaTypeConfig`, (_req, res, ctx) => {
      return res(ctx.json(mockMetaTypeConfig()));
    }),

    rest.get(`${apiRoot}/GetMetaList/:documentId`, (req, res, ctx) => {
      const metaList = cache.get<Meta[]>('metaList', seedMetaList);

      return res(
        ctx.json(
          metaList?.filter((m) => m.documentId == req.params.documentId) ?? [],
        ),
      );
    }),

    rest.post<Meta>(`${apiRoot}/SaveMetaData`, ({ body }, res, ctx) => {
      saveToCachedList(body, cache, 'metaList', seedMetaList);
      return res(ctx.json(body));
    }),
  ];

  // RuleAPI
  const ruleHandlers: RestHandler[] = [
    rest.get(`${apiRoot}/GetRuleList/:ruleSetId`, (_req, res, ctx) => {
      const ruleList = cache.get<Rule[]>('rule', seedRuleList);

      return res(ctx.json(ruleList));
    }),

    rest.get(
      `${apiRoot}/GetRuleConditionOperandList/:ruleSetId`,
      (_req, res, ctx) => {
        const operandList = cache.get<RuleConditionOperand[]>(
          'ruleConditionOperand',
          seedRuleOperandList,
        );

        // NOTE: ignoring ruleSetId for now
        return res(ctx.json(operandList ?? []));
      },
    ),

    rest.get(
      `${apiRoot}/GetRuleConditionOperatorList/:operandId`,
      (req, res, ctx) => {
        const operatorList = mockRuleConditionOperators(req.params.operandId);

        return res(ctx.json(operatorList ?? []));
      },
    ),

    rest.get(
      `${apiRoot}/GetRuleConditionValueList/:operandId/:operatorId`,
      (req, res, ctx) => {
        const valueList = mockRuleConditionValues(
          req.params.operandId,
          req.params.operatorId,
        );

        return res(ctx.json(valueList));
      },
    ),

    rest.get(`${apiRoot}/GetRuleActionList/:ruleSetId`, (_req, res, ctx) => {
      const actionList = mockRuleActions();
      return res(ctx.json(actionList));
    }),

    rest.get<DefaultRequestBody, RuleTarget[], { actionId: string }>(
      `${apiRoot}/GetRuleTargetList/:actionId`,
      (req, res, ctx) => {
        const targetList = mockRuleTargets(req.params.actionId);
        return res(ctx.json(targetList));
      },
    ),

    rest.post<Rule>(`${apiRoot}/SaveRule/:ruleSetId`, ({ body }, res, ctx) => {
      saveToCachedList(body, cache, 'rule', seedRuleList);
      return res(ctx.json(body));
    }),
  ];

  return [
    ...documentHandlers,
    ...documentMessageHandlers,
    ...documentPDFHandlers,
    ...userHandlers,
    ...metaHandlers,
    ...ruleHandlers,
    ...userSessionHandlers,
  ];
}
