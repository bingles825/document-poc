import { User } from '@wsh/core';

const firstNames = [
  'Ada',
  'Bill',
  'Blake',
  'Cory',
  'George',
  'Jane',
  'Jeffery',
  'Jennifer',
  'Jim',
  'John',
  'Joseph',
  'Katherine',
  'Lily',
  'Lori',
  'Mark',
  'Nathan',
  'Ruby',
  'Rebekah',
  'Samit',
  'Stephanie',
  'Susan',
  'Thomas',
  'Tori',
].sort((a, b) => b.localeCompare(a));

const lastNames = [
  'Blakely',
  'Corwin',
  'Devi',
  'Hastings',
  'Irving',
  'Jennings',
  'Kumar',
  'Jokla',
  'Jones',
  'Lopez',
  'Martinez',
  'Masterson',
  'Puli',
  'Smith',
  'Tesla',
  'Weston',
];

export const providerTitles = ['MD', 'NP', 'PA'];

// Factory function for creating user filters that filter based on a list of titles.
export const isTitle =
  (...titles: string[]) =>
  ({ title }: { title?: string }): boolean =>
    !!title && titles.includes(title);

export const isProvider = isTitle(...providerTitles);

export const isNotProvider = ({ title }: { title?: string }): boolean =>
  !title || !providerTitles.includes(title);

const titles = ['', '', '', '', '', '', 'Team Leader', ...providerTitles];

export function* mockUsers(
  n: number,
): Generator<Omit<User, 'displayName'>, void, unknown> {
  for (let i = 1; i <= n; ++i) {
    const firstName = firstNames[i % firstNames.length];
    const lastName = lastNames[i % lastNames.length];
    const title = titles[i % titles.length];

    yield {
      id: String(i),
      firstName,
      lastName,
      email: `${firstName}.${lastName}${i}@acmemedical.com`.toLowerCase(),
      title,
    };
  }
}
