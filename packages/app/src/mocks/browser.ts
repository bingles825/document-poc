import { setupWorker } from 'msw';
import { defaultAPIRoot, defaultAuthAPIRoot } from '@wsh/core';
import { createHandlers } from './handlers';

export async function start(
  apiRoot?: string,
  authApiRoot?: string,
): Promise<void> {
  const worker = setupWorker();
  worker.use(
    ...createHandlers(
      apiRoot ?? defaultAPIRoot,
      authApiRoot ?? defaultAuthAPIRoot,
    ),
  );
  await worker.start({
    onUnhandledRequest: 'bypass',
  });
}
