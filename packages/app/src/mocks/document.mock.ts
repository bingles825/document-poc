import {
  defaultAPIRoot,
  displayDate,
  numericEnumValues,
  sortByString,
  withDisplayName,
} from '@wsh/core';
import { Document, DocumentType } from '@wsh/document';
import { isNotProvider, mockUsers } from './user.mock';

/** Generate a list of n mock document queue items. */
export function* mockDocuments(n: number): Generator<Document, void, unknown> {
  const documentTypes = numericEnumValues(DocumentType);

  const date = new Date(2021, 0, 1);
  const locations = ['Acme Medical', 'Jones Pediatrics', 'Yeslab Inc'];
  const providers = ['Jim Jacobs', 'Jenny Smith', 'Ahmed Zira'];
  const specialties = ['Pediatrics', 'Orthopedics', 'Obstetrics'];

  const [user1, user2, user3] = [...mockUsers(20)]
    .map(withDisplayName)
    .filter(isNotProvider)
    .sort(sortByString('displayName'));

  // NOTE: the user ids in this list list are tied to sort order of mock data
  // coming from documentUser list so we have to keep them in sync if we want to
  // know which users will have docs assigned
  const assignedTo = [
    user1.id,
    user1.id,
    user1.id,
    user2.id,
    user3.id,
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
  ];

  for (let i = 1; i <= n; ++i) {
    yield {
      id: String(i),
      status: (Math.floor(i / 10) % 4) + 1,
      assignedToId: assignedTo[(i - 1) % assignedTo.length],
      documentType: documentTypes[i % documentTypes.length],
      documentNumber: String(i).padStart(5, '0'),
      documentPath: `${defaultAPIRoot}/GetPDF/${i}`,
      scanDate: displayDate(new Date(date.getTime() + i * 24 * 60 * 60 * 1000)),
      filedDate: displayDate(
        new Date(date.getTime() + i * 24 * 60 * 60 * 1000),
      ),
      location: locations[i % locations.length],
      provider: providers[i % providers.length],
      specialty: specialties[i % specialties.length],
      batchNumber: String(Math.ceil(i / 10)),
      batchDescription: `Batch ${Math.ceil(i / 10)}`,
    };
  }
}
