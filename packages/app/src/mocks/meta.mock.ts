import { range } from '@wsh/core';
import { Meta, MetaType, MetaTypeConfig } from '@wsh/document';

/**
 * Mock meta type config.
 */
export function mockMetaTypeConfig(): MetaTypeConfig {
  return {
    uncategorizedTypeId: '1',
    suggestedTypeIds: [
      ...range('2', String(mockMetaTypes.suggested.length + 1)),
    ],
    types: [
      ...mockMetaTypes.uncategorized,
      ...mockMetaTypes.suggested,
      ...mockMetaTypes.derived,
    ],
  };
}

/** Helper to auto increment an id and add it to given data */
let i = 0;
function addId<TData>(data: TData): TData & { id: string } {
  return { ...data, id: String(++i) };
}

/**
 * Base mock data for mocking meta types.
 * NOTE: ids are being auto-generated to make it easier to modify the data
 * without having to manually update all the ids in the code. Also, the order
 * of keys should not be changed here as `mockMetaTypeConfig` is tightly
 * coupled to this data structure.
 */
const mockMetaTypes: Record<
  'uncategorized' | 'derived' | 'suggested',
  MetaType[]
> = {
  uncategorized: (
    [
      {
        name: 'Uncategorized',
        dataType: 'text',
      },
    ] as const
  ).map(addId),
  suggested: (
    [
      {
        name: 'Test Name',
        dataType: 'text',
      },
      {
        name: 'Test Results',
        dataType: 'text',
      },
      {
        name: 'Values',
        dataType: 'text',
      },
      {
        name: 'Units',
        dataType: 'text',
      },
    ] as const
  ).map(addId),
  derived: (
    [
      // {
      //   name: 'Document Type',
      //   dataType: 'doctype',
      // },
      {
        name: 'Lab Name',
        dataType: 'text',
      },
      {
        name: 'MRN',
        dataType: 'mrn',
      },
      {
        name: 'Patient Name',
        dataType: 'text',
      },
      {
        name: 'Date of Birth',
        dataType: 'date',
      },
      {
        name: 'Date of Service',
        dataType: 'date',
      },
      {
        name: 'Order Date',
        dataType: 'date',
      },
      {
        name: 'Result Date',
        dataType: 'date',
      },
      {
        name: 'Ordering Provider',
        dataType: 'provider',
      },
      {
        name: 'Rendering Provider',
        dataType: 'provider',
      },
      {
        name: 'Referring Provider',
        dataType: 'provider',
      },
      {
        name: 'Location',
        dataType: 'location',
      },
      // {
      //   name: 'Lab Name',
      //   dataType: 'text',
      // },
    ] as const
  ).map(addId),
};

export const mockMeta: Meta[] = (
  [
    // {
    //   documentId: '1',
    //   typeId: '6',
    //   value: 'Lab Report',
    //   score: 100,
    // },
    {
      documentId: '1',
      typeId: '8',
      value: 'Joy Smith',
      score: 100,
    },
    {
      documentId: '1',
      typeId: '9',
      value: '1998-06-05',
      score: 60,
      isFlagged: true,
    },
    {
      documentId: '1',
      typeId: '10',
      value: '2021-03-21',
      score: 60,
    },
    {
      documentId: '1',
      typeId: '11',
      value: '2021-03-22',
      score: 60,
    },
    {
      documentId: '1',
      typeId: '12',
      value: '2021-03-25',
      score: 40,
    },
    {
      documentId: '1',
      typeId: '13',
      value: 'Joel Turner MD',
      score: 20,
    },
    // 2
    // {
    //   documentId: '2',
    //   typeId: '6',
    //   value: 'Lab Report',
    //   score: 100,
    // },
    {
      documentId: '2',
      typeId: '8',
      value: 'Joy Smith',
      score: 76,
    },
    {
      documentId: '2',
      typeId: '9',
      value: '1998-06-05',
      score: 60,
      isFlagged: true,
    },
    {
      documentId: '2',
      typeId: '10',
      value: '2021-03-21',
      score: 60,
    },
    {
      documentId: '2',
      typeId: '13',
      value: 'Joel Turner MD',
      score: 20,
    },
    // 3
    // {
    //   documentId: '3',
    //   typeId: '6',
    //   value: 'Lab Report',
    //   score: 100,
    // },
    {
      documentId: '3',
      typeId: '8',
      value: 'Joy Smith',
      score: 76,
    },
    {
      documentId: '3',
      typeId: '9',
      value: '1998-06-05',
      score: 60,
      isFlagged: true,
    },
    {
      documentId: '3',
      typeId: '10',
      value: '2021-03-21',
      score: 60,
    },
    {
      documentId: '3',
      typeId: '11',
      value: '2021-03-22',
      score: 60,
    },
    {
      documentId: '3',
      typeId: '12',
      value: '2021-03-25',
      score: 40,
    },
    {
      documentId: '3',
      typeId: '13',
      value: 'Joel Turner MD',
      score: 20,
    },
  ] as const
).map(addId);
