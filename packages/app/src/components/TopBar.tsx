import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Icon,
  ButtonNormal,
  GroupIcon,
  Header3,
  colors,
  coreClassName,
  useLoadData,
  Dropdown,
  useUserAPI,
  useUserSession,
  useValueChangeCallback,
  sortByString,
  useUserSessionService,
} from '@wsh/core';
import MenuItem from '@material-ui/core/MenuItem';

const useStyles = makeStyles(({ typography }) => ({
  root: {
    alignItems: 'center',
    backgroundColor: colors.branding.wsh_light_grey,
    display: 'flex',
    height: 42,
    padding: '0px 20px',
    gap: 12,
  },
  icon: {
    width: 18,
    height: 18,
  },
  groupName: {
    display: 'flex',
    fontFamily: typography.body1.fontFamily,
    fontSize: 14,
    fontWeight: 600,
    color: colors.greys.black,
    gap: 5,
  },
  sessionInfo: {
    display: 'flex',
    flexGrow: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    gap: 16,
  },
  appName: {
    color: colors.theme.text,
  },
}));

export interface TopBarProps {
  className?: string;
}

const TopBar: React.FC<TopBarProps> = ({ className }) => {
  const classes = useStyles();

  const { userId, setUserId } = useUserSession();
  const onUserChange = useValueChangeCallback(setUserId);

  const userSessionService = useUserSessionService();

  const userAPI = useUserAPI();
  const { data: users } = useLoadData(userAPI.getUserList, []);

  const usersSorted = React.useMemo(
    () => (users ? [...users].sort(sortByString('displayName')) : []),
    [users],
  );

  React.useEffect(() => {
    if (usersSorted.length) {
      setUserId(usersSorted[0].id);
    }
  }, [setUserId, usersSorted]);

  const onLogoutClick = React.useCallback(async () => {
    await userSessionService.logout();
    document.location.href = '/'; // hard reset page just to make sure everything resets
  }, [userSessionService]);

  return (
    <div className={coreClassName('top-bar', classes.root, className)}>
      <Icon icon="showdeployments" onClick={console.log} />
      <div className={classes.groupName}>
        <GroupIcon className={classes.icon} />
        <span>Westbridge Healthcare</span>
      </div>
      <div className={classes.sessionInfo}>
        <Dropdown value={userId ?? ''} onChange={onUserChange}>
          {usersSorted.map(({ id, displayName }) => (
            <MenuItem key={id} value={id}>
              {displayName}
            </MenuItem>
          ))}
        </Dropdown>
        <ButtonNormal onClick={onLogoutClick}>Log Out</ButtonNormal>
        <Header3 className={classes.appName}>Documents Manager</Header3>
        <Icon icon="help" onClick={console.log} />
      </div>
    </div>
  );
};

export default TopBar;
