import React from 'react';

const MainContainerLazy = React.lazy(() =>
  import('@wsh/document').then((module) => ({ default: module.MainContainer })),
);

export default MainContainerLazy;
