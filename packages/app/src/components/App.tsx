import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  fontFamily,
  LANDING_URL,
  LoadingOverlayContainer,
  LoginContainer,
  LOGIN_URL,
} from '@wsh/core';
import { appClassName } from '..';
import { Document, TopBar } from '.';
import { Route, useHistory } from 'react-router-dom';

const useStyles = makeStyles({
  '@global': {
    '*': {
      boxSizing: 'border-box',
      fontFamily,
    },
  },
  root: {
    display: 'flex',
    flexDirection: 'column',
    height: '100vh',
    flexGrow: 1,
  },
  topBar: {
    flexShrink: 0,
  },
  document: {
    flexGrow: 1,
  },
  login: {
    flexGrow: 1,
  },
});

export interface AppProps {}

const App: React.FC<AppProps> = () => {
  const classes = useStyles();

  const history = useHistory();

  React.useEffect(() => {
    if (history.location.pathname === '/') {
      history.push(LANDING_URL);
    }
  }, [history]);

  return (
    <div className={appClassName('app', classes.root)}>
      <React.Suspense fallback={<></>}>
        <Route path={LANDING_URL}>
          <TopBar className={classes.topBar} />
          <Document className={classes.document} />
        </Route>
        <Route path={LOGIN_URL}>
          <LoginContainer className={classes.login} />
        </Route>
        <LoadingOverlayContainer />
      </React.Suspense>
    </div>
  );
};

export default App;
