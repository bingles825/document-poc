const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

const deployPath = path.resolve(__dirname, '..', '..', 'dist', 'app');
const storybookAssetsPath = path.resolve(__dirname, '../../.storybook_public');
const webConfigPath = path.resolve(__dirname, '../../config/web.config');

module.exports = {
  devtool: 'inline-source-map',
  devServer: {
    contentBase: path.join(__dirname, 'deploy'),
    historyApiFallback: true,
  },
  entry: {
    app: './src/bootstrap',
  },
  mode: 'development',
  output: {
    filename: '[name].bundle.[chunkhash].js',
    path: deployPath,
    // publicPath: 'auto',
    publicPath: '/',
    clean: true,
  },
  resolve: {
    extensions: ['.Webpack.js', '.web.js', '.ts', '.js', '.jsx', '.tsx'],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'ts-loader',
        options: {
          compilerOptions: {
            noEmit: false,
          },
          configFile: 'tsconfig.build.json',
        },
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          'style-loader',
          // Translates CSS into CommonJS
          'css-loader',
          // Resolve relative url paths inside of scss files.
          'resolve-url-loader',
          // Compiles Sass to CSS
          {
            loader: 'sass-loader',
            options: {
              // resolve-url-loader requires that any loader that runs before it
              // include source maps
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.css$/,
        //hot reload css
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'babel-loader',
          },
          {
            loader: 'react-svg-loader',
            options: {
              jsx: true, // true outputs JSX tags
            },
          },
        ],
      },
      {
        test: /\.(png|jpg|gif|ico)$/,
        exclude: /(node_modules|bower_components)/,
        use: ['file-loader?name=static/images/[name].[ext]'],
      },
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        use: 'url-loader?name=[name].[ext]',
      },
    ],
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        { from: storybookAssetsPath, to: deployPath },
        { from: webConfigPath, to: deployPath },
      ],
    }),
    new HtmlWebpackPlugin({
      title: 'Hot Module Replacement',
      template: 'index.html',
    }),
  ],
};
