const base = require('../../config/eslint.base.js');

module.exports = {
  ...base,
  parserOptions: {
    tsconfigRootDir: __dirname,
    project: ['./tsconfig.build.json'],
  },
};
