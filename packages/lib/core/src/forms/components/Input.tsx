import React from 'react';
import { InputBase, InputBaseProps } from '.';
import InputAdornment from '@material-ui/core/InputAdornment';
import ErrorIcon from '@material-ui/icons/Error';
import { coreClassName } from '../../styles';

export interface InputProps extends InputBaseProps {
  errorText?: string;
}

const Input: React.FC<InputProps> = ({ className, errorText, ...props }) => {
  return (
    <InputBase
      {...props}
      className={coreClassName(
        'input',
        coreClassName(`input--${props.type ?? 'text'}`),
        className,
      )}
      endAdornment={
        errorText != null ? (
          <InputAdornment position="end">
            <ErrorIcon color="error" />
          </InputAdornment>
        ) : null
      }
    />
  );
};

export default Input;
