import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import { Dropdown, DropdownProps, InputLabel } from '..';
import { coreClassName } from '../../styles';

export interface DropdownFieldProps extends DropdownProps {
  label: string;
}

const DropdownField: React.FC<DropdownFieldProps> = ({
  className,
  label,
  id,
  ...dropdownProps
}) => {
  return (
    <FormControl className={coreClassName('dropdown-field', className)}>
      <InputLabel htmlFor={id}>{label}</InputLabel>
      <Dropdown id={id} {...dropdownProps} />
    </FormControl>
  );
};

export default DropdownField;
