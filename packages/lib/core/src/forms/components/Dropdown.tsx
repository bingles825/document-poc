import React from 'react';
import Select, { SelectProps } from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { InputBase } from '.';
import { makeStyles } from '@material-ui/core';
import { CreateCSSProperties } from '@material-ui/core/styles/withStyles';
import { buttonBase } from '../../styles';
import { sortByString } from '../..';

interface DropdownStyleProps extends DropdownProps {
  overrides?: CreateCSSProperties;
}

const useSelectStyles = makeStyles(() => ({
  select: ({ readOnly, overrides }: DropdownStyleProps) => ({
    alignItems: 'center',
    cursor: readOnly ? 'auto' : 'pointer',
    display: 'flex',
    padding: '0 24px 0 8px',
    minWidth: 100,
    '&:focus': {
      backgroundColor: 'transparent',
    },
    ...overrides,
  }),
}));

const useButtonStyles = makeStyles({
  root: ({ readOnly, overrides }: DropdownStyleProps) => ({
    ...buttonBase({ readOnly }, overrides),
    alignItems: 'stretch',
    cursor: 'auto',
    padding: 0,
  }),
});

export interface DropdownItem<
  TDisplay extends string | React.ReactNode = string,
> {
  value: string;
  display: TDisplay;
}

export interface DropdownProps extends SelectProps {
  items?: DropdownItem[];
}

export const withDropdownVariant = (
  overrides: {
    button?: CreateCSSProperties;
    select?: CreateCSSProperties;
  } = {},
): React.FC<DropdownProps> =>
  React.forwardRef((props, ref) => {
    const { items, children, ...selectProps } = props;
    const selectClasses = useSelectStyles({
      ...props,
      overrides: overrides.select,
    });
    const buttonClasses = useButtonStyles({
      ...props,
      overrides: overrides.button,
    });

    return (
      <Select
        {...selectProps}
        ref={ref}
        classes={selectClasses}
        input={<InputBase classes={buttonClasses} />}
        MenuProps={{
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'left',
          },
          getContentAnchorEl: null,
        }}>
        {items?.map(({ value, display }) => (
          <MenuItem key={value} value={value}>
            {display}
          </MenuItem>
        ))}
        {children}
      </Select>
    );
  });

const Dropdown: React.FC<DropdownProps> = withDropdownVariant();

export default Dropdown;

/**
 * Hook to map an array to dropdown items.
 */
export function useDropdownItems<
  TData extends { [P in TValue | TDisplay]: string },
  TValue extends string = 'id',
  TDisplay extends string = 'display',
>(
  data: TData[] | null,
  valueProp: TValue,
  displayProp: TDisplay,
): DropdownItem[] {
  return React.useMemo(
    () =>
      data
        ?.map((item) => ({
          value: item[valueProp],
          display: item[displayProp],
        }))
        .sort(sortByString('display')) ?? [],
    [data, displayProp, valueProp],
  );
}
