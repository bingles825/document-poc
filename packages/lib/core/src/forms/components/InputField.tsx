import React from 'react';
import FormControl, { FormControlProps } from '@material-ui/core/FormControl';
import { Input, InputBaseProps, InputLabel } from '.';

export interface InputFieldProps extends Omit<FormControlProps, 'onChange'> {
  label: string;
  type?: InputBaseProps['type'];
  multiline?: InputBaseProps['multiline'];
  rows?: InputBaseProps['rows'];
  maxRows?: InputBaseProps['maxRows'];
  value?: string;
  errorText?: string;
  readOnly?: boolean;
  onChange?: InputBaseProps['onChange'];
}

const InputField: React.FC<InputFieldProps> = React.forwardRef(
  (
    {
      label,
      errorText,
      type,
      placeholder,
      value,
      defaultValue,
      multiline,
      rows,
      maxRows,
      readOnly,
      onChange,
      ...props
    },
    ref,
  ) => {
    const inputId = props.id && `${props.id}-input`;

    return (
      <FormControl ref={ref} {...props} error={errorText != null}>
        <InputLabel htmlFor={inputId}>{label}</InputLabel>
        <Input
          id={inputId}
          placeholder={placeholder}
          type={type}
          multiline={multiline}
          defaultValue={defaultValue}
          value={value}
          errorText={errorText}
          rows={rows}
          maxRows={maxRows}
          onChange={onChange}
          readOnly={readOnly}
        />
        {/* {errorText != null && (
        <FormHelperText error={errorText != null} id={`${id}-helper-text`}>
          {errorText}
        </FormHelperText>
      )} */}
      </FormControl>
    );
  },
);

export default InputField;
