import InputLabelMUI, { InputLabelProps } from '@material-ui/core/InputLabel';
import { withStyles } from '@material-ui/core/styles';
import { labelBase } from '../../styles';

export type { InputLabelProps };

const InputLabel = withStyles(() => ({
  root: labelBase,
}))(InputLabelMUI);

export default InputLabel;
