/**
 * Util to join a list of class names into a space delimited string.
 * Empty and undefined strings will be ignored.
 */
export function classList(...classNames: (string | undefined)[]): string {
  return classNames.filter((c) => c).join(' ');
}

/**
 * Creates a util to create a className for a component. It includes a prefix using
 * Block Element Modifier (BEM) notation "wsh-[libId]_c_[componentId] next-class"
 * e.g. "wsh-core_c_some-component next-class"
 * @param libId
 */
export function createClassName(libId: string) {
  /**
   * Util to create a className for a component. It includes a prefix using
   * Block Element Modifier (BEM) notation "wsh-[libId]_c_[componentId] next-class"
   * e.g. "wsh-core_c_some-component next-class"
   * @param componentId
   * @param classNames
   */
  return (
    componentId: string,
    ...classNames: (string | undefined)[]
  ): string => {
    return classList(`wsh-${libId}_c_${componentId}`, ...classNames);
  };
}

/**
 * Create component css className string for core lib.
 */
export const coreClassName = createClassName('core');
