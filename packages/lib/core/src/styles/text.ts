import colors from './colors';

export const fontFamily = [
  '"Nunito Sans"',
  '"Arial"',
  '"Segoe UI"',
  'helvetica',
  'verdana',
  'sans-serif',
].join(',');

export const accentedLabel = {
  color: colors.branding.wsh_blue,
  fontFamily: `Nunito Sans Medium,${fontFamily}`,
  fontSize: 11,
  fontWeight: 400,
} as const;

export const actionLabel = (variant: 'normal' | 'bold') =>
  ({
    color: colors.branding.wsh_blue,
    fontFamily: `Nunito Sans Medium,${fontFamily}`,
    fontSize: 10,
    fontWeight: variant === 'bold' ? 800 : 400,
  } as const);

export const bodyText = (variant: 'normal' | 500) =>
  ({
    fontFamily: `Nunito Sans Medium,${fontFamily}`,
    fontSize: 13,
    fontWeight: variant,
  } as const);

export const hintText = {
  color: colors.text.hint.color,
  fontFamily: `Nunito Sans Medium,${fontFamily}`,
  fontSize: 13,
  fontWeight: 'normal',
  fontStyle: 'italic',
} as const;

export const tableHeader = () =>
  ({
    fontFamily: `Nunito Sans,${fontFamily}`,
    fontSize: 12,
    fontWeight: '800',
  } as const);

export const tableCell = () =>
  ({
    fontFamily: `Nunito Sans,${fontFamily}`,
    fontSize: 13,
    fontWeight: '400',
  } as const);
