import { CreateCSSProperties } from '@material-ui/core/styles/withStyles';
import colors from '../colors';

export const labelBase = (): CreateCSSProperties => ({
  color: colors.greys.black,
  fontSize: '10px',
  fontWeight: 'bold',
  marginBottom: 3,
  position: 'relative',
  transform: 'none',
  '&.Mui-focused,&.Mui-error': {
    color: colors.greys.black,
  },
});
