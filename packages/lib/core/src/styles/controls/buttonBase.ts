import { CreateCSSProperties } from '@material-ui/core/styles/withStyles';
import { controlBase, ControlVariant } from '.';
import colors from '../colors';

export const buttonBase = (
  props: {
    controlVariant?: ControlVariant;
    readOnly?: boolean;
  },
  overrides: CreateCSSProperties = {},
): CreateCSSProperties => {
  const buttonColors = colors.controls[props.controlVariant ?? 'normal'];

  return {
    ...controlBase(props),
    backgroundColor: buttonColors.up.background,
    borderColor: buttonColors.up.background,
    color: buttonColors.up.text,
    '&:hover,&:focus': props.readOnly
      ? undefined
      : {
          color: buttonColors.hover.text,
          backgroundColor: buttonColors.hover.background,
          borderColor: buttonColors.hover.background,
        },
    '&:active': props.readOnly
      ? undefined
      : {
          color: buttonColors.selected.text,
          backgroundColor: buttonColors.selected.background,
          borderColor: buttonColors.selected.background,
        },
    '&:disabled': {
      color: buttonColors.up.text,
      backgroundColor: buttonColors.up.background,
      borderColor: buttonColors.up.background,
      opacity: '30%',
    },
    ...overrides,
  };
};
