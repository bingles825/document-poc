import { Theme } from '@material-ui/core/styles';
import { CreateCSSProperties } from '@material-ui/core/styles/withStyles';
import colors from '../colors';

const root: CreateCSSProperties = {
  border: `1px solid ${colors.border.normal}`,
  borderRadius: 4,
};

const title: CreateCSSProperties = {
  fontSize: 24,
  padding: '12px 24px',
  color: colors.theme.text,
  borderBottom: `1px solid ${colors.border.normal}`,
};

const content: CreateCSSProperties = {
  display: 'flex',
  flexDirection: 'column',
  gap: 10,
  padding: '12px 24px 0',
};

const actions: CreateCSSProperties = {
  display: 'flex',
  justifyContent: 'flex-end',
  gap: 10,
  padding: '14px 24px',
};

/**
 * Base styles for Dialog components.
 */
export const dialogBase = (_theme: Theme) =>
  ({
    root,
    title,
    content,
    actions,
  } as const);
