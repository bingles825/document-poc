import { alpha } from '@material-ui/core';

const greys = {
  black: '#000000',
  gray1: '#F2F2F2',
  gray2: '#D7D7D7',
  white: '#FFFFFF',
  e4e4e4: '#E4E4E4',
  f8f8f8: '#F8F8F8',
  '9e9e9e': '#9E9E9E',
} as const;

const branding = {
  wsh_blue: '#01557F',
  wsh_teal: '#1DB1B8',
  wsh_vibrant_teal: '#28CBC5',
  wsh_light_teal: '#79DFDB',
  wsh_slate_grey: '#444444',
  wsh_light_grey: '#EBEBEB',
} as const;

const border = {
  normal: greys.e4e4e4,
} as const;

const controls = {
  normal: {
    up: {
      text: greys.black,
      background: greys.e4e4e4,
      border: greys.e4e4e4,
    },
    hover: {
      text: greys.black,
      background: branding.wsh_light_teal,
      border: branding.wsh_light_teal,
    },
    selected: {
      text: greys.white,
      background: branding.wsh_vibrant_teal,
      border: branding.wsh_vibrant_teal,
    },
  },
  default: {
    up: {
      text: greys.white,
      background: branding.wsh_blue,
      border: branding.wsh_blue,
    },
    hover: {
      text: greys.black,
      background: branding.wsh_light_teal,
      border: branding.wsh_light_teal,
    },
    selected: {
      text: greys.white,
      background: branding.wsh_vibrant_teal,
      border: branding.wsh_vibrant_teal,
    },
  },

  primary_fill: '#1DB1B8',
  primary_stroke_hover: '#FFFFFF',
  primary_label: '#FFFFFF',
  focus: '#79DFDB',
  selected: branding.wsh_blue,

  // default_text: '#FFFFFF',
  // default_background: branding.wsh_blue,
  // default_border: branding.wsh_blue,
  // default_hover_text: '#000000',
  // default_hover: '#79DFDB',
  // default_hover_border: '#79DFDB',
  // default_selected_text: '#FFFFFF',
  // default_selected_background: '#28CBC5',
  // default_selected_border: '#28CBC5',
  // normal_text: '#000000',
  // normal_background: '#E4E4E4',
  // normal_border: '#E4E4E4',
  // normal_hover_text: '#000000',
  // normal_hover_background: '#79DFDB',
  // normal_hover_border: '#79DFDB',
  // normal_selected_text: '#FFFFFF',
  // normal_selected_background: '#28CBC5',
  // normal_selected_border: '#28CBC5',
  standard_hover: '#79DFDB',
  standard_down: '#28CBC5',
  input_background: '#FFFFFF',
  alt_row: '#F2F2F2',
  row_recessed: '#E0E0E0',
  alt_row_recessed: '#BDBDBD',
  text_recessed: '#828282',
  old_stroke: '#727272',
  old_fill_dark: '#3B3B3B',
  dark_border: '#C4C4C4',
} as const;

const checkbox = {
  color: branding.wsh_blue,
};

const panel = {
  action: {
    backgroundColor: alpha(branding.wsh_blue, 0.05),
  },
  light: {
    backgroundColor: greys.f8f8f8,
  },
} as const;

const rules = {
  groupOperator: {
    backgroundColor: '#6FCF97',
    borderColor: '#6FCF97',
  },
  operand: {
    backgroundColor: '#56CCF2',
    borderColor: '#56CCF2',
  },
  operator: {
    backgroundColor: '#BB6BD9',
    borderColor: '#BB6BD9',
  },
  value: {},
} as const;

const text = {
  accent: {
    color: branding.wsh_blue,
  },
  default: {
    color: greys.black,
  },
  hint: {
    color: greys['9e9e9e'],
  },
} as const;

const theme = {
  accent: branding.wsh_blue,
  text: text.default.color,
  background: '#FFFFFF',
  border: '#DDDDDD',
  success: '#8CC63F',
  warning: '#EBD53E',
  danger: '#E2404C',
  info: '#408BE2',
  hovered_text: '#000000',
  hovered_background: '#79DFDB',
  focused_background: branding.wsh_blue,
  badge_background: branding.wsh_blue,
  glyph: branding.wsh_blue,
} as const;

export default {
  border,
  checkbox,
  greys,
  branding,
  controls,
  panel,
  rules,
  text,
  theme,
};
