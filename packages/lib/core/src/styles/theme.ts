import { createTheme, ThemeOptions } from '@material-ui/core';
import colors from './colors';
import { bodyText, fontFamily } from './text';

const themeOptions: ThemeOptions = {
  typography: {
    fontFamily,
    fontSize: 13,
    button: {
      fontSize: 13,
      textTransform: 'none',
    },
    h1: {
      color: colors.branding.wsh_blue,
      fontFamily: `Nunito Sans Black,${fontFamily}`,
      fontSize: 24,
      fontWeight: 'bold',
    },
    h2: {
      color: colors.branding.wsh_blue,
      fontFamily: `Nunito Sans Black,${fontFamily}`,
      fontSize: 18,
      fontWeight: 'bold',
    },
    h3: {
      color: colors.branding.wsh_blue,
      fontFamily: `Nunito Sans Heavy,${fontFamily}`,
      fontSize: 13,
      fontWeight: 'bold',
    },
    body1: {
      ...bodyText('normal'),
    },
  },
  palette: {
    error: {
      main: colors.theme.danger,
    },
  },
};

export const theme = createTheme(themeOptions);
console.log('theme:', theme);
