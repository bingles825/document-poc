import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Document, Page } from 'react-pdf/dist/esm/entry.webpack';
import { coreClassName, colors, accentedLabel } from '../../styles';
import {
  Icon,
  Dropdown,
  mapGenerator,
  range,
  useValueChangeCallback,
  useIncrementDecrementCallback,
  LoadingSpinner,
  getUserSessionToken,
} from '../..';

const documentContainerPadding = 27;

const useStyles = makeStyles({
  root: {
    alignItems: 'stretch',
    backgroundColor: colors.theme.background,
    display: 'flex',
    flexDirection: 'column',
    flexBasis: 0,
    flexGrow: 1,
  },
  controls: {
    alignItems: 'center',
    backgroundColor: colors.panel.light.backgroundColor,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    padding: '4px 6px',
    '& > :not(:first-child)': {
      marginLeft: 14,
    },
  },
  zoomLabel: {
    ...accentedLabel,
  },
  documentContainer: {
    alignItems: 'center',
    display: 'flex',
    backgroundColor: '#7B7B7B',
    flexDirection: 'column',
    flexBasis: 0,
    flexGrow: 1,
    overflow: 'auto',
    padding: `${documentContainerPadding}px 0 ${documentContainerPadding}px`,
  },
  document: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
    gap: 18,
    '& .react-pdf__message--loading': {
      display: 'flex',
      alignItems: 'center',
      flexGrow: 1,
      justifyContent: 'center',
    },
  },
});

const ZOOM_MIN = 0 as const;
const ZOOM_MAX = 200 as const;
const OBSERVER_PADDING_PCT = 40;
const INTERSECTION_THRESHOLD_PCT = 15;

export interface DocumentEditorProps {
  className?: string;
  documentPath: string;
}

const DocumentEditor: React.FC<DocumentEditorProps> = ({
  className,
  documentPath,
}) => {
  const classes = useStyles();

  const [loadCount, setLoadCount] = React.useState(0);
  const [currentPage, setCurrentPage] = React.useState(1);
  const [currentZoom, setCurrentZoom] = React.useState(100);
  const [totalPages, setTotalPages] = React.useState(0);

  const contentContainerRef = React.useRef<HTMLDivElement>(null);

  // We keep our currentPage in sync with the scroll position within the document.
  // 1. IntersectionObserver used to detect when a page comes into view to update the currentPage
  // 2. When currentPage is set by a React event, we use scrollIntoView on the div for the page
  // This ref is used to avoid conflicts between the 2 mechanisms.
  const pageSyncRef = React.useRef({
    isIntersectionObserverActive: false,
    isScrollIntoViewActive: false,
  });

  const [docWidth, setDocWidth] = React.useState(0);

  const scale = currentZoom / 100;

  // Reset current page and total pages whenever doc changes
  React.useEffect(() => {
    setCurrentPage(1);
    setTotalPages(0);
  }, [documentPath]);

  // When user navigates to a new page via the controls, scroll the appropriate
  // page into view
  React.useEffect(() => {
    console.log(
      'isIntersectionObserverActive:',
      pageSyncRef.current.isIntersectionObserverActive,
    );

    // if currentPage change was initiated by tthe IntersectionObserver, ignore
    // it but set the flag back to inactive.
    if (pageSyncRef.current.isIntersectionObserverActive) {
      pageSyncRef.current.isIntersectionObserverActive = false;
      return;
    }

    const page = document.querySelector(
      `.react-pdf__Page[data-page-number="${currentPage}"]`,
    );

    pageSyncRef.current.isScrollIntoViewActive = true;
    page?.scrollIntoView({ behavior: 'auto' });
    pageSyncRef.current.isScrollIntoViewActive = false;
  }, [currentPage]);

  // Use an IntersectionObserver to detect when a page is scrolled into view
  // so that we can update the current page number
  React.useEffect(() => {
    const pages = document.querySelectorAll('.react-pdf__Page');

    function callback(entries: IntersectionObserverEntry[]) {
      console.log(
        'IntersectionObserver',
        pageSyncRef.current.isScrollIntoViewActive,
      );

      // If scrollIntoView is currently active, ignore intersection events
      if (pageSyncRef.current.isScrollIntoViewActive) {
        return;
      }

      entries = entries.filter((entry) => entry.isIntersecting);

      // When the document first loads, many pages can intersect (presumably
      // because their containers are small). Since the real events we want to
      // catch should only show up for 1 or 2 pages at a time, we constrain the
      // entry count
      if (entries.length === 0 || entries.length > 2) {
        return;
      }

      const pageNumber = Number(
        entries[0].target.getAttribute('data-page-number'),
      );

      const newPageNumber = Math.min(pageNumber, totalPages);

      if (newPageNumber !== currentPage) {
        pageSyncRef.current.isIntersectionObserverActive = true;
        setCurrentPage(newPageNumber);
      }
    }

    const threshold = INTERSECTION_THRESHOLD_PCT / 100 / scale;
    console.log(threshold);

    const observer = new IntersectionObserver(callback, {
      root: contentContainerRef.current,
      // target an area in the middle of the document
      rootMargin: `-${OBSERVER_PADDING_PCT}% 0px -${OBSERVER_PADDING_PCT}% 0px`,
      threshold,
    });

    // observe all of our PDF page elements
    pages.forEach((page) => observer.observe(page));

    return () => {
      pages.forEach((page) => observer.unobserve(page));
    };
  }, [currentPage, loadCount, scale, totalPages]);

  // Try to keep document size smaller than the container div to avoid scrollbars
  React.useEffect(() => {
    function callback() {
      if (contentContainerRef.current) {
        setDocWidth(
          contentContainerRef.current.clientWidth -
            documentContainerPadding * 2,
        );
      }
    }

    callback();
    window.addEventListener('resize', callback);

    return () => {
      window.removeEventListener('resize', callback);
    };
  }, []);

  const token = getUserSessionToken();

  const file = React.useMemo(
    () => ({
      url: documentPath,
      httpHeaders: {
        Authorization: token ? `Bearer ${token}` : '',
      },
    }),
    [documentPath, token],
  );

  const pageItems = React.useMemo(
    () => [
      ...mapGenerator(range(1, totalPages), (i) => ({
        value: String(i),
        display: `Page ${i}`,
      })),
    ],
    [totalPages],
  );

  const onLoadSuccess = React.useCallback((pdf: { numPages: number }) => {
    setTotalPages(pdf.numPages);
    setLoadCount((i) => i + 1);
  }, []);

  const onPageChange = useValueChangeCallback(
    // HACK: Since dropdown is using string ids, make sure we convert
    // to number to avoid any subtle bugs.
    React.useCallback(
      (value: string | number) => {
        setCurrentPage(Number(value));
      },
      [setCurrentPage],
    ),
  );

  const [onPrevPage, onNextPage] = useIncrementDecrementCallback(
    setCurrentPage,
    {
      by: 1,
      min: 1,
      max: totalPages,
    },
  );

  const onZoomIn = React.useCallback(() => {
    setCurrentZoom((zoom) => Math.min(zoom + 25, ZOOM_MAX));
  }, []);

  const onZoomOut = React.useCallback(() => {
    setCurrentZoom((zoom) => Math.max(zoom - 25, ZOOM_MIN));
  }, []);

  return (
    <div className={coreClassName('document-editor', classes.root, className)}>
      <div className={classes.controls}>
        <Dropdown
          items={pageItems}
          value={pageItems.length > 0 ? currentPage : ''}
          onChange={onPageChange}
        />
        <Icon icon="next" size={18} flipH onClick={onPrevPage} />
        <Icon icon="next" size={18} onClick={onNextPage} />
        <Icon icon="zoomin" size={18} onClick={onZoomIn} />
        <Icon icon="zoomout" size={18} onClick={onZoomOut} />
        <span className={classes.zoomLabel}>{currentZoom}%</span>
      </div>
      <div ref={contentContainerRef} className={classes.documentContainer}>
        <Document
          className={classes.document}
          loading={<LoadingSpinner />}
          file={file}
          onLoadSuccess={onLoadSuccess}>
          {Array.from(range(1, totalPages)).map((pageNumber) => (
            <Page
              key={pageNumber}
              loading=""
              pageNumber={pageNumber}
              scale={scale}
              width={docWidth * scale}
            />
          ))}
        </Document>
      </div>
    </div>
  );
};

export default DocumentEditor;
