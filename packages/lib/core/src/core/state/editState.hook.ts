import React from 'react';

/** Return type for useEditState hook */
export type UseEditStateResult<T> = [
  T,
  React.Dispatch<React.SetStateAction<T>>,
  () => void,
];

/**
 * Custom state hook that updates if input value changes. It also provides a
 * reset function that can be used to revert to last inputValue. Otherwise is
 * identical to React.useState
 * @param inputValue
 * @returns
 */
export function useEditState<T>(inputValue: T): UseEditStateResult<T> {
  const [state, setState] = React.useState(inputValue);
  const [initialState, setInitialState] = React.useState(inputValue);

  // If our input state changes, reset the state to the new input
  React.useEffect(() => {
    setState(inputValue);
    setInitialState(inputValue);
  }, [inputValue]);

  // Reset our state to the last inputValue
  const reset = React.useCallback(() => {
    setState(initialState);
  }, [initialState]);

  return [state, setState, reset];
}

/** Return type for useEditListState hook */
export interface UseEditListStateResult<TItem extends { id: string | number }> {
  list: TItem[];
  setItem: (item: TItem) => void;
  setList: React.Dispatch<React.SetStateAction<TItem[]>>;
  resetList: () => void;
}

/**
 * Custom state hook to make it easier to deal with arrays of data. Mostly works
 * like useEditState but includes a setItem callback that can be used to replace
 * an item in the list
 * @param inputValue
 * @returns
 */
export function useEditListState<TItem extends { id: string | number }>(
  inputValue: TItem[],
): UseEditListStateResult<TItem> {
  const [list, setList, resetList] = useEditState(inputValue);

  // Setter that can replace an item in the list based on matching id
  const setItem = React.useCallback(
    (item: TItem) => {
      const i = list.findIndex(({ id }) => id === item.id);
      setList([...list.slice(0, i), item, ...list.slice(i + 1)]);
    },
    [list, setList],
  );

  return {
    list,
    setItem,
    setList,
    resetList,
  };
}
