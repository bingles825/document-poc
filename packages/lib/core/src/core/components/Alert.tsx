import { makeStyles } from '@material-ui/core/styles';
import Snackbar, { SnackbarProps } from '@material-ui/core/Snackbar';
import React from 'react';
import { Icon } from '.';
import { bodyText, colors, coreClassName } from '../../styles';
import { IconID } from '..';

const useStyles = makeStyles({
  root: ({ level }: Pick<AlertProps, 'level'>) => ({
    '& .MuiSnackbarContent-root': {
      ...bodyText(500),
      backgroundColor: colors.theme[level],
      padding: '18px 12px',
    },
    '& .MuiSnackbarContent-message': {
      padding: 0,
    },
  }),
  message: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    gap: 12,
  },
});

export interface UseAlertResult {
  Alert: React.FC<AlertProps>;
  isOpen: boolean;
  showAlert: (message?: string) => void;
}

export interface AlertProps extends Omit<SnackbarProps, 'open' | 'onClose'> {
  level: 'success' | 'warning' | 'danger' | 'info';
}

const levelIcon: Record<AlertProps['level'], IconID> = {
  success: 'check',
  warning: 'alert',
  danger: 'narrowx',
  info: 'info',
};

/**
 *
 * @returns
 */
export function useAlert(): UseAlertResult {
  const [messageFromShowAlert, setMessageFromShowAlert] = React.useState<
    React.ReactNode | undefined
  >();
  const [isOpen, setIsOpen] = React.useState(false);

  const onClose = React.useCallback(() => {
    setMessageFromShowAlert(undefined);
    setIsOpen(false);
  }, []);

  const showAlert = React.useCallback((message?: React.ReactNode) => {
    setMessageFromShowAlert(message);
    setIsOpen(true);
  }, []);

  const Alert: React.FC<AlertProps> = React.useCallback(
    ({ className, level, message, ...snackbarProps }) => {
      // eslint-disable-next-line react-hooks/rules-of-hooks
      const classes = useStyles({ level });

      if (messageFromShowAlert !== undefined) {
        message = messageFromShowAlert;
      }

      return (
        <Snackbar
          {...snackbarProps}
          className={coreClassName('alert', classes.root, className)}
          message={
            typeof message === 'string' ? (
              <div className={classes.message}>
                <Icon
                  icon={levelIcon[level]}
                  size={24}
                  fillColor={colors.greys.white}
                />
                {message}
              </div>
            ) : (
              message
            )
          }
          open={isOpen}
          onClose={onClose}
        />
      );
    },
    [isOpen, messageFromShowAlert, onClose],
  );

  return {
    Alert,
    isOpen,
    showAlert,
  };
}
