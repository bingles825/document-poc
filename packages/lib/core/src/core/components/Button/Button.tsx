import React from 'react';
import ButtonMUI, {
  ButtonProps as ButtonMUIProps,
} from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { CreateCSSProperties } from '@material-ui/core/styles/withStyles';
import { buttonBase, coreClassName, ControlVariant } from '../../../styles';

export interface ButtonProps extends ButtonMUIProps {
  controlVariant?: ControlVariant;
  enableMinWidth?: true;
  text?: string;
}

interface ButtonStyleProps
  extends Pick<ButtonProps, 'controlVariant' | 'enableMinWidth' | 'text'> {
  overrides?: CreateCSSProperties;
}

const useButtonStyles = makeStyles({
  root: ({ overrides, ...props }: ButtonStyleProps) => ({
    minWidth: props.enableMinWidth ? 96 : undefined,
    ...buttonBase(props, overrides),
  }),
});

/** Higher-order component to set the controlVariant */
export const withButtonVariant = (
  controlVariant: ControlVariant,
  overrides?: CreateCSSProperties,
): React.FC<ButtonProps> =>
  React.forwardRef(
    ({ className, children, enableMinWidth, text, ...props }, ref) => {
      const classes = useButtonStyles({
        controlVariant,
        enableMinWidth,
        overrides,
      });

      return (
        <ButtonMUI
          ref={ref}
          className={coreClassName(
            'button',
            coreClassName(`button--${controlVariant}`),
            className,
          )}
          classes={classes}
          disableRipple={true}
          {...props}>
          {text ?? children}
        </ButtonMUI>
      );
    },
  );

export const ButtonNormal = withButtonVariant('normal');
export const ButtonDefault = withButtonVariant('default');
