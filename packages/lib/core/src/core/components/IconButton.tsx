import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { ButtonNormal, coreClassName, Icon, IconProps } from '../..';

const useStyles = makeStyles({
  root: ({ buttonSize }: IconButtonProps) => ({
    height: buttonSize,
    minWidth: 0,
    width: buttonSize,
    padding: 0,
  }),
});

export interface IconButtonProps
  extends Omit<IconProps, 'className' | 'onClick'> {
  className?: string;
  buttonSize?: number;
  disabled?: boolean;
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
}

/**
 * A ButtonNormal with an Icon as its content.
 */
const IconButton: React.FC<IconButtonProps> = (props) => {
  const classes = useStyles(props);

  const { className, disabled, onClick, children, ...iconProps } = props;

  return (
    <ButtonNormal
      className={coreClassName('icon-button', classes.root, className)}
      disabled={disabled}
      onClick={onClick}>
      <Icon {...iconProps} />
      {children}
    </ButtonNormal>
  );
};

export default IconButton;
