import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { classList, colors, coreClassName } from '../../../styles';
import { IconID, iconMap } from '../../util/iconMap';

// Targetting the primary svg child and setting the fill color. This may not
// work for all of our icons, but it seems to catch the majority. TODO: how to
// do this in a way that will always work for all .svg files.
const iconColor = (fill: string) => ({
  '& g,circle,elipse,path': {
    '&:first-child': {
      fill,
    },
  },
});

const useStyles = makeStyles({
  root: ({ size = 16 }: IconProps) => ({
    width: size,
    height: size,
  }),
  icon: ({ fillColor, flipH, onClick }: IconProps) => ({
    ...iconColor(fillColor ?? colors.controls.default.up.background),
    transform: `scaleX(${flipH ? -1 : 1})`,
    // only show hover styles if we have an onClick handler
    '&:hover': onClick
      ? {
          cursor: 'pointer',
          ...iconColor(colors.controls.default.hover.background),
        }
      : undefined,
  }),
});

const iconClassName = coreClassName('icon');

export interface IconProps {
  className?: string;
  icon: IconID;
  onClick?: () => void;
  fillColor?: string;
  size?: number;
  title?: string;
  flipH?: boolean;
}

const Icon = React.forwardRef<HTMLDivElement, IconProps>((props, ref) => {
  const classes = useStyles(props);

  const { className, icon, title, onClick } = props;
  const Icon = iconMap[icon];

  return (
    <div
      ref={ref}
      className={classList(
        iconClassName,
        `${iconClassName}--${icon}`,
        classes.root,
        className,
      )}
      title={title}
      onClick={onClick}>
      <Icon className={classes.icon} />
    </div>
  );
});

export default Icon;
