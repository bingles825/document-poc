import React from 'react';
import { default as MUIDialog } from '@material-ui/core/Dialog';
import { coreClassName } from '../../styles';

export interface UseModalResult<TProps> {
  Modal: React.FC<TProps>;
  isOpen: boolean;
  openModal: () => void;
}

/**
 * Custom hook for wrapping a component in a modal dialog + some state utils.
 *
 * e.g.
 *
 * const { Modal, openModal, isOpen } = useModal(SomeDialog, [
 *   'onCancel',
 *   'onOk',
 * ]);
 *
 * <span>Is open: {isOpen}</span>
 * <button onClick={openModal}>Open</button>
 * <Modal onCancel={onHandleCancel} onOk={onHandleOk}/>
 *
 * @param ContentComponent Component to render in the Modal
 * @param actionPropNames Any callback props on the ContentComponent that should close the modal
 */
export function useModal<
  TProps,
  // eslint-disable-next-line @typescript-eslint/ban-types
  TCallbackProp extends KeysWithValue<TProps, Function>,
>(
  ContentComponent: React.FC<TProps>,
  actionPropNames?: TCallbackProp[],
): UseModalResult<TProps> {
  const [isOpen, setIsOpen] = React.useState(false);

  const onClose = React.useCallback(() => {
    setIsOpen(false);
  }, []);

  const openModal = React.useCallback(() => {
    setIsOpen(true);
  }, []);

  const Modal: React.FC<TProps> = React.useCallback(
    (props) => {
      // For any props designated as action callbacks, wrap the incoming callbacks
      // with a function that closes the modal then calls the original callback
      const actionCallbackProps = actionPropNames?.reduce(
        (actionCallbackProps, actionProp: TCallbackProp) => {
          // Original callback prop
          const callback = (props as TProps)[actionProp];

          // Wrapper that closes modal and calls original callback
          const wrapper = ((
            ...args: Parameters<TProps[TCallbackProp]>
          ): ReturnType<TProps[TCallbackProp]> => {
            setIsOpen(false);
            return callback?.(...args);
          }) as TProps[TCallbackProp];

          actionCallbackProps[actionProp] = wrapper;

          return actionCallbackProps;
        },
        {} as Record<TCallbackProp, TProps[TCallbackProp]>,
      );

      return (
        <MUIDialog
          className={coreClassName('modal')}
          maxWidth={false}
          open={isOpen}
          onClose={onClose}>
          <ContentComponent {...props} {...actionCallbackProps} />
        </MUIDialog>
      );
    },
    [ContentComponent, actionPropNames, isOpen, onClose],
  );

  return {
    Modal,
    isOpen,
    openModal,
  };
}
