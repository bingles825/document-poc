import * as icons from '../../assets';

/** Helper type to extract the name portion of each icon type */
type ExtractIconName<TKey extends string> = TKey extends `${infer Name}Icon`
  ? Name
  : never;

/** Lowercase prefix of icon */
export type IconID = Lowercase<ExtractIconName<keyof typeof icons>>;

/** Map of Icon components to id keys that can be used to access them. */
export const iconMap = Object.keys(icons).reduce((map, iconName) => {
  map[iconName.replace(/Icon$/, '').toLowerCase() as IconID] =
    icons[iconName as keyof typeof icons];
  return map;
}, {} as Record<IconID, SVGComponent>);
