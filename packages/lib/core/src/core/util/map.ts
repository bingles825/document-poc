/**
 * Reducer function to create a map of items in an array keyed on a specific prop.
 */
export function mapBy<K extends string>(prop: K) {
  return <T extends { [P in K]: string }>(
    map: Record<T[K], T>,
    item: T,
  ): Record<T[K], T> => {
    map[item[prop]] = item;
    return map;
  };
}

/**
 * Copy an object converting listed props to a given type.
 *
 * e.g.
 * propsToType(String, 'id')({ id: 4 }) // { id: '4' }
 * propsToType(Number, 'id', 'age')({ id: '1', age: '29' }) // { id: 1, age, 29 }
 *
 * @param cast
 * @param props
 * @returns
 */
export function propsToType<TTo, TProps extends string>(
  cast: (value: unknown) => TTo,
  ...props: TProps[]
) {
  return <
    T extends { [P in TProps]: unknown },
    TResult extends Omit<T, TProps> & { [P in TProps]: TTo },
  >(
    item: T,
  ): TResult => {
    const copy = { ...item } as unknown as TResult;
    for (const prop of props) {
      copy[prop] = cast(item[prop]) as TResult[TProps];
    }
    return copy;
  };
}
