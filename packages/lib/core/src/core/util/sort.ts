/**
 * Create a sort comparator that compares a string prop.
 * @param key
 *
 * declare const users: { firstName: string }[];
 * users.sort(sortByString('firstName'))
 */
export function sortByString<K extends string>(
  key: K,
  direction: 'ascending' | 'descending' = 'ascending',
) {
  return <T extends { [P in K]: string }>(t1: T, t2: T): number =>
    direction === 'descending'
      ? t2[key].localeCompare(t1[key])
      : t1[key].localeCompare(t2[key]);
}

/**
 * Create a sort comparator that compares a numeric string prop.
 * @param key
 *
 * const users: { id: string }[
 *   { id: '17' },
 *   { id: '2' }
 * ];
 * users.sort(sortByNumericString('id')) // [{ id: '2' }, { id: '17' }]
 */
export function sortByNumericString<K extends string>(
  key: K,
  direction: 'ascending' | 'descending' = 'ascending',
) {
  return <T extends { [P in K]: string }>(t1: T, t2: T): number =>
    direction === 'descending'
      ? Number(t2[key]) - Number(t1[key])
      : Number(t1[key]) - Number(t2[key]);
}
