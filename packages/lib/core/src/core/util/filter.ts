/**
 * Filter predicate for matching a key + value.
 * @param prop
 * @param value
 */
export function isMatch<K extends string, V>(prop: K, value: V) {
  return <T extends { [P in K]: V }>(item: T): boolean => {
    return item[prop] === value;
  };
}
