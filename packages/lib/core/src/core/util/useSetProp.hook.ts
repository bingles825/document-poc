import React from 'react';

/**
 * Custom hook to set single properties on state objects. Supports both forms
 * of setState setter functions. If the setter takes a value: T, the previous
 * value has to be explicitly passed. If the setter takes a callback, the previous
 * value must be implicitly provided by the setter.
 *
 * e.g.
 * const [state, setState] = useState({ name: 'John Doe' })
 *
 * // creating the callback can be done in 2 different ways
 * const setProp = useSetProp(setState, state) // pass previous value explicitly
 * const setProp = useSetProp(setState)        // pass previous value implicitly
 *
 * // setter for specific prop
 * const setName = setProp('name')
 * setName('Jane Doe')
 */
export function useSetProp<T>(
  // if setter function takes a callback, previousValue must be implicitly
  // provided by the setter function
  setter: (callback: (prev: T) => T) => void,
): <K extends keyof T>(prop: K) => (value: T[K]) => void;
export function useSetProp<T>(
  setter: (value: T) => void,
  // if setter function takes a value, we need previousValue to be passed
  // explicitly
  previousValue: T,
): <K extends keyof T>(prop: K) => (value: T[K]) => void;
export function useSetProp<T>(
  // eslint-disable-next-line @typescript-eslint/ban-types
  setter: Function,
  previousValue?: T,
): <K extends keyof T>(prop: K) => (value: T[K]) => void {
  return React.useCallback(
    <K extends keyof T>(prop: K) => {
      return (value: T[K]) => {
        if (previousValue) {
          setter({ ...previousValue, [prop]: value });
        } else {
          setter((previousValue: T) => ({ ...previousValue, [prop]: value }));
        }
      };
    },
    [previousValue, setter],
  );
}
