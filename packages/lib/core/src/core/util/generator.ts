/**
 * Map a generator to another one.
 */
export function* mapGenerator<T, U>(
  generator: Generator<T, void, unknown>,
  predicate: (t: T) => U,
): Generator<U, void, unknown> {
  for (const t of generator) {
    yield predicate(t);
  }
}

/**
 * Generate a range of numbers. The `start` and `end` params can be either
 * numbers or numeric strings, and the yielded type will match the input type.
 */
export function* range<T extends number | string>(
  start: T,
  end: T,
): Generator<T, void, unknown> {
  const ctr = typeof start === 'number' ? Number : String;
  const s = Number(start);
  const e = Number(end);

  for (let i = s; i <= e; ++i) {
    yield ctr(i) as T;
  }
}
