import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { colors, coreClassName } from '../../styles';

const useStyles = makeStyles({
  '@keyframes spinner-keyframes': {
    '0%': {
      opacity: 1,
    },
    '100%': {
      opacity: 0,
    },
  },
  root: {
    width: 80,
    height: 80,
    '& div': {
      transformOrigin: '40px 40px',
      animation: '$spinner-keyframes 1.2s linear infinite',
    },
    '& div:after': {
      content: '" "',
      display: 'block',
      position: 'absolute',
      top: 18,
      left: 37,
      width: 6,
      height: 14,
      borderRadius: '20%',
      background: colors.branding.wsh_blue,
    },
    '& div:nth-child(1)': {
      transform: 'rotate(0deg)',
      animationDelay: '-1.2s',
    },
    '& div:nth-child(2)': {
      transform: 'rotate(51.4deg)',
      animationDelay: '-1s',
    },
    '& div:nth-child(3)': {
      transform: 'rotate(102.8deg)',
      animationDelay: '-0.8s',
    },
    '& div:nth-child(4)': {
      transform: 'rotate(154.2deg)',
      animationDelay: '-0.6s',
    },
    '& div:nth-child(5)': {
      transform: 'rotate(205.6deg)',
      animationDelay: '-0.4s',
    },
    '& div:nth-child(6)': {
      transform: 'rotate(257deg)',
      animationDelay: '-0.2s',
    },
    '& div:nth-child(7)': {
      transform: 'rotate(308.4deg)',
      animationDelay: '-0s',
    },
  },
});

export interface LoadingSpinnerProps {
  className?: string;
}

const LoadingSpinner: React.FC<LoadingSpinnerProps> = ({ className }) => {
  const classes = useStyles();

  return (
    <div className={coreClassName('loading-spinner', classes.root, className)}>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};

export default LoadingSpinner;
