import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { coreClassName } from '../../styles';
import { LoadingSpinner, useLoadingOverlay } from '..';

const useStyles = makeStyles({
  root: {
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    overflow: 'hidden',
    zIndex: 3,
  },
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    background: 'rgba(0, 0, 0, 0.2)',
  },
  spinner: {
    display: 'inline-block',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    margin: 'auto',
  },
});

export interface LoadingOverlayProps {}

export const LoadingOverlay: React.FC<LoadingOverlayProps> = () => {
  const classes = useStyles();

  return (
    <div className={coreClassName('loading-overlay', classes.root)}>
      <div className={classes.overlay}></div>
      <LoadingSpinner className={classes.spinner} />
    </div>
  );
};

export const LoadingOverlayContainer: React.FC<LoadingOverlayProps> = () => {
  const { isLoading } = useLoadingOverlay();
  return isLoading ? <LoadingOverlay /> : null;
};
