import React from 'react';
import { CreateCSSProperties } from '@material-ui/core/styles/withStyles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Pagination from '@material-ui/lab/Pagination';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import {
  classList,
  coreClassName,
  colors,
  tableHeader,
  tableCell,
} from '../../styles';
import Checkbox from '@material-ui/core/Checkbox';

export const classNames = {
  component: coreClassName('data-grid'),
  cell: coreClassName('data-grid_cell'),
  cellContent: coreClassName('data-grid_cell-content'),
  row: coreClassName('data-grid_row'),
  rowSelected: coreClassName('data-grid_row--selected'),
} as const;

/**
 * Enforcing a fixed width seems to require setting widths on both the cell
 * container (th or td) and the cell content div. This util
 * makes it easier to constrain a cell to a given width. TODO: figure out if
 * there is a simpler way to do this.
 *
 * @param cellSelector Css selector for the th or td element
 * @param width Fixed width to coAnstrain the cell to
 */
export function constrainCellWidth<TSelector extends string>(
  cellSelector: TSelector,
  width: number,
): CreateCSSProperties {
  return {
    [cellSelector]: {
      // This selector targets both the cellSelector and the direct child
      [`&, & > .${classNames.cellContent}`]: {
        maxWidth: width,
        width,
      },
    },
  };
}

const useStyles = makeStyles({
  root: {},
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  row: ({ enableRowSelection, showCheckboxColumn }: DataGridProps<any>) => ({
    '&:nth-child(even)': {
      backgroundColor: colors.controls.alt_row,
    },
    [`&.${classNames.rowSelected}`]: {
      backgroundColor: showCheckboxColumn
        ? undefined
        : colors.controls.default.selected.background,
    },
    '&:hover': {
      cursor: enableRowSelection ? 'pointer' : undefined,
      backgroundColor: enableRowSelection
        ? colors.controls.standard_hover
        : undefined,
    },
  }),
  checkbox: {
    paddingTop: 0,
    paddingBottom: 0,
    '&.Mui-checked': {
      color: colors.controls.default.selected.background,
    },
    '&&:hover': {
      backgroundColor: 'transparent',
    },
  },
  cell: {
    boxSizing: 'border-box',
    height: 36,
    padding: 0,
    'th&': {
      ...tableHeader(),
      backgroundColor: colors.greys.gray2,
    },
    'td&': {
      ...tableCell(),
    },
    [`& > .${classNames.cellContent}`]: {
      alignItems: 'center',
      borderRight: `1px solid ${colors.greys.white}`,
      boxSizing: 'border-box',
      display: 'flex',
      height: '100%',
      padding: '0 8px',
    },
    '&:last-child': {
      '& > div': {
        borderColor: 'transparent',
      },
    },
  },
  footer: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  pagination: {
    alignItems: 'center',
    display: 'flex',
    flexGrow: 1,
    padding: '8px 0',
    '& .MuiPaginationItem-page.Mui-selected': {
      color: colors.controls.default.selected.text,
      backgroundColor: colors.controls.default.selected.background,
      '&:hover': {
        color: colors.controls.default.hover.text,
        backgroundColor: colors.controls.default.hover.background,
      },
    },
  },
  actions: {},
});

export type Datum = { id: string | number };

/** Definition for a datagrid column */
export interface DataGridColumn<
  TData extends Datum,
  TField extends keyof TData = keyof TData,
> {
  className?: string;
  display: string;
  field: TField | ((item: TData, rowIndex?: number) => JSX.Element);
}

export interface DataGridProps<TData extends Datum> {
  className?: string;
  columns: DataGridColumn<TData, keyof TData>[];
  data: TData[];
  pageSize?: number;
  enableRowSelection?: boolean;
  showCheckboxColumn?: boolean;
  onSelectedIdsChange?: (selectedIds: string[]) => void;
  actions?: React.ReactNode;
}

const DataGrid = <TData extends Datum>(
  props: DataGridProps<TData>,
): JSX.Element => {
  const {
    className,
    columns,
    data,
    actions,
    pageSize = 10,
    onSelectedIdsChange,
    enableRowSelection = false,
    showCheckboxColumn = false,
  } = props;

  const classes = useStyles(props);

  const [currentPage, setCurrentPage] = React.useState(1);
  const [selectedIds, setSelectedIds] = React.useState<
    Record<number | string, boolean>
  >({});

  // Reset pager whenever data changes
  React.useEffect(() => {
    setCurrentPage(1);
  }, [data]);

  React.useEffect(() => {
    if (onSelectedIdsChange) {
      onSelectedIdsChange(
        Object.keys(selectedIds).filter((id) => selectedIds[id]),
      );
    }
  }, [onSelectedIdsChange, selectedIds]);

  const pageCount = React.useMemo(
    () => Math.ceil(data.length / pageSize),
    [data, pageSize],
  );

  const dataPage = React.useMemo(() => {
    const start = pageSize * (currentPage - 1);
    const end = start + pageSize;

    return data.slice(start, end);
  }, [data, currentPage, pageSize]);

  // determine if every item in the current page is selected
  const isAllSelected = React.useMemo(() => {
    return dataPage.every(({ id }) => selectedIds[id]);
  }, [dataPage, selectedIds]);

  const onPageChange = React.useCallback(
    (_event: React.ChangeEvent<unknown>, page: number) => {
      setCurrentPage(page);
      setSelectedIds({});
    },
    [setSelectedIds],
  );

  const onRowClick = React.useCallback((rowId: number | string) => {
    return (event: React.MouseEvent<HTMLTableRowElement>): void => {
      // Modifier key allows multi-select
      if (event.metaKey || event.ctrlKey) {
        // Toggle single row selection
        setSelectedIds((ids) => {
          const current = ids[rowId];
          return { ...ids, [rowId]: !current };
        });
      } else {
        // Clear selection on all other rows
        setSelectedIds((ids) => ({
          [rowId]: !ids[rowId],
        }));
      }
    };
  }, []);

  const onToggleSelectAll = React.useCallback(() => {
    if (isAllSelected) {
      setSelectedIds({});
    } else {
      // create a record of all selected ids
      const ids = dataPage.reduce((memo, item) => {
        memo[item.id] = true;
        return memo;
      }, {} as Record<number | string, boolean>);

      setSelectedIds(ids);
    }
  }, [dataPage, isAllSelected]);

  return (
    <div className={classList(classNames.component, classes.root, className)}>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              {showCheckboxColumn && (
                <TableCell className={classes.cell} component="th">
                  <Checkbox
                    className={classes.checkbox}
                    disableRipple
                    checked={isAllSelected}
                    onClick={onToggleSelectAll}
                  />
                </TableCell>
              )}
              {/* TODO: figure out what `scope` does */}
              {columns.map((column, i) => (
                <TableCell
                  className={coreClassName(
                    'data-grid_cell',
                    classes.cell,
                    column.className,
                  )}
                  key={`${String(column.field)}-${i}`}
                  component="th"
                  scope="column">
                  <div className={classNames.cellContent}>{column.display}</div>
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {dataPage.map((row, iRow) => (
              <TableRow
                key={row.id}
                className={classList(
                  classNames.row,
                  selectedIds[row.id] ? classNames.rowSelected : undefined,
                  classes.row,
                )}
                onClick={enableRowSelection ? onRowClick(row.id) : undefined}>
                {showCheckboxColumn && (
                  <TableCell className={classes.cell}>
                    <Checkbox
                      className={classes.checkbox}
                      disableRipple
                      checked={selectedIds[row.id] ?? false}
                    />
                  </TableCell>
                )}
                {columns.map((column, iCol) => (
                  <TableCell
                    key={`${String(column.field)}-${iCol}`}
                    className={classList(
                      classNames.cell,
                      classes.cell,
                      column.className,
                    )}>
                    <div className={classNames.cellContent}>
                      {typeof column.field === 'function'
                        ? column.field(row, iRow)
                        : row[column.field]}
                    </div>
                  </TableCell>
                ))}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <div className={classes.footer}>
        {data.length > pageSize && (
          <div className={classes.pagination}>
            <Pagination
              page={currentPage}
              count={pageCount}
              onChange={onPageChange}
            />
            <Typography variant="body1">
              Page {currentPage} of {pageCount} ({data.length} items)
            </Typography>
          </div>
        )}
        <div className={classes.actions}>{actions ?? null}</div>
      </div>
    </div>
  );
};

export default DataGrid;
