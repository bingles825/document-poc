import React from 'react';

interface LoadingOverlayContextValue {
  isLoading: boolean;
  setIsLoading: (isLoading: boolean) => void;
}

const LoadingOverlayContext =
  React.createContext<LoadingOverlayContextValue | null>(null);

export const LoadingOverlayContextProvider: React.FC = ({ children }) => {
  const [loadingCount, setLoadingCount] = React.useState(0);

  const setIsLoading = React.useCallback((isLoading: boolean) => {
    const value = isLoading ? 1 : -1;
    setLoadingCount((count) => count + value);
  }, []);

  const value = React.useMemo(
    () => ({ isLoading: loadingCount > 0, setIsLoading }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [loadingCount > 0, setIsLoading],
  );

  return (
    <LoadingOverlayContext.Provider value={value}>
      {children}
    </LoadingOverlayContext.Provider>
  );
};

type LoadData = <T>(promise: Promise<T>) => Promise<T>;

/**
 * Custom hook for managing a LoadingOverlay.
 */
export function useLoadingOverlay(): LoadingOverlayContextValue & {
  showLoadingUntilResolved: LoadData;
} {
  const context = React.useContext(LoadingOverlayContext);

  if (!context) {
    throw new Error(
      'No LoadingOverlayContext was found. Are you missing a LoadingOverlayContextProvider?',
    );
  }

  const { isLoading, setIsLoading } = context;

  /** Sets loading status before and after a promise is resolved. */
  const showLoadingUntilResolved = React.useCallback<
    <T>(promise: Promise<T>) => Promise<T>
  >(
    async (promise) => {
      setIsLoading(true);
      const data = await promise;
      setIsLoading(false);

      return data;
    },
    [setIsLoading],
  );

  return {
    isLoading,
    setIsLoading,
    showLoadingUntilResolved,
  };
}
