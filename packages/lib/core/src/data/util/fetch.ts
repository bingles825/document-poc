import { LOGIN_URL } from '../..';

/**
 * Helper util for fetching json data.
 */
export function fetchJSON<TData>(
  input: RequestInfo,
  init?: RequestInit,
): Promise<TData> {
  const token = sessionStorage.getItem('document_poc_session_token');

  return fetch(input, {
    ...init,
    headers: {
      ...init?.headers,
      Authorization: token ? `Bearer ${token}` : '',
    },
  }).then((response) => {
    if (response.status === 401) {
      sessionStorage.removeItem('document_poc_session_token');
      window.location.pathname = LOGIN_URL;
    }
    return response.json();
  });
}

/**
 * Helper util for posting json data.
 */
export function postJSON<TData, TBody = TData>(
  input: RequestInfo,
  body: TBody,
): Promise<TData> {
  return fetchJSON(input, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
    },
  });
}
