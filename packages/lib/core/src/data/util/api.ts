/**
 * Default REST api root url.
 */
export const defaultAPIRoot = '/api/WSC/v1';

/**
 * Default REST auth API root url.
 */
export const defaultAuthAPIRoot = '/api/Auth/v1';
