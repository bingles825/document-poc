/**
 * Get a list of enum values from a numeric enum.
 * e.g.
 * enum Status { one = 1, two = 2 }
 * const statuses = numericEnumValues(Status) // [Status.one, Status.two]
 *
 * Note: this will not work for string valued enums since there's no way to
 * differentiate string values from string keys when filtering
 * e.g.
 * enum Status { one = '1', two = '2' }
 * const statuses = numericEnumValues(Status) // []
 * @param enumType
 */
export function numericEnumValues<
  TEnum,
  TValue extends { [P in keyof TEnum]: TEnum[P] }[keyof TEnum],
>(enumType: TEnum): TValue[] {
  return Object.values(enumType).filter((v) => typeof v === 'number');
}
