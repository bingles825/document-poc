import format from 'date-fns/fp/format';
import parseISO from 'date-fns/fp/parseISO';

export const displayDate: CurriedFn1<number | Date, string> =
  format('MM/dd/yyyy');
export const displayDateTime: CurriedFn1<number | Date, string> =
  format('MM/dd/yyyy hh:mm a');

/**
 * Given an ISO date string, returns a display friendly date / time.
 * @param isoString
 */
export function displayISOstrAsDateTime(isoString?: string): string {
  if (isoString == null) {
    return '';
  }

  return displayDateTime(parseISO(isoString));
}
