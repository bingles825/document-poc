import React from 'react';

export interface UseLoadDataResult<TData, TArgs extends unknown[]> {
  data: TData | null;
  error: Error | null;
  isLoading: boolean;
  reload: (...args: TArgs) => Promise<void>;
}

/**
 * Hook for wrapping a data loading function in React state.
 * TODO: Possibly replace this with react-query
 */
export function useLoadData<TData, TArgs extends unknown[]>(
  dataLoader: (...args: TArgs) => Promise<TData>,
  args: TArgs,
  options?: { pause?: boolean },
): UseLoadDataResult<TData, TArgs> {
  // Since we have async code that sets state, we need a mechanism to check if
  // component is still mounted
  const isActiveRef = React.useRef(true);

  // TODO: Fix loading overlay. It currently has an issue of re-rendering things
  // when loading is complete which causes infinite re-rendering of modals
  // const { isLoading, setIsLoading } = useLoadingOverlay();
  const [isLoading, setIsLoading] = React.useState(false);
  const [data, setData] = React.useState<TData | null>(null);
  const [error, setError] = React.useState<Error | null>(null);

  const load = React.useCallback(
    (...args: TArgs) => {
      setIsLoading(true);

      return dataLoader(...args)
        .then((data) => {
          if (isActiveRef.current) {
            setData(data);
          }
        })
        .catch((err) => {
          const error = new Error(String(err));
          console.error(error);
          if (isActiveRef.current) {
            setError(error);
          }
        })
        .finally(() => {
          if (isActiveRef.current) {
            setIsLoading(false);
          }
        });
    },
    [dataLoader, setIsLoading],
  );

  React.useEffect(() => {
    isActiveRef.current = true;

    if (options?.pause) {
      return;
    }

    void load(...args);

    // Flag the state as inactive to avoid async code setting state after
    // component has unmounted
    return () => {
      isActiveRef.current = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [load, options?.pause, ...args]);

  return {
    data,
    error,
    isLoading,
    reload: load,
  };
}
