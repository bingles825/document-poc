import React from 'react';

/**
 * Custom hook for passing target.value from a change event to a simple string value callback.
 */
export function useValueChangeCallback<TValue>(
  onValueChange: (value: TValue) => void,
): (event: React.ChangeEvent<{ value: unknown }>) => void {
  return React.useCallback(
    ({ target: { value } }: React.ChangeEvent<{ value: unknown }>) => {
      onValueChange(value as TValue);
    },
    [onValueChange],
  );
}

/**
 * Callback hook for increment / decrement scenarios. Takes a useState<number>
 * setter function and a configuration object controlling bounds and how much
 * to increment / decrement.
 * @param setState
 * @param by
 */
export function useIncrementDecrementCallback(
  setState: React.Dispatch<React.SetStateAction<number>>,
  { by, min, max }: { by: number; min: number; max: number },
): [() => void, () => void] {
  const change = React.useCallback(
    (by: number) => {
      setState((i) => {
        const newI = i + by;

        if (newI < min || newI > max) {
          return i;
        }

        return newI;
      });
    },
    [setState, min, max],
  );

  const prev = React.useCallback(() => change(-by), [change, by]);
  const next = React.useCallback(() => change(by), [change, by]);

  return [prev, next];
}

/**
 * Wraps an event handler callback such that preventDefault() is called before
 * the callback gets called.
 */
export function usePreventDefaultCallback<TEvent extends React.SyntheticEvent>(
  callback: (event: TEvent) => void,
  dependencyList: React.DependencyList,
): React.EventHandler<TEvent> {
  return React.useCallback((event: TEvent): void => {
    event.preventDefault();
    callback(event);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, dependencyList);
}
