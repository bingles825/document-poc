import React from 'react';

/**
 * Helper to extract currentTarget.value from an text input change event.
 */
export function getCurrentTargetValue<TElement extends { value: string }>(
  event: React.ChangeEvent<TElement>,
): string {
  return event.currentTarget.value;
}
