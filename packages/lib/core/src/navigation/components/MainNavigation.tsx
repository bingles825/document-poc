import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { MainNavigationItem, MainNavigationItemProps } from '.';
import { colors } from '../../styles';

const useStyles = makeStyles({
  root: {
    backgroundColor: colors.branding.wsh_blue,
    display: 'flex',
    flexDirection: 'column',
    width: 82,
  },
});

export interface MainNavigationProps {
  itemProps: MainNavigationItemProps[];
}

const MainNavigation: React.FC<MainNavigationProps> = ({ itemProps }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {itemProps.map((props) => (
        <MainNavigationItem key={props.to} {...props} />
      ))}
    </div>
  );
};

export default MainNavigation;
