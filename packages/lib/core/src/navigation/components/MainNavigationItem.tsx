import React from 'react';
import { NavLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { colors } from '../../styles';
import { IconID, iconMap } from '../..';

const selectedColors = {
  color: colors.branding.wsh_blue,
  backgroundColor: colors.greys.white,
} as const;

const itemColors = {
  color: colors.greys.white,
  backgroundColor: colors.branding.wsh_blue,
} as const;

const useStyles = makeStyles(({ typography }) => ({
  root: {
    ...itemColors,
    alignItems: 'center',
    cursor: 'pointer',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 0,
    textDecoration: 'none',
    height: 78,
    width: 82,
    gap: 3,
    '& > *': {
      lineHeight: '1.2em',
      margin: 0,
      textAlign: 'center',
    },
    '&:hover': {
      color: itemColors.color,
      backgroundColor: 'rgba(255, 255, 255, 0.3)',
    },
    '& g,& path': {
      fill: itemColors.color,
    },
    '&:hover g,&:hover path': {
      fill: itemColors.color,
    },
  },
  isActive: {
    ...selectedColors,
    '& g,& path': {
      fill: selectedColors.color,
    },
  },
  icon: {
    width: 32,
    height: 32,
  },
  title: {
    fontFamily: typography.body1.fontFamily,
    fontSize: 12,
    fontWeight: 300,
    letterSpacing: -0.5,
  },
}));

export interface MainNavigationItemProps {
  icon: IconID;
  label: string;
  to: string;
  exact?: boolean;
}

const MainNavigationItem: React.FC<MainNavigationItemProps> = ({
  icon,
  label,
  to,
  exact,
}) => {
  const classes = useStyles();
  const Icon = iconMap[icon];

  return (
    <NavLink
      className={classes.root}
      activeClassName={classes.isActive}
      to={to}
      exact={exact}>
      <Icon className={classes.icon} alt={label} />
      <h2 className={classes.title}>{label}</h2>
    </NavLink>
  );
};

export default MainNavigationItem;
