/**
 * Global types for image imports.
 */

type SVGComponent = (props: {
  alt?: string;
  className?: string;
}) => JSX.Element;

/** react-svg-loader allows us to import SVG's as JSX */
declare module '*.svg' {
  const content: SVGComponent;
  export default content;
}
declare module '*.jpg';
declare module '*.png';
declare module '*.jpeg';
declare module '*.gif';
