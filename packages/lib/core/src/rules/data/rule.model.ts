import React from 'react';
import { DropdownItem, numericEnumValues } from '../..';

export interface Rule {
  id: string;
  isEnabled: boolean;
  name: string;
  description: string;
  conditions: RuleConditionGroup;
  actions: RuleActionTargetMap[];
}

export type RuleConditionGroupOperator = 'And' | 'Or';

export enum RuleConditionAddOption {
  'Add Condition' = 1,
  'Add Group' = 2,
}

export type RuleConditionOperatorDisplay =
  | '<'
  | '>'
  | '='
  | '!='
  | '>='
  | '<='
  | 'is'
  | 'not'
  | 'contains';

export interface RuleConditionOperand {
  id: string;
  display: string;
}

export interface RuleConditionOperator {
  id: string;
  display: RuleConditionOperatorDisplay;
}

export interface RuleConditionValue {
  id: string;
  display: string;
}

export interface RuleAction {
  id: string;
  display: string;
}

export interface RuleTarget {
  id: string;
  display: string;
}

export interface RuleActionTargetMap {
  id: RuleAction['id'];
  targetId?: RuleTarget['id'];
}

export interface RuleCondition {
  operand: RuleConditionOperand['id'];
  operator: RuleConditionOperator['id'];
  value: RuleConditionValue['id'];
}

export interface RuleConditionGroup {
  operator: RuleConditionGroupOperator;
  conditions: (RuleCondition | RuleConditionGroup)[];
}

export function isRuleCondition(
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  value: RuleCondition | any,
): value is RuleCondition {
  return 'operand' in value && 'operator' in value && 'value' in value;
}

export const ruleConditionGroupOperatorOptions: {
  value: string;
  display: string;
}[] = [
  {
    value: 'And',
    display: 'And',
  },
  {
    value: 'Or',
    display: 'Or',
  },
];

export const newRule: Rule = {
  id: '',
  isEnabled: true,
  name: '',
  description: '',
  actions: [],
  conditions: {
    operator: 'And',
    conditions: [],
  },
};

export const ruleConditionAddOptions = numericEnumValues(
  RuleConditionAddOption,
).map((value) => ({
  id: String(value),
  display: RuleConditionAddOption[value],
}));

/**
 * Custom hook to create DropdownItem[] from given list of rule data.
 */
export function useRuleDropdownItem<
  TData extends { id: string; display: string },
>(data: TData[] | null): DropdownItem[] {
  return React.useMemo(
    () =>
      data?.map(({ id, display }) => ({
        value: id,
        display,
      })) ?? [],
    [data],
  );
}
