import {
  Rule,
  RuleAction,
  RuleConditionOperand,
  RuleConditionOperator,
  RuleConditionValue,
  RuleTarget,
} from '.';
import { createExplicitContext, fetchJSON, postJSON } from '../..';

export class RuleAPI {
  constructor(public apiRoot: string) {}

  getRules = async (ruleSetId: string): Promise<Rule[]> => {
    return fetchJSON(`${this.apiRoot}/GetRuleList/${ruleSetId}`);
  };

  getOperands = async (ruleSetId: string): Promise<RuleConditionOperand[]> => {
    return fetchJSON(
      `${this.apiRoot}/GetRuleConditionOperandList/${ruleSetId}`,
    );
  };

  getOperators = async (
    operandId: RuleConditionOperand['id'],
  ): Promise<RuleConditionOperator[]> => {
    return fetchJSON(
      `${this.apiRoot}/GetRuleConditionOperatorList/${operandId}`,
    );
  };

  getValues = async (
    operandId: RuleConditionOperand['id'],
    operatorId: RuleConditionOperator['id'],
  ): Promise<RuleConditionValue[]> => {
    return fetchJSON(
      `${this.apiRoot}/GetRuleConditionValueList/${operandId}/${operatorId}`,
    );
  };

  getActions = async (ruleSetId: string): Promise<RuleAction[]> => {
    return fetchJSON(`${this.apiRoot}/GetRuleActionList/${ruleSetId}`);
  };

  getTargets = async (actionId: string): Promise<RuleTarget[]> => {
    return fetchJSON(`${this.apiRoot}/GetRuleTargetList/${actionId}`);
  };

  saveRule = async (ruleSetId: string, rule: Rule): Promise<Rule> => {
    return postJSON(`${this.apiRoot}/SaveRule/${ruleSetId}`, rule);
  };
}

export const { Provider: RuleAPIProvider, useContext: useRuleAPI } =
  createExplicitContext<RuleAPI>('ruleAPI');
