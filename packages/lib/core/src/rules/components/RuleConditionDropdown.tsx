import React from 'react';
import { DropdownItem, useEditState, withDropdownVariant } from '../..';
import { colors, coreClassName } from '../../styles';

const select = {
  minWidth: 'auto',
  '&.MuiSelect-select': { padding: '0 8px 0 8px' },
};

const variants = {
  groupOperand: withDropdownVariant({
    button: colors.rules.groupOperator,
    select,
  }),
  operand: withDropdownVariant({
    button: colors.rules.operand,
    select,
  }),
  operator: withDropdownVariant({
    button: colors.rules.operator,
    select,
  }),
  value: withDropdownVariant({
    button: colors.rules.value,
    select,
  }),
};

export interface RuleConditionDropdownProps {
  className?: string;
  variant: 'groupOperand' | 'operand' | 'operator' | 'value';
  value?: string;
  items: DropdownItem[];
  onChange: (selectedId: string) => void;
}

const RuleConditionDropdown: React.FC<RuleConditionDropdownProps> = ({
  className,
  variant,
  value: inputValue = '',
  items,
  onChange,
}) => {
  const Dropdown = variants[variant];

  const [value, setValue] = useEditState(inputValue);

  // Create a lookup object to make it easy to get items by id
  const itemsById = React.useMemo(
    () =>
      items.reduce((itemsById, item) => {
        itemsById[item.value] = item;
        return itemsById;
      }, {} as Record<string, DropdownItem>),
    [items],
  );

  const onDropdownChange = React.useCallback(
    ({ target: { value } }) => {
      setValue(value);
      onChange(value);
    },
    [onChange, setValue],
  );

  return (
    <Dropdown
      className={coreClassName('rule-condition-dropdown', className)}
      renderValue={(value) =>
        itemsById[value as string]?.display ?? (
          <span>&nbsp;&nbsp;&nbsp;&nbsp;</span>
        )
      }
      displayEmpty={true}
      items={items}
      onChange={onDropdownChange}
      value={itemsById[value] ? value : ''}
      IconComponent={() => null}
    />
  );
};

export default RuleConditionDropdown;
