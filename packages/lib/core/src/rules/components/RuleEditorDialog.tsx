import React from 'react';
import { styled } from '@material-ui/core/styles';
import {
  ButtonDefault,
  ButtonNormal,
  dialogBase,
  Icon,
  InputField,
  RuleActionEditor,
  RuleConditionGroupEditor,
  useEditState,
  usePreventDefaultCallback,
  useSetProp,
  useValueChangeCallback,
} from '../..';
import { actionLabel, colors, coreClassName } from '../../styles';
import { Rule, RuleActionTargetMap, RuleConditionGroup } from '..';

const StyledRoot = styled('form')(({ theme }) => {
  const base = dialogBase(theme);
  return {
    ...base.root,
    width: 650,
  };
});

const StyledTitle = styled('div')(({ theme }) => dialogBase(theme).title);
const StyledContent = styled('div')(({ theme }) => ({
  ...dialogBase(theme).content,
  gap: 12,
  '& > :first-child': {
    display: 'flex',
    flexDirection: 'row',
    gap: 10,
    '& > :first-child': {
      width: 220,
    },
    '& > :last-child': {
      flexGrow: 1,
    },
  },
  '& > div:nth-child(2)': {
    display: 'flex',
    flexDirection: 'column',
    gap: 6,
    '& > label': {
      ...actionLabel('bold'),
      color: colors.text.default.color,
    },
    [`& > .${coreClassName('rule-condition-group-editor')}`]: {
      border: `1px solid ${colors.border.normal}`,
      borderRadius: 4,
      padding: 8,
      height: 300,
    },
  },
  [`& > .${coreClassName('button')},.${coreClassName('dropdown-field')}`]: {
    alignSelf: 'flex-start',
    '& > .MuiButton-label': {
      gap: 6,
    },
  },
}));
const StyledActions = styled('div')(({ theme }) => dialogBase(theme).actions);

export interface RuleEditorDialogProps {
  className?: string;
  ruleSetId: string;
  value: Rule;
  onSave: (rule: Rule) => void;
  onCancel?: () => void;
}

const RuleEditorDialog: React.FC<RuleEditorDialogProps> = ({
  className,
  ruleSetId,
  value: inputValue,
  onSave,
  onCancel,
}) => {
  const [isDirty, setIsDirty] = React.useState(false);
  const [value, setValue, revert] = useEditState(inputValue);

  const updateRule = React.useCallback(
    (value: Rule) => {
      setIsDirty(true);
      setValue(value);
    },
    [setValue],
  );

  const setValueProp = useSetProp(updateRule, value);

  const onConditionsChange = React.useCallback(
    (conditions: RuleConditionGroup) => {
      updateRule({
        ...value,
        conditions,
      });
    },
    [updateRule, value],
  );

  const onActionChange = (index: number) => {
    return (action: RuleActionTargetMap): void => {
      updateRule({
        ...value,
        actions: [
          ...value.actions.slice(0, index),
          action,
          ...value.actions.slice(index + 1),
        ],
      });
    };
  };

  const onSubmit = usePreventDefaultCallback(() => {
    onSave(value);
  }, [onSave, value]);

  const onCancelClick = React.useCallback(() => {
    setIsDirty(false);
    revert();
    onCancel?.();
  }, [onCancel, revert]);

  console.log('RuleEditor:', isDirty, JSON.stringify(value, undefined, 2));

  return (
    <StyledRoot
      className={coreClassName('rule-editor-dialog', className)}
      onSubmit={onSubmit}>
      <StyledTitle>Edit Rule</StyledTitle>
      <StyledContent>
        <div>
          <InputField
            label="Rule Name"
            onChange={useValueChangeCallback(setValueProp('name'))}
            value={value.name}
          />
          <InputField
            label="Description"
            onChange={useValueChangeCallback(setValueProp('description'))}
            value={value.description}
          />
        </div>

        <div>
          <label>If the following is true</label>
          <RuleConditionGroupEditor
            ruleSetId={ruleSetId}
            value={value.conditions}
            onChange={onConditionsChange}
          />
        </div>

        {value.actions.map((action, i) => (
          <RuleActionEditor
            key={`${action.id}-${i}`}
            ruleSetId={ruleSetId}
            value={action}
            onChange={onActionChange(i)}
          />
        ))}

        <ButtonNormal>
          <Icon icon="add" />
          Action
        </ButtonNormal>
      </StyledContent>
      <StyledActions>
        <ButtonDefault disabled={!isDirty} enableMinWidth type="submit">
          Save Changes
        </ButtonDefault>
        <ButtonNormal enableMinWidth onClick={onCancelClick}>
          Cancel
        </ButtonNormal>
      </StyledActions>
    </StyledRoot>
  );
};

export default RuleEditorDialog;
