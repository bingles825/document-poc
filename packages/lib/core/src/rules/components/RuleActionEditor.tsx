import React from 'react';
import { styled } from '@material-ui/core/styles';
import {
  coreClassName,
  DropdownField,
  DropdownItem,
  mapBy,
  RuleActionTargetMap,
  useDropdownItems,
  useLoadData,
  useRuleAPI,
  useValueChangeCallback,
} from '../..';

const StyledRoot = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  gap: 10,
});

export interface RuleActionEditorProps {
  className?: string;
  ruleSetId: string;
  value: RuleActionTargetMap;
  onChange: (value: RuleActionTargetMap) => void;
}

const RuleActionEditor: React.FC<RuleActionEditorProps> = ({
  className,
  ruleSetId,
  value,
  onChange,
}) => {
  const ruleAPI = useRuleAPI();
  const { data: actions } = useLoadData(ruleAPI.getActions, [ruleSetId]);
  const { data: targets } = useLoadData(ruleAPI.getTargets, [value.id]);

  const actionOptions = useDropdownItems(actions, 'id', 'display');
  const targetOptions = useDropdownItems(targets, 'id', 'display');

  // easy lookup of action items to determine if an id exists
  const actionsItemsByValue = React.useMemo(
    () =>
      actionOptions.reduce(mapBy('value'), {} as Record<string, DropdownItem>),
    [actionOptions],
  );

  // easy lookup of target items to determine if an id exists
  const targetItemsByValue = React.useMemo(
    () =>
      targetOptions.reduce(mapBy('value'), {} as Record<string, DropdownItem>),
    [targetOptions],
  );

  const onActionChange = useValueChangeCallback(
    React.useCallback(
      (actionId: string) => {
        onChange({
          ...value,
          id: actionId,
        });
      },
      [onChange, value],
    ),
  );

  const onTargetChange = useValueChangeCallback(
    React.useCallback(
      (targetId: string) => {
        onChange({
          ...value,
          targetId,
        });
      },
      [onChange, value],
    ),
  );

  return (
    <StyledRoot className={coreClassName('rule-action-editor', className)}>
      <DropdownField
        label="Take Action"
        value={actionsItemsByValue[value.id] ? value.id : ''}
        items={actionOptions}
        onChange={onActionChange}
      />

      <DropdownField
        label="Select User"
        value={targetItemsByValue[value.targetId ?? ''] ? value.targetId : ''}
        items={targetOptions}
        onChange={onTargetChange}
      />
    </StyledRoot>
  );
};

export default RuleActionEditor;
