import React from 'react';
import { styled } from '@material-ui/core/styles';
import { RuleCondition, useRuleAPI, useRuleDropdownItem } from '..';
import {
  DropdownItem,
  isMatch,
  useEditState,
  useLoadData,
  useSetProp,
} from '../..';
import { RuleConditionDropdown } from '.';

const StyledRoot = styled('div')({
  alignItems: 'center',
  display: 'flex',
  flexDirection: 'row',
  gap: 6,
});

export interface RuleConditionEditorProps {
  className?: string;
  ruleSetId: string;
  value: RuleCondition;
  onChange: (value: RuleCondition) => void;
}

const RuleConditionEditor: React.FC<RuleConditionEditorProps> = ({
  className,
  ruleSetId,
  value,
  onChange,
}) => {
  const [ruleCondition, setRuleCondition] = useEditState<RuleCondition>(value);

  // Helper to set new rule condition and raise onChange event for the new value
  const setRuleConditionAndRaiseOnChange = React.useCallback(
    (ruleCondition: RuleCondition) => {
      setRuleCondition(ruleCondition);
      onChange(ruleCondition);
    },
    [onChange, setRuleCondition],
  );

  const ruleAPI = useRuleAPI();

  const { data: operands } = useLoadData(ruleAPI.getOperands, [ruleSetId]);

  // Once operand has been set, load operators
  const { data: operators } = useLoadData(
    ruleAPI.getOperators,
    [ruleCondition.operand],
    { pause: !ruleCondition.operand },
  );

  // Once operand and operator have been set load values
  const { data: values } = useLoadData(
    ruleAPI.getValues,
    [ruleCondition.operand, ruleCondition.operator],
    { pause: !ruleCondition.operand || !ruleCondition.operator },
  );

  const operandOptions = useRuleDropdownItem(operands);
  const operatorOptions = useRuleDropdownItem(operators);
  const valueOptions = useRuleDropdownItem(values);

  // Create a callback to update a specified property on the ruleCondition
  const onChangeSetRuleConditionProp = useSetProp(
    setRuleConditionAndRaiseOnChange,
    ruleCondition,
  );

  // Whenever operator options change, ensure a valid operator is selected
  React.useEffect(() => {
    if (shouldResetValue(ruleCondition.operator, operatorOptions)) {
      setRuleConditionAndRaiseOnChange({
        ...ruleCondition,
        operator: operatorOptions[0].value,
      });
    }
  }, [operatorOptions, ruleCondition, setRuleConditionAndRaiseOnChange]);

  // Whenever value options change, ensure a valid value is selected
  React.useEffect(() => {
    if (shouldResetValue(ruleCondition.value, valueOptions)) {
      setRuleConditionAndRaiseOnChange({
        ...ruleCondition,
        value: valueOptions[0].value,
      });
    }
  }, [ruleCondition, setRuleConditionAndRaiseOnChange, valueOptions]);

  return (
    <StyledRoot className={className}>
      <RuleConditionDropdown
        variant="operand"
        items={operandOptions ?? []}
        value={value.operand}
        onChange={onChangeSetRuleConditionProp('operand')}
      />
      <RuleConditionDropdown
        variant="operator"
        items={operatorOptions ?? []}
        value={value.operator}
        onChange={onChangeSetRuleConditionProp('operator')}
      />
      <RuleConditionDropdown
        variant="value"
        items={valueOptions ?? []}
        value={value.value}
        onChange={onChangeSetRuleConditionProp('value')}
      />
    </StyledRoot>
  );
};

export default RuleConditionEditor;

/**
 * Determine if value is empty or if it doesn't exist in given options
 */
function shouldResetValue(value: string, options: DropdownItem[]): boolean {
  return (
    (value === '' || !options.find(isMatch('value', value))) &&
    Boolean(options.length)
  );
}
