import React from 'react';
import { styled } from '@material-ui/core/styles';
import { colors, coreClassName } from '../../styles';
import {
  ButtonNormal,
  DataGrid,
  dataGridClassNames,
  DataGridColumn,
  dataGridConstrainCellWidth,
  Icon,
  Rule,
  useModal,
} from '../..';
import { RuleEditorDialog } from '.';
import Checkbox from '@material-ui/core/Checkbox';
import { newRule } from '..';

const StyledRoot = styled('div')({
  ...dataGridConstrainCellWidth(
    '& th.col-active,th.col-order,th.col-actions',
    62,
  ),
  ...dataGridConstrainCellWidth('& th.col-name', 136),
  maxWidth: 800, // TODO: make this more dynamic
  '& td.col-actions': {
    [`& > .${dataGridClassNames.cellContent}`]: {
      justifyContent: 'center',
    },
  },
  '& > button': {
    marginTop: 12,
    '& > .MuiButton-label': {
      gap: 6,
    },
  },
  '& .MuiCheckbox-root': {
    color: colors.checkbox.color,
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
});

export interface RuleDataGridProps {
  className?: string;
  ruleSetId: string;
  rules: Rule[];
  onRuleChange: (rule: Rule) => void;
}

const RuleDataGrid: React.FC<RuleDataGridProps> = ({
  className,
  ruleSetId,
  rules,
  onRuleChange,
}) => {
  const [ruleInEdit, setRuleInEdit] = React.useState<Rule>(newRule);

  const { Modal: RuleEditorModal, openModal: openRuleEditorModal } = useModal(
    RuleEditorDialog,
    ['onSave', 'onCancel'],
  );

  const onCancel = React.useCallback(() => {
    setRuleInEdit(newRule);
  }, []);

  const columns: DataGridColumn<Rule>[] = React.useMemo(() => {
    const onEditClick = (rule: Rule) => {
      return () => {
        setRuleInEdit(rule);
        openRuleEditorModal();
      };
    };

    return [
      {
        className: 'col-active',
        display: 'Active',
        field: (row) => (
          <Checkbox checked={row.isEnabled} readOnly disableRipple={true} />
        ),
      },
      {
        className: 'col-order',
        display: 'Order',
        field: (_row, rowIndex = 0) => <span>{rowIndex + 1}</span>,
      },
      {
        className: 'col-name',
        display: 'Name',
        field: 'name',
      },
      {
        className: 'col-description',
        display: 'Description',
        field: 'description',
      },
      {
        className: 'col-actions',
        display: 'Actions',
        field: (row) => (
          <>
            <Icon icon="edit" onClick={onEditClick(row)} />
          </>
        ),
      },
    ];
  }, [openRuleEditorModal]);

  return (
    <StyledRoot className={coreClassName('rule-data-grid', className)}>
      <DataGrid columns={columns} data={rules} />

      <ButtonNormal onClick={openRuleEditorModal}>
        <Icon icon="add" />
        Rule
      </ButtonNormal>
      <RuleEditorModal
        ruleSetId={ruleSetId}
        value={ruleInEdit}
        onSave={onRuleChange}
        onCancel={onCancel}
      />
    </StyledRoot>
  );
};

export default RuleDataGrid;
