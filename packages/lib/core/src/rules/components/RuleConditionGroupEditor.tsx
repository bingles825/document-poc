import React from 'react';
import { styled } from '@material-ui/core/styles';
import {
  isRuleCondition,
  RuleCondition,
  RuleConditionAddOption,
  RuleConditionGroup,
  RuleConditionGroupOperator,
  ruleConditionGroupOperatorOptions,
} from '../data';
import { coreClassName, Icon } from '../..';
import {
  RuleConditionAddMenu,
  RuleConditionDropdown,
  RuleConditionEditor,
} from '.';

const StyledRoot = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  gap: 6,
  '& > :first-child': {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    gap: 8,
  },
  '& > :not(:first-child)': {
    marginLeft: 16,
  },
});

const StyledCondition = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  gap: 6,
  // We can't use align-items: center since our group conditions need to have
  // the delete button aligned with buttons in the top row.
  '& .wsh-core_c_icon': {
    height: 32, // set icon container height to match button height
    display: 'flex', // causes the SVG to be centered within the container
    alignItems: 'column',
  },
});

export interface RuleConditionGroupEditorProps {
  className?: string;
  ruleSetId: string;
  value: RuleConditionGroup;
  onChange: (value: RuleConditionGroup) => void;
}

const RuleConditionGroupEditor: React.FC<RuleConditionGroupEditorProps> = ({
  className,
  ruleSetId,
  value,
  onChange,
}) => {
  const onGroupOperatorChange = React.useCallback(
    (groupOperator: string) => {
      onChange({
        ...value,
        operator: groupOperator as RuleConditionGroupOperator,
      });
    },
    [onChange, value],
  );

  const onAddCondition = React.useCallback(
    (selectedValue: string) => {
      // condition or group to add
      const newCondition =
        Number(selectedValue) === RuleConditionAddOption['Add Condition']
          ? {
              operand: '',
              operator: '',
              value: '',
            }
          : ({
              operator: 'And',
              conditions: [],
            } as RuleCondition | RuleConditionGroup);

      onChange({
        ...value,
        conditions: [...value.conditions, newCondition],
      });
    },
    [onChange, value],
  );

  const onUpdateCondition =
    (index: number) => (condition: RuleCondition | RuleConditionGroup) => {
      onChange({
        ...value,
        conditions: [
          ...value.conditions.slice(0, index),
          condition,
          ...value.conditions.slice(index + 1),
        ],
      });
    };

  const onDeleteCondition = (index: number) => () => {
    onChange({
      ...value,
      conditions: [
        ...value.conditions.slice(0, index),
        ...value.conditions.slice(index + 1),
      ],
    });
  };

  return (
    <StyledRoot
      className={coreClassName('rule-condition-group-editor', className)}>
      <div>
        <RuleConditionDropdown
          variant="groupOperand"
          items={ruleConditionGroupOperatorOptions}
          value={value.operator}
          onChange={onGroupOperatorChange}
        />
        <RuleConditionAddMenu onSelect={onAddCondition} />
      </div>
      {value.conditions.map((condition, i) => (
        <StyledCondition key={i}>
          <Icon icon="narrowx" onClick={onDeleteCondition(i)} />
          {isRuleCondition(condition) ? (
            <RuleConditionEditor
              key={i}
              ruleSetId={ruleSetId}
              value={condition}
              onChange={onUpdateCondition(i)}
            />
          ) : (
            <RuleConditionGroupEditor
              ruleSetId={ruleSetId}
              value={condition}
              onChange={onUpdateCondition(i)}
            />
          )}
        </StyledCondition>
      ))}
    </StyledRoot>
  );
};

export default RuleConditionGroupEditor;
