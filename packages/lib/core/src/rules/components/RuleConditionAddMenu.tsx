import React from 'react';
import { styled } from '@material-ui/core/styles';
import Menu from '@material-ui/core/Menu';
import { coreClassName } from '../../styles';
import { Icon, ruleConditionAddOptions } from '../..';
import MenuItem from '@material-ui/core/MenuItem';

const StyledRoot = styled('div')({});

export interface RuleConditionAddMenuProps {
  className?: string;
  onSelect: (value: string) => void;
}

const RuleConditionAddMenu: React.FC<RuleConditionAddMenuProps> = ({
  className,
  onSelect,
}) => {
  const anchorRef = React.useRef<HTMLDivElement>(null);
  const [isOpen, setIsOpen] = React.useState(false);

  const onClose = React.useCallback(() => {
    setIsOpen(false);
  }, []);

  const onAnchorClick = React.useCallback(() => {
    setIsOpen(true);
  }, []);

  const onMenuItemClick = (id: string) => () => {
    onClose();
    onSelect(id);
  };

  return (
    <StyledRoot className={coreClassName('anchored-menu', className)}>
      <Icon ref={anchorRef} icon="add" onClick={onAnchorClick} />
      <Menu
        anchorEl={anchorRef.current}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
        transformOrigin={{ vertical: 'top', horizontal: 'center' }}
        getContentAnchorEl={null}
        open={isOpen}
        onClose={onClose}>
        {ruleConditionAddOptions.map(({ id, display }) => (
          <MenuItem key={id} onClick={onMenuItemClick(id)} disableRipple>
            {display}
          </MenuItem>
        ))}
      </Menu>
    </StyledRoot>
  );
};

export default RuleConditionAddMenu;
