export interface User {
  id: string;
  displayName: string;
  firstName: string;
  lastName: string;
  email: string;
  title?: string;
}

/**
 * Build a display name for a user.
 */
export function userDisplayName(user: Partial<User>): string {
  const { title, firstName, lastName } = user;

  return [firstName, lastName, title ? `(${title})` : undefined]
    .filter((token) => token?.length)
    .join(' ');
}

/**
 * Add displayName to a given partial User object.
 * @param user
 */
export function withDisplayName<TUser extends Partial<User>>(
  user: TUser,
): TUser & { displayName: string } {
  return {
    ...user,
    displayName: userDisplayName(user),
  };
}
