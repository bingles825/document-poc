import {
  createExplicitContext,
  fetchJSON,
  sortByString,
  useLoadData,
  User,
  withDisplayName,
} from '../..';
import React from 'react';

/** API for managing document users. */
export class UserAPI {
  constructor(public apiRoot: string) {}

  getUser = async (userId: string): Promise<User> => {
    const raw = await fetchJSON<Omit<User, 'displayName'>>(
      `${this.apiRoot}/GetDocumentUser/${userId}`,
    );

    return withDisplayName(raw);
  };

  getUserList = async (): Promise<User[]> => {
    const raw = await fetchJSON<Omit<User, 'displayName'>[]>(
      `${this.apiRoot}/GetDocumentUserList`,
    );

    return raw.map(withDisplayName);
  };
}

export const { Provider: UserAPIProvider, useContext: useUserAPI } =
  createExplicitContext<UserAPI>('userAPI');

/**
 * Custom hook for retrieving the list of document users sorted by displayName.
 * It also supports an optional filter predicate to slim down the list of users.
 * @param filter
 */
export function useSortedUserList(filter?: (user: User) => boolean): User[] {
  const userAPI = useUserAPI();
  const { data: users } = useLoadData(userAPI.getUserList, []);

  return React.useMemo(
    () =>
      users
        ? users.filter(filter ?? (() => true)).sort(sortByString('displayName'))
        : [],
    [filter, users],
  );
}
