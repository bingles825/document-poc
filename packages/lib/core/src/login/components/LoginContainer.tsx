import React from 'react';
import { styled } from '@material-ui/core/styles';
import { coreClassName, LoginForm, LoginFormData } from '../..';
import { LANDING_URL, useUserSessionService } from '..';
import { useHistory } from 'react-router';

const StyledRoot = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
});

export interface LoginContainerProps {
  className?: string;
}

const LoginContainer: React.FC<LoginContainerProps> = ({ className }) => {
  const userSessionService = useUserSessionService();
  const history = useHistory();

  const onSubmit = React.useCallback(
    async (loginFormData: LoginFormData) => {
      const result = await userSessionService.login(loginFormData);
      if (result === true) {
        setTimeout(() => {
          history.push(LANDING_URL);
        }, 0);
      }
      return result;
    },
    [history, userSessionService],
  );

  return (
    <StyledRoot className={coreClassName('login-container', className)}>
      <LoginForm onSubmit={onSubmit} />
    </StyledRoot>
  );
};

export default LoginContainer;
