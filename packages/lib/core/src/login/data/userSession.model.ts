export interface UserSessionToken {
  token: string;
}

export interface UserSessionTokenError {
  exception: 'Invalid Credentials';
}

export type GetUserSessionTokenResponse =
  | UserSessionToken
  | UserSessionTokenError;

export interface UserSessionCredentials {
  username: string;
  password: string;
}

// TODO: these should probably be configured somewhere else, but TBD
export const LANDING_URL = '/document';
export const LOGIN_URL = '/login';
