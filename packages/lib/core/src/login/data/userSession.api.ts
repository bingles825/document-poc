import {
  UserSessionCredentials,
  UserSessionToken,
  UserSessionTokenError,
} from '.';
import { createExplicitContext, postJSON } from '../..';

export class UserSessionAPI {
  constructor(public apiRoot: string) {}

  getToken = async (
    credentials: UserSessionCredentials,
  ): Promise<UserSessionToken | UserSessionTokenError> => {
    return postJSON(`${this.apiRoot}/Login`, credentials);
  };
}

export const {
  Provider: UserSessionAPIProvider,
  useContext: useUserSessionAPI,
} = createExplicitContext<UserSessionAPI>('userSessionAPI');
