import React from 'react';

export interface UserSession {
  userId: string | null;
}

export type UserSessionContextValue = {
  userId: string | null;
  setUserId: (userId: string | null) => void;
};

const Context = React.createContext<UserSessionContextValue>({
  userId: null,
  setUserId: () => void 0,
});

/**
 * Context provider for a simple user session.
 * TODO: move this into Redux or whatever centralized state solution we go with
 */
export const UserSessionContextProvider: React.FC = ({ children }) => {
  const [userId, setUserId] = React.useState<string | null>(null);

  return (
    <Context.Provider value={{ userId, setUserId }}>
      {children}
    </Context.Provider>
  );
};

export function useUserSession(): UserSessionContextValue {
  return React.useContext(Context);
}
