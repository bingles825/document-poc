import { createExplicitContext } from '../..';
import { LOGIN_URL, UserSessionAPI, UserSessionCredentials } from '..';

export class UserSessionService {
  constructor(private userSessionAPI: UserSessionAPI) {}

  login = async (
    credentials: UserSessionCredentials,
  ): Promise<true | { message: string }> => {
    await this.logout();

    const result = await this.userSessionAPI.getToken(credentials);

    if ('token' in result) {
      sessionStorage.setItem('document_poc_session_token', result.token);
      return true;
    }

    return { message: result.exception };
  };

  // eslint-disable-next-line @typescript-eslint/require-await
  logout = async (): Promise<void> => {
    localStorage.clear(); // TODO: this is needed while we are using mock data but can likely be removed post POC
    sessionStorage.removeItem('document_poc_session_token');
  };
}

export const {
  Provider: UserSessionServiceProvider,
  useContext: useUserSessionService,
} = createExplicitContext<UserSessionService>('userSessionService');

/** Get the session token. */
export function getUserSessionToken(): string | null {
  return sessionStorage.getItem('document_poc_session_token');
}

/** Check if we have a token and if not redirect to login page */
export function ensureSessionToken(): void {
  const token = getUserSessionToken();

  if (!token && window.location.pathname !== LOGIN_URL) {
    window.location.pathname = LOGIN_URL;
  }
}
