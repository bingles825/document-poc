import React from 'react';
import { styled } from '@material-ui/core/styles';
import {
  ButtonNormal,
  DataGrid,
  DataGridColumn,
  useModal,
  useUserSession,
} from '@wsh/core';
import {
  Document,
  DocumentType,
  DocumentStatusIcon,
  docClassName,
  AssignToDialog,
} from '..';

const StyledRoot = styled('div')({});

const StyledActions = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  gap: 3,
  marginTop: 6,
});

export interface DocumentDataGridProps {
  className?: string;
  items: Document[];
  getUserDisplayName: (userId: string) => string;
  onAssignDocuments: (assignToId: string, documentIds: string[]) => void;
}

const DocumentDataGrid: React.FC<DocumentDataGridProps> = ({
  className,
  items,
  getUserDisplayName,
  onAssignDocuments,
}) => {
  const [selectedIdList, setSelectedIdList] = React.useState<string[]>([]);

  const assignToModalTitle = `Assign Document${
    selectedIdList.length > 1 ? 's' : ''
  } to`;

  const { Modal: AssignToMeModal, openModal: openAssignToMeModal } = useModal(
    AssignToDialog,
    ['onCancel', 'onSend'],
  );

  const { Modal: AssignToModal, openModal: openAssignToModal } = useModal(
    AssignToDialog,
    ['onCancel', 'onSend'],
  );

  const userId = useUserSession().userId ?? undefined;

  const onSend = React.useCallback(
    (assigneeId: string) => {
      onAssignDocuments(assigneeId, selectedIdList);
    },
    [onAssignDocuments, selectedIdList],
  );

  /** Column definitions */
  const columns: DataGridColumn<Document>[] = React.useMemo(
    () => [
      {
        display: 'Status',
        field: (item) => <DocumentStatusIcon status={item.status} />,
      },
      {
        display: 'Document #',
        field: 'documentNumber',
      },
      {
        display: 'Document Type',
        field: (item) => <span>{DocumentType[item.documentType]}</span>,
      },
      {
        display: 'Assigned to',
        field: (item) => <span>{getUserDisplayName(item.assignedToId)}</span>,
      },
      {
        display: 'Batch #',
        field: 'batchNumber',
      },
      // {
      //   display: 'Batch Description',
      //   field: 'batchDescription',
      // },
      {
        display: 'Scanned',
        field: 'scanDate',
      },
      {
        display: 'Rules Processed',
        field: 'filedDate',
      },
      {
        display: 'Location',
        field: 'location',
      },
      {
        display: 'Provider',
        field: 'provider',
      },
      {
        display: 'Specialty',
        field: 'specialty',
      },
    ],
    [getUserDisplayName],
  );

  return (
    <StyledRoot className={docClassName('document-data-grid', className)}>
      <DataGrid
        pageSize={12}
        columns={columns}
        data={items}
        enableRowSelection={true}
        onSelectedIdsChange={setSelectedIdList}
        actions={
          <StyledActions>
            <ButtonNormal
              disabled={selectedIdList.length === 0}
              onClick={openAssignToMeModal}>
              Assign to Me
            </ButtonNormal>
            <ButtonNormal
              disabled={selectedIdList.length === 0}
              onClick={openAssignToModal}>
              Assign...
            </ButtonNormal>
          </StyledActions>
        }
      />
      <AssignToMeModal
        title={assignToModalTitle}
        selectedAssigneeId={userId}
        onSend={onSend}
      />
      <AssignToModal title={assignToModalTitle} onSend={onSend} />
    </StyledRoot>
  );
};

export default DocumentDataGrid;
