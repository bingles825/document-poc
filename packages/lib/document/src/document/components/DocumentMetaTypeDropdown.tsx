import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import { Dropdown, useValueChangeCallback } from '@wsh/core';
import { docClassName, MetaTypeDropdownOptions } from '..';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {},
  withTopBorder: {
    borderTop: '1px solid #ccc',
  },
});

export interface DocumentMetaTypeDropdownProps {
  className?: string;
  typeId?: string;
  onTypeIdChange: (typeId: string) => void;
  options: MetaTypeDropdownOptions;
  readOnly?: boolean;
}

/**
 * Dropdown component for selecting a document meta type.
 */
const DocumentMetaTypeDropdown: React.FC<DocumentMetaTypeDropdownProps> = ({
  typeId,
  onTypeIdChange,
  options,
  readOnly,
}) => {
  const classes = useStyles();

  const items = React.useMemo(
    () =>
      [...options.suggested, ...options.derived].sort((a, b) =>
        // alpha sort by name
        a.name.localeCompare(b.name),
      ),
    [options],
  );

  const onChange = useValueChangeCallback(onTypeIdChange);

  return (
    <div className={docClassName('document-meta-type-dropdown', classes.root)}>
      <Dropdown
        readOnly={readOnly}
        value={typeId ?? options.uncategorized.id}
        onChange={onChange}>
        <MenuItem value={options.uncategorized.id}>
          {options.uncategorized.name}
        </MenuItem>
        <MenuItem value="__newid__">New...</MenuItem>
        {items.map(({ id, name }, i) => (
          <MenuItem
            className={i === 0 ? classes.withTopBorder : undefined}
            key={id}
            value={id}>
            {name}
          </MenuItem>
        ))}
      </Dropdown>
    </div>
  );
};

export default DocumentMetaTypeDropdown;
