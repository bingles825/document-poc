import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import {
  ButtonDefault,
  ButtonNormal,
  dialogBase,
  DropdownField,
  InputField,
  InputLabel,
  useEditState,
  User,
  useSortedUserList,
  useUserSession,
  useValueChangeCallback,
} from '@wsh/core';
import { docClassName } from '../..';
import { actionRequiredOptions, DocumentStatus, replyToOptions } from '..';

const useStyles = makeStyles((theme) => {
  const base = dialogBase(theme);

  return {
    ...base,
    root: {
      ...base.root,
      width: 430,
    },
  };
});

export interface OnSendData {
  recipientId: string;
  message: string;
  status: DocumentStatus;
}

export interface SendToDialogProps {
  className?: string;
  returnToId?: string;
  onSend: (data: OnSendData) => void;
  onCancel?: () => void;
}

const SendToDialog: React.FC<SendToDialogProps> = ({
  className,
  returnToId,
  onSend,
  onCancel,
}) => {
  const classes = useStyles();

  const { userId } = useUserSession();
  const title = returnToId ? 'Reply & Return' : 'Send to...';

  const userFilter = React.useMemo(
    () => (user: User) =>
      returnToId ? user.id === returnToId : user.id !== userId,
    [returnToId, userId],
  );

  const usersSorted = useSortedUserList(userFilter);

  const [recipientId, setRecipientId] = useEditState(usersSorted[0]?.id ?? '');

  const targetStatusOptions = returnToId
    ? replyToOptions
    : actionRequiredOptions;

  const targetStatusLabel = returnToId ? 'Response' : 'Action Required';

  const [targetStatusId, setTargetStatusId] = React.useState(
    targetStatusOptions[0].value,
  );

  const [message, setMessage] = React.useState('');

  const onRecipientChange = useValueChangeCallback(setRecipientId);
  const onActionRequiredChange = useValueChangeCallback(setTargetStatusId);
  const onMessageChange = useValueChangeCallback(setMessage);

  const onSubmit = React.useCallback(
    (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();

      onSend({
        recipientId,
        message,
        status: targetStatusId,
      });
    },
    [message, onSend, recipientId, targetStatusId],
  );

  return (
    <form
      className={docClassName('send-to-dialog', classes.root, className)}
      onSubmit={onSubmit}>
      <div className={classes.title}>{title}</div>
      <div className={classes.content}>
        {returnToId ? (
          <div>
            <InputLabel>Return to</InputLabel>
            <span>{usersSorted?.[0]?.displayName}</span>
          </div>
        ) : (
          <DropdownField
            label="Recipient"
            value={recipientId}
            onChange={onRecipientChange}>
            {usersSorted.map(({ id, displayName }) => (
              <MenuItem key={id} value={id}>
                {displayName}
              </MenuItem>
            ))}
          </DropdownField>
        )}

        <DropdownField
          label={targetStatusLabel}
          value={targetStatusId}
          onChange={onActionRequiredChange}>
          {targetStatusOptions.map(({ value, display }) => (
            <MenuItem key={value} value={value}>
              {display}
            </MenuItem>
          ))}
        </DropdownField>

        <InputField
          label="Description"
          multiline
          rows={4}
          value={message}
          onChange={onMessageChange}
        />
      </div>
      <div className={classes.actions}>
        <ButtonDefault
          disabled={message.length === 0}
          enableMinWidth
          type="submit">
          Send
        </ButtonDefault>
        <ButtonNormal enableMinWidth onClick={onCancel}>
          Cancel
        </ButtonNormal>
      </div>
    </form>
  );
};

export default SendToDialog;
