import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Icon } from '@wsh/core';
import { defaultConfidenceThresholds, getMetaScoreIconId } from '..';

const useStyles = makeStyles({
  root: {
    width: 16,
    height: 16,
  },
});

export interface ConfidenceIconProps {
  score: number;
}

/**
 * Icon representing confidence of a meta data prediction.
 */
const ConfidenceIcon: React.FC<ConfidenceIconProps> = ({ score }) => {
  const classes = useStyles();
  const iconId = getMetaScoreIconId(score, defaultConfidenceThresholds);
  return <Icon icon={iconId} className={classes.root} fillColor="auto" />;
};

export default ConfidenceIcon;
