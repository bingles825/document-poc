import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  colors,
  DataGrid,
  dataGridClassNames,
  DataGridColumn,
  dataGridConstrainCellWidth,
  Input,
} from '@wsh/core';
import {
  buildMetaTypeDropdownOptions,
  ConfidenceIcon,
  createGetDataType,
  DocumentMetaTypeDropdown,
  FlagIconToggle,
  Meta,
  MetaTypeMap,
} from '..';
import { docClassName } from '../styles';

const useStyles = makeStyles({
  root: {
    ...dataGridConstrainCellWidth('& th.col-confidence', 16),
    ...dataGridConstrainCellWidth('& th.col-actions', 65),
    '& th': {
      backgroundColor: colors.greys.white,
      fontWeight: 'bold',
    },
    [`& .${dataGridClassNames.cellContent}`]: {
      padding: '0 4px',
      // Hiding Chrome mm/dd/yyyy for demo
      '& input[type=date]:invalid': {
        '&::-webkit-datetime-edit-month-field,&::-webkit-datetime-edit-day-field,&::-webkit-datetime-edit-year-field,&::-webkit-datetime-edit-text':
          {
            color: 'transparent',
          },
      },
    },
    [`& .col-confidence .${dataGridClassNames.cellContent}`]: {
      padding: '0 12px',
    },
    [`& .${docClassName('document-meta-type-dropdown')},.MuiInputBase-root`]: {
      width: '100%',
    },
  },
  icon: {
    width: 16,
    height: 16,
    marginLeft: 4,
    marginRight: 6,
  },
});

export interface DocumentMetaDataGridProps {
  className?: string;
  meta: Meta[];
  metaTypeMap: MetaTypeMap;
  isReadOnly?: boolean;
  onMetaChanged: (meta: Meta) => void;
}

/** Data grid component for editing document meta data. */
const DocumentMetaDataGrid: React.FC<DocumentMetaDataGridProps> = ({
  className,
  meta,
  metaTypeMap,
  isReadOnly,
  onMetaChanged,
}) => {
  const classes = useStyles();

  const getDataTypeForMetaType = React.useMemo(
    () => createGetDataType(metaTypeMap),
    [metaTypeMap],
  );

  // dropdown options
  const metaTypeOptions = React.useMemo(
    () => buildMetaTypeDropdownOptions(metaTypeMap),
    [metaTypeMap],
  );

  const metaSorted = React.useMemo(
    () =>
      [...meta].sort(
        // sorting by lowest score then meta type
        // NOTE: the meta type sorting only works if ids are sorted the way we
        // want in the DB. we'll probably want a stricter alpha sort at some
        // point.
        (a, b) => b.score - a.score || Number(a.typeId) - Number(b.typeId),
      ),
    [meta],
  );

  // Create column definitions for the meta data grid.
  const columns = React.useMemo<DataGridColumn<Meta>[]>(
    () => [
      {
        className: 'col-confidence',
        display: 'Co', // TODO: figure out what to do for this column header
        field: (item) => <ConfidenceIcon score={item.score} />,
      },
      {
        display: 'Field',
        field: (item) => (
          <DocumentMetaTypeDropdown
            readOnly={isReadOnly}
            typeId={item.typeId}
            options={metaTypeOptions}
            onTypeIdChange={(typeId) => onMetaChanged({ ...item, typeId })}
          />
        ),
      },
      {
        display: 'Value',
        field: (item) => {
          const dataType = getDataTypeForMetaType(item.typeId);
          return (
            <>
              {/* {dataType === 'date' && (
                <DatePickerIcon className={classes.icon} />
              )} */}
              <Input
                readOnly={isReadOnly}
                required={true}
                value={item.value}
                type={dataType}
                onChange={(event) =>
                  onMetaChanged({ ...item, value: event.currentTarget.value })
                }
              />
            </>
          );
        },
      },
      {
        className: 'col-actions',
        display: 'Actions',
        field: (item) => (
          <>
            <FlagIconToggle
              isReadOnly={isReadOnly}
              isFlagged={item.isFlagged}
              onChangeIsFlagged={(isFlagged) =>
                onMetaChanged({ ...item, isFlagged })
              }
            />
          </>
        ),
      },
    ],
    [getDataTypeForMetaType, isReadOnly, metaTypeOptions, onMetaChanged],
  );

  return (
    <DataGrid
      className={docClassName(
        'document-meta-data-grid',
        classes.root,
        className,
      )}
      pageSize={15}
      columns={columns}
      data={metaSorted}
    />
  );
};

export default DocumentMetaDataGrid;
