import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import {
  ButtonDefault,
  ButtonNormal,
  dialogBase,
  DropdownField,
  useEditState,
  useSortedUserList,
  useValueChangeCallback,
} from '@wsh/core';
import { docClassName } from '..';

const useStyles = makeStyles((theme) => {
  const base = dialogBase(theme);

  return {
    ...base,
    root: {
      ...base.root,
      width: 280,
    },
  };
});

export interface AssignToDialogProps {
  className?: string;
  title: string;
  selectedAssigneeId?: string;
  onSend: (assigneeId: string) => void;
  onCancel?: () => void;
}

/**
 * Dialog for assigning things to a user.
 */
const AssignToDialogProps: React.FC<AssignToDialogProps> = ({
  className,
  title,
  selectedAssigneeId,
  onCancel,
  onSend,
}) => {
  const classes = useStyles();

  const usersSorted = useSortedUserList();
  const [assigneeId, setAssigneeId] = useEditState(
    selectedAssigneeId ?? usersSorted[0]?.id ?? '',
  );
  const onAssigneeChange = useValueChangeCallback(setAssigneeId);

  const onSubmit = React.useCallback(
    (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();
      onSend(assigneeId);
    },
    [assigneeId, onSend],
  );

  return (
    <form
      className={docClassName('assign-to-dialog', classes.root, className)}
      onSubmit={onSubmit}>
      <div className={classes.title}>{title}</div>
      <div className={classes.content}>
        <DropdownField
          label="Assignee"
          value={usersSorted.length ? assigneeId : ''}
          onChange={onAssigneeChange}>
          {usersSorted.map(({ id, displayName }) => (
            <MenuItem key={id} value={id}>
              {displayName}
            </MenuItem>
          ))}
        </DropdownField>
      </div>
      <div className={classes.actions}>
        <ButtonDefault enableMinWidth type="submit">
          Send
        </ButtonDefault>
        <ButtonNormal enableMinWidth onClick={onCancel}>
          Cancel
        </ButtonNormal>
      </div>
    </form>
  );
};

export default AssignToDialogProps;
