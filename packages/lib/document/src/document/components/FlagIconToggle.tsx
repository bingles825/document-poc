import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { colors, FlagIcon } from '@wsh/core';

const useStyles = makeStyles({
  root: ({ isFlagged, isReadOnly }: FlagIconToggleProps) => ({
    backgroundColor: isFlagged
      ? colors.controls.normal.selected.background
      : undefined,
    borderRadius: 4,
    cursor: isReadOnly ? undefined : 'pointer',
    display: 'flex',
    padding: 4,
    '&:hover': isReadOnly
      ? undefined
      : {
          backgroundColor: colors.controls.normal.hover.background,
        },
  }),
  svg: {
    width: 16,
    height: 16,
  },
});

export interface FlagIconToggleProps {
  isFlagged?: boolean;
  onChangeIsFlagged: (isFlagged: boolean) => void;
  isReadOnly?: boolean;
}

const FlagIconToggle: React.FC<FlagIconToggleProps> = (props) => {
  const { isFlagged, isReadOnly, onChangeIsFlagged } = props;
  const classes = useStyles(props);

  const onClick = React.useCallback(() => {
    if (isReadOnly) {
      return;
    }

    onChangeIsFlagged(!isFlagged);
  }, [isFlagged, isReadOnly, onChangeIsFlagged]);

  return (
    <div
      className={classes.root}
      onClick={onClick}
      title={isFlagged ? 'Unflag' : 'Flag for Review'}>
      <FlagIcon className={classes.svg} />
    </div>
  );
};

export default FlagIconToggle;
