import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { accentedLabel, colors, Header2, Header3 } from '@wsh/core';
import { docClassName, Document } from '../..';
import { DocumentStatus, DocumentType } from '..';

const useStyles = makeStyles({
  root: ({ isSelected }: DocumentQueueItemProps) => ({
    display: 'flex',
    flexDirection: 'row',
    gap: 6,
    borderRadius: 8,
    border: `1px solid ${colors.border.normal}`,
    height: 132,
    padding: 10,
    backgroundColor: isSelected
      ? colors.controls.default.selected.background
      : colors.greys.white,
    borderColor: isSelected
      ? colors.controls.default.selected.background
      : colors.greys.white,
    '&:hover': {
      backgroundColor: colors.controls.standard_hover,
      borderColor: colors.controls.standard_hover,
      cursor: 'pointer',
    },
  }),
  thumbnail: {
    border: `1px solid ${colors.border.normal}`,
    width: 82,
  },
  main: {
    overflow: 'hidden',
  },
  label: {
    ...accentedLabel,
  },
});

export interface DocumentQueueItemProps {
  className?: string;
  document: Document;
  isSelected?: boolean;
  onClick: () => void;
}

const DocumentQueueItem: React.FC<DocumentQueueItemProps> = (props) => {
  const classes = useStyles(props);

  const { className, document, onClick } = props;

  return (
    <div
      className={docClassName('document-queue-item', classes.root, className)}
      onClick={onClick}>
      <img className={classes.thumbnail} src="/pdf/thumbnail.png" />
      <div className={classes.main}>
        <Header2>{document.documentNumber}</Header2>
        <Header3>{DocumentType[document.documentType]}</Header3>
        <span className={classes.label}>
          Status: {DocumentStatus[document.status]}
        </span>
      </div>
    </div>
  );
};

export default DocumentQueueItem;
