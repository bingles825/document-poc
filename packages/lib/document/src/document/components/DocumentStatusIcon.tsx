import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { colors, Icon, IconProps } from '@wsh/core';
import { DocumentStatus } from '../data/document.model';

// Map of status to icon props
const statusIconProps: Record<
  DocumentStatus,
  Pick<IconProps, 'icon' | 'fillColor'>
> = {
  [DocumentStatus.Completed]: {
    icon: 'check',
    fillColor: colors.theme.success,
  },
  [DocumentStatus['In Progress']]: {
    icon: 'locked',
    fillColor: colors.branding.wsh_blue,
  },
  [DocumentStatus['Not Started']]: {
    icon: 'pending',
    fillColor: 'transparent',
  },
  [DocumentStatus['On Hold']]: {
    icon: 'alertround',
    fillColor: colors.theme.warning,
  },
  [DocumentStatus['Needs Review']]: {
    icon: 'alertround',
    fillColor: colors.theme.warning,
  },
} as const;

const useStyles = makeStyles({
  root: {
    display: 'flex',
  },
  icon: ({ status }: DocumentStatusIconProps) => ({
    marginRight: 8,
    '& circle:not(:first-child)':
      status === DocumentStatus['Not Started']
        ? {
            fill: colors.branding.wsh_blue,
          }
        : undefined,
  }),
});

export interface DocumentStatusIconProps {
  status: DocumentStatus;
}

const DocumentStatusIcon: React.FC<DocumentStatusIconProps> = (props) => {
  const classes = useStyles(props);

  const iconProps = statusIconProps[props.status];

  return (
    <div className={classes.root}>
      <Icon className={classes.icon} {...iconProps} />
      {DocumentStatus[props.status]}
    </div>
  );
};

export default DocumentStatusIcon;
