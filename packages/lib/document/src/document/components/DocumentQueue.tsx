import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { colors, DropdownField, useValueChangeCallback } from '@wsh/core';
import { docClassName, Document } from '../..';
import MenuItem from '@material-ui/core/MenuItem';
import { DocumentQueueItem } from '.';

// When we want a scrollable area to fill the remaining space in a flex container,
// there can be issues if the height is larger than the available space. The
// methodology here is to:
// 1. set flex-basis: 0 as a starting place instead of the height of the content
// 2. set flex-grow: 1 so that the item can grow into the available space
// 3. set overflow-y: auto so that it doesn't keep growing
const fillFlexSpaceAutoOverflow = {
  flexBasis: 0, // this is important so that the content height doesn't impact the height of the container
  flexGrow: 1,
  overflowY: 'auto',
} as const;

const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  header: {
    backgroundColor: colors.panel.light.backgroundColor,
    height: 72,
    flex: 'none',
    padding: '10px 8px 0',
  },
  main: {
    ...fillFlexSpaceAutoOverflow,
    display: 'flex',
    flexDirection: 'column',
    overflowY: 'auto',
    padding: 4,
    gap: 8,
    '& > *': {
      flex: 'none',
    },
  },
  footer: {
    backgroundColor: colors.panel.light.backgroundColor,
    height: 50,
    flex: 'none',
  },
});

enum AssignedToFilter {
  'Me' = 1,
  'Me or Unassigned' = 2,
}

const assignedToFilterOptions = [
  AssignedToFilter.Me,
  AssignedToFilter['Me or Unassigned'],
] as const;

export interface DocumentQueueProps {
  className?: string;
  documents: Document[] | null;
  selectedDocument: Document | null;
  onSelectDocument: (document: Document) => void;
  userId: string;
}

const DocumentQueue: React.FC<DocumentQueueProps> = ({
  className,
  documents,
  selectedDocument,
  onSelectDocument,
  userId,
}) => {
  const classes = useStyles();

  const [assignedToFilter, setAssignedToFilter] = React.useState(
    AssignedToFilter.Me,
  );

  React.useEffect(() => {
    setAssignedToFilter(AssignedToFilter.Me);
  }, [userId]);

  const onAssignedToChange = useValueChangeCallback(setAssignedToFilter);

  const documentsFiltered = React.useMemo(
    () =>
      documents
        ? documents.filter(
            ({ assignedToId }) =>
              assignedToId === userId ||
              (assignedToFilter === AssignedToFilter['Me or Unassigned'] &&
                assignedToId === ''),
          )
        : null,
    [assignedToFilter, documents, userId],
  );

  return (
    <div className={docClassName('document-queue', classes.root, className)}>
      <div className={classes.header}>
        <DropdownField
          label="Assigned to"
          value={assignedToFilter}
          onChange={onAssignedToChange}>
          {assignedToFilterOptions.map((assignedTo) => (
            <MenuItem key={assignedTo} value={assignedTo}>
              {AssignedToFilter[assignedTo]}
            </MenuItem>
          ))}
        </DropdownField>
      </div>
      <div className={classes.main}>
        {documentsFiltered &&
          documentsFiltered.map((document) => (
            <DocumentQueueItem
              key={document.id}
              document={document}
              isSelected={document.id === selectedDocument?.id}
              onClick={() => onSelectDocument(document)}
            />
          ))}
      </div>
      <div className={classes.footer}></div>
    </div>
  );
};

export default DocumentQueue;
