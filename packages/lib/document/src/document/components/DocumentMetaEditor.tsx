import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import {
  accentedLabel,
  ButtonDefault,
  ButtonNormal,
  colors,
  coreClassName,
  DropdownField,
  Header2,
  Icon,
  IconButton,
  IconID,
  SendToIcon,
  useEditListState,
  useEditState,
  useLoadingOverlay,
  useModal,
  useValueChangeCallback,
} from '@wsh/core';
import { DocumentMetaDataGrid, SendToDialog } from '.';
import {
  buildMetaTypeMap,
  docClassName,
  Document,
  DocumentStatus,
  DocumentType,
  documentTypeOptions,
  getMetaScoreIconId,
  Meta,
  MetaTypeConfig,
  useIsDirtyState,
} from '..';
import { OnSendData } from './SendToDialog';
import { DocumentMessage, DocumentMessageHistory } from '../..';

const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  header: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    '& > :nth-child(2)': {
      flexGrow: 1,
      marginLeft: 12,
    },
    '& > :nth-last-child(2)': {
      ...accentedLabel,
      width: 80,
      textAlign: 'right',
      marginRight: 6,
    },
    '& > :nth-last-child(1)': {
      color: colors.theme.success,
    },
  },
  filters: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    gap: 3,
    [`& .${coreClassName('icon-button')}`]: {
      // TODO: the color will need to be dynamic once we actually support toggling filters
      backgroundColor: colors.controls.normal.selected.background,
      height: 42,
      width: 60,
      '& .MuiButton-label': {
        gap: 3,
      },
    },
  },
  main: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
    gap: 9,
    padding: 12,
  },
  documentType: {
    width: 195,
  },
  messages: {},
  footer: {
    backgroundColor: colors.greys.gray1,
    display: 'flex',
    padding: '6px 6px 12px',
    justifyContent: 'flex-end',
    '& button': {
      marginLeft: 6,
      whiteSpace: 'nowrap',
    },
  },
  icon: {
    width: 16,
    height: 16,
    marginRight: 6,
  },
});

export interface DocumentMetaEditorProps {
  className?: string;
  config: MetaTypeConfig;
  document: Document;
  messages: DocumentMessage[];
  meta: Meta[];
  onSaveMeta: (meta: Meta[]) => Promise<unknown>;
  onSaveDocument: (document: Document) => Promise<unknown>;
  onCreateDocumentMessage: (
    documentMessage: Omit<DocumentMessage, 'fromId'>,
  ) => Promise<unknown>;
}

const DocumentMetaEditor: React.FC<DocumentMetaEditorProps> = ({
  className,
  config,
  document,
  messages,
  meta,
  onSaveMeta,
  onSaveDocument,
  onCreateDocumentMessage,
}) => {
  const classes = useStyles();
  const { Modal: SendToModal, openModal: openSendToModal } = useModal(
    SendToDialog,
    ['onCancel', 'onSend'],
  );

  const metaTypeMap = React.useMemo(() => buildMetaTypeMap(config), [config]);

  const {
    dirtyState: { isDirty, isDocumentDirty, isMetaDirty, dirtyMetaIds },
    resetDirtyState,
    setDirtyDocument,
    setDirtyMetaId,
  } = useIsDirtyState();

  const [documentEdit, setDocumentEdit, resetDocumentEdit] =
    useEditState(document);

  const isInReview = documentEdit.status === DocumentStatus['Needs Review'];

  const metaInit = React.useMemo(
    () => (isInReview ? meta.filter(({ isFlagged }) => isFlagged) : meta),
    [isInReview, meta],
  );

  const { list, setItem, resetList } = useEditListState(metaInit);

  const filterCounts = React.useMemo(
    () =>
      list.reduce((filterCounts, item) => {
        const iconId = getMetaScoreIconId(item.score);
        filterCounts[iconId] = (filterCounts[iconId] ?? 0) + 1;
        if (item.isFlagged) {
          filterCounts.flag = (filterCounts.flag ?? 0) + 1;
        }
        return filterCounts;
      }, {} as Record<IconID, number>),
    [list],
  );

  const returnToId = isInReview
    ? messages[messages.length - 1]?.fromId
    : undefined;

  const canSave = isDirty;
  const canMarkComplete =
    !isDirty && documentEdit.status !== DocumentStatus.Completed;
  const canSendTo = !isDirty;

  const { showLoadingUntilResolved } = useLoadingOverlay();

  const onDocumentTypeChange = useValueChangeCallback(
    React.useCallback(
      (documentType: DocumentType) => {
        setDocumentEdit((documentEdit) => ({
          ...documentEdit,
          documentType,
          status: DocumentStatus['In Progress'],
        }));
        setDirtyDocument();
      },
      [setDirtyDocument, setDocumentEdit],
    ),
  );

  const onMetaChanged = React.useCallback(
    (item: Meta) => {
      setItem(item);
      setDirtyMetaId(item.id);
      if (documentEdit.status !== DocumentStatus['In Progress']) {
        setDocumentEdit({
          ...documentEdit,
          status: DocumentStatus['In Progress'],
        });
        setDirtyDocument();
      }
    },
    [documentEdit, setDirtyDocument, setDirtyMetaId, setDocumentEdit, setItem],
  );

  const onSaveClick = React.useCallback(() => {
    if (isMetaDirty) {
      const dirtyItems = list.filter((item) => dirtyMetaIds[item.id]);
      void showLoadingUntilResolved(onSaveMeta(dirtyItems));
    }

    if (isDocumentDirty) {
      void showLoadingUntilResolved(onSaveDocument(documentEdit));
    }

    resetDirtyState();
  }, [
    isMetaDirty,
    isDocumentDirty,
    resetDirtyState,
    list,
    showLoadingUntilResolved,
    onSaveMeta,
    dirtyMetaIds,
    onSaveDocument,
    documentEdit,
  ]);

  const onRevertClick = React.useCallback(() => {
    resetDocumentEdit();
    resetList();
    resetDirtyState();
  }, [resetDocumentEdit, resetDirtyState, resetList]);

  const onSend = React.useCallback(
    (data: OnSendData) => {
      const newDocumentEdit = {
        ...documentEdit,
        assignedToId: data.recipientId,
        status: data.status,
      };

      setDocumentEdit(newDocumentEdit);
      resetDirtyState();
      void showLoadingUntilResolved(onSaveDocument(newDocumentEdit));

      void onCreateDocumentMessage({
        documentId: documentEdit.id,
        subject: DocumentStatus[data.status],
        toId: data.recipientId,
        content: data.message,
        createdAt: new Date().toISOString(),
      });
    },
    [
      documentEdit,
      onCreateDocumentMessage,
      onSaveDocument,
      resetDirtyState,
      setDocumentEdit,
      showLoadingUntilResolved,
    ],
  );

  const onMarkCompleteClick = React.useCallback(() => {
    const newDocumentEdit = {
      ...documentEdit,
      status: DocumentStatus.Completed,
    };

    setDocumentEdit(newDocumentEdit);
    resetDirtyState();
    void showLoadingUntilResolved(onSaveDocument(newDocumentEdit));
  }, [
    documentEdit,
    onSaveDocument,
    resetDirtyState,
    setDocumentEdit,
    showLoadingUntilResolved,
  ]);

  return (
    <div
      className={docClassName('document-meta-editor', classes.root, className)}>
      <div className={classes.main}>
        <div className={classes.header}>
          <Icon
            title="Document Confidence"
            icon="kpineutral"
            size={24}
            fillColor="auto"
          />
          <Header2>{documentEdit.documentNumber}</Header2>

          <span>Document Confidence</span>
          <Typography variant="h1" component="span">
            TBD
          </Typography>
        </div>
        <DropdownField
          className={classes.documentType}
          readOnly={isInReview}
          label="Document Type"
          value={documentEdit.documentType}
          onChange={onDocumentTypeChange}>
          {documentTypeOptions.map(({ display, value }) => (
            <MenuItem key={value} value={value}>
              {display}
            </MenuItem>
          ))}
        </DropdownField>
        <div className={classes.filters}>
          <IconButton icon="kpineutral" size={24} fillColor="auto">
            {filterCounts.kpineutral ?? 0}
          </IconButton>
          <IconButton icon="kpiwarning" size={24} fillColor="auto">
            {filterCounts.kpiwarning ?? 0}
          </IconButton>
          <IconButton icon="kpibad" size={24} fillColor="auto">
            {filterCounts.kpibad ?? 0}
          </IconButton>
          <IconButton icon="kpiquestion" size={24} fillColor="auto">
            {filterCounts.kpiquestion ?? 0}
          </IconButton>
          <IconButton icon="flag" size={24} fillColor="auto">
            {filterCounts.flag ?? 0}
          </IconButton>
        </div>
        {list.length ? (
          <DocumentMetaDataGrid
            isReadOnly={isInReview}
            meta={list}
            metaTypeMap={metaTypeMap}
            onMetaChanged={onMetaChanged}
          />
        ) : null}
      </div>
      <div className={classes.messages}>
        {messages.length ? (
          <DocumentMessageHistory messages={messages} />
        ) : null}
      </div>
      <div className={classes.footer}>
        {isInReview ? null : (
          <>
            <ButtonNormal
              enableMinWidth
              disabled={!isDirty}
              onClick={onRevertClick}>
              Revert
            </ButtonNormal>
            <ButtonNormal
              enableMinWidth
              disabled={!canSave}
              onClick={onSaveClick}>
              Save
            </ButtonNormal>

            <ButtonNormal
              enableMinWidth
              disabled={!canSendTo}
              onClick={openSendToModal}>
              <SendToIcon className={classes.icon} />
              Send to...
            </ButtonNormal>
          </>
        )}

        <ButtonDefault
          enableMinWidth
          disabled={!canMarkComplete}
          onClick={isInReview ? openSendToModal : onMarkCompleteClick}>
          {isInReview ? 'Reply & Return' : 'Mark Complete'}
        </ButtonDefault>
      </div>

      <SendToModal returnToId={returnToId} onSend={onSend} />
    </div>
  );
};

export default DocumentMetaEditor;
