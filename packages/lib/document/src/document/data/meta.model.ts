import { IconID } from '@wsh/core';

export type DataType =
  | 'doctype'
  | 'mrn'
  | 'date'
  | 'number'
  | 'text'
  | 'provider'
  | 'location';

export type ConfidenceThresholds = Record<
  'perfect' | 'good' | 'warn' | 'error' | 'uncategorized',
  number
>;

// TODO: This will probably need to come from configuration instead of a const
// eventually
export const defaultConfidenceThresholds: ConfidenceThresholds = {
  perfect: 100,
  good: 75,
  warn: 50,
  error: 25,
  uncategorized: 0,
};

export interface MetaType {
  id: string;
  name: string;
  dataType: DataType;
}

export interface MetaTypeConfig {
  uncategorizedTypeId: string;
  suggestedTypeIds: string[];
  types: MetaType[];
}

export interface Meta {
  id: string;
  documentId: string;
  typeId: string;
  value: string;
  score: number;
  isFlagged?: boolean;
}

/** Map of different meta types. */
export interface MetaTypeMap {
  uncategorized: MetaType;
  derived: Record<string, MetaType>;
  suggested: Record<string, MetaType>;
}

/** Datastructure for meta type dropdowns. */
export interface MetaTypeDropdownOptions {
  uncategorized: MetaType;
  derived: MetaType[];
  suggested: MetaType[];
}

/**
 * Build a map of different meta types from a meta type config.
 */
export function buildMetaTypeMap(config: MetaTypeConfig): MetaTypeMap {
  let uncategorized: MetaType | undefined = undefined;
  const derived: Record<string, MetaType> = {};
  const suggested: Record<string, MetaType> = {};

  for (const type of config.types) {
    if (type.id === config.uncategorizedTypeId) {
      uncategorized = type;
    } else if (config.suggestedTypeIds.includes(type.id)) {
      suggested[type.id] = type;
    } else {
      derived[type.id] = type;
    }
  }

  if (!uncategorized) {
    throw new Error(`No 'Uncategorized' meta type was found`);
  }

  return {
    uncategorized,
    derived,
    suggested,
  };
}

/**
 * Build a data model for a meta type dropdown.
 */
export function buildMetaTypeDropdownOptions(
  metaTypeMap: MetaTypeMap,
): MetaTypeDropdownOptions {
  return {
    uncategorized: metaTypeMap.uncategorized,
    suggested: Object.values(metaTypeMap.suggested),
    derived: Object.values(metaTypeMap.derived),
  };
}

/**
 * Factory function that creates a function that can get a data type for a meta
 * type.
 */
export function createGetDataType(metaTypeMap: MetaTypeMap) {
  /**
   * Get the data type for a meta type.
   */
  return function getDataType(metaTypeId: string): DataType {
    let metaType: MetaType | undefined = undefined;

    if (metaTypeId === metaTypeMap.uncategorized.id) {
      metaType = metaTypeMap.uncategorized;
    } else if (metaTypeId in metaTypeMap.suggested) {
      metaType = metaTypeMap.suggested[metaTypeId];
    } else if (metaTypeId in metaTypeMap.derived) {
      metaType = metaTypeMap.derived[metaTypeId];
    }

    return metaType?.dataType ?? 'text';
  };
}

export function getMetaScoreIconId(
  score: number,
  thresholds: ConfidenceThresholds = defaultConfidenceThresholds,
): IconID {
  if (score >= thresholds.perfect) {
    return 'kpigood';
  }

  if (score >= thresholds.good) {
    return 'kpineutral';
  }

  if (score >= thresholds.warn) {
    return 'kpiwarning';
  }

  if (score >= thresholds.error) {
    return 'kpibad';
  }

  return 'kpiquestion';
}
