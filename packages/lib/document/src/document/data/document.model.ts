import { numericEnumValues, sortByString } from '@wsh/core';

export enum DocumentStatus {
  'Not Started' = 1,
  'In Progress' = 2,
  'Completed' = 3,
  'On Hold' = 4,
  'Needs Review' = 5,
}

export enum DocumentType {
  'Allergy Testing' = 1,
  'Chart Note' = 2,
  'Consent Form' = 3,
  'CT Scan' = 4,
  'EEG' = 5,
  'EKG' = 6,
  'External Medical Records' = 7,
  'Hosp Consult' = 8,
  'Lab/Study/Test Reports' = 9,
  'Misc Correspondence' = 10,
  'Pathology Report' = 11,
  'Rx Refill' = 12,
}

export interface Document {
  id: string;
  assignedToId: string;
  batchDescription: string;
  batchNumber: string;
  documentNumber: string;
  documentPath: string;
  documentType: DocumentType;
  filedDate: string;
  location: string;
  provider: string;
  scanDate: string;
  specialty: string;
  status: DocumentStatus;
}

export const actionRequiredOptions = [
  {
    display: 'Review Flagged Fields',
    value: DocumentStatus['Needs Review'],
  },
] as const;

export const replyToOptions = [
  {
    display: 'Complete',
    value: DocumentStatus['In Progress'],
  },
] as const;

export const documentTypeOptions = numericEnumValues(DocumentType)
  .map((value) => ({
    display: DocumentType[value],
    value,
  }))
  .sort(sortByString('display'));
