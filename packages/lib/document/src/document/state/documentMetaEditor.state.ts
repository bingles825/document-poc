import React from 'react';

export interface DirtyState {
  isDirty: boolean;
  isDocumentDirty: boolean;
  isMetaDirty: boolean;
  dirtyMetaIds: Record<string, boolean>;
}

const initialDirtyState: DirtyState = {
  isDirty: false,
  isDocumentDirty: false,
  isMetaDirty: false,
  dirtyMetaIds: {},
};

export interface UseIsDirtyStateResult {
  dirtyState: DirtyState;
  resetDirtyState: () => void;
  setDirtyDocument: () => void;
  setDirtyMetaId: (metaId: string) => void;
}

/**
 * Custom hook for tracking edits in document meta editor component.
 */
export function useIsDirtyState(): UseIsDirtyStateResult {
  const [dirtyState, setIsDirtyState] = React.useState(initialDirtyState);

  const resetDirtyState = React.useCallback(() => {
    setIsDirtyState(initialDirtyState);
  }, []);

  const setDirtyDocument = React.useCallback(() => {
    setIsDirtyState((isDirtyState) => ({
      ...isDirtyState,
      isDirty: true,
      isDocumentDirty: true,
    }));
  }, []);

  const setDirtyMetaId = React.useCallback((metaId: string) => {
    setIsDirtyState((isDirtyState) => ({
      ...isDirtyState,
      isDirty: true,
      isMetaDirty: true,
      dirtyMetaIds: {
        ...isDirtyState.dirtyMetaIds,
        [metaId]: true,
      },
    }));
  }, []);

  return {
    dirtyState,
    resetDirtyState,
    setDirtyDocument,
    setDirtyMetaId,
  };
}
