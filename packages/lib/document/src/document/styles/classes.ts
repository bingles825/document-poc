import { createClassName } from '@wsh/core';

/**
 * Create component css className string for document lib.
 */
export const docClassName = createClassName('doc');
