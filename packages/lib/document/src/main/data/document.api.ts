import {
  createExplicitContext,
  fetchJSON,
  postJSON,
  propsToType,
} from '@wsh/core';
import { Document, DocumentStatus } from '../..';

// TODO: fix this in the backend API
const propsToString = propsToType(
  (value: unknown) => (value === 0 ? '' : String(value)),
  'id',
  'assignedToId',
);

export class DocumentAPI {
  constructor(public apiRoot: string) {}

  getDocumentList = async (): Promise<Document[]> => {
    const raw = await fetchJSON<Document[]>(`${this.apiRoot}/GetDocumentList`);

    return raw.map((doc) =>
      propsToString({
        ...doc,
        documentPath:
          doc.documentPath ?? `${this.apiRoot}/GetDocumentImage/${doc.id}`, // `/api/WSC/v1/GetPDF/${doc.id}`
        status: doc.status ?? DocumentStatus['Not Started'],
      }),
    );
  };

  saveDocument = async (document: Document): Promise<Document> => {
    return postJSON(`${this.apiRoot}/SaveDocument`, document);
  };
}

export const { Provider: DocumentAPIProvider, useContext: useDocumentAPI } =
  createExplicitContext<DocumentAPI>('documentAPI');
