import { createExplicitContext, fetchJSON, postJSON } from '@wsh/core';
import { DocumentMessage } from '../..';

export class DocumentMessageAPI {
  constructor(public apiRoot: string) {}

  getMessageList = async (documentId: string): Promise<DocumentMessage[]> => {
    return fetchJSON(`${this.apiRoot}/GetDocumentMessageList/${documentId}`);
  };

  saveMessage = async (message: DocumentMessage): Promise<DocumentMessage> => {
    return postJSON(`${this.apiRoot}/SaveDocumentMessage`, message);
  };
}

export const {
  Provider: DocumentMessageAPIProvider,
  useContext: useDocumentMessageAPI,
} = createExplicitContext<DocumentMessageAPI>('documentMessageAPI');
