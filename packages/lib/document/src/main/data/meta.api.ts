import {
  createExplicitContext,
  fetchJSON,
  postJSON,
  propsToType,
} from '@wsh/core';
import { Meta, MetaTypeConfig } from '../..';

// TODO: Fix this on the backend API
const propsToString = propsToType(
  (value: unknown) => (value === 0 ? '' : String(value)),
  'id',
  'documentId',
  'typeId',
);

export class MetaAPI {
  constructor(public apiRoot: string) {}

  getMetaConfig = async (): Promise<MetaTypeConfig> => {
    return fetchJSON(`${this.apiRoot}/GetMetaTypeConfig`);
  };

  getMeta = async (documentId: string): Promise<Meta[]> => {
    const raw = await fetchJSON<Meta[]>(
      `${this.apiRoot}/GetMetaList/${documentId}`,
    );
    return raw.map(propsToString);
  };

  saveMetaList = async (metaList: Meta[]): Promise<Meta[]> => {
    const promises = metaList.map((meta) =>
      postJSON<Meta>(`${this.apiRoot}/SaveMetaData`, meta),
    );

    return await Promise.all(promises);
  };
}

export const { Provider: MetaAPIProvider, useContext: useMetaAPI } =
  createExplicitContext<MetaAPI>('metaAPI');
