import React from 'react';
import { styled } from '@material-ui/core/styles';
import { Header1, useLoadData, useUserAPI } from '@wsh/core';
import { docClassName, DocumentDataGrid } from '../..';
import { useDocumentAPI } from '..';

const StyledRoot = styled('div')({
  padding: 20,
  '& .wsh-doc_c_document-data-grid': {
    marginTop: 12,
  },
});

export interface DocumentsContainerProps {
  className?: string;
}

const DocumentsContainer: React.FC<DocumentsContainerProps> = ({
  className,
}) => {
  const docAPI = useDocumentAPI();
  const userAPI = useUserAPI();

  const { data: documentList, reload: reloadDocumentList } = useLoadData(
    docAPI.getDocumentList,
    [],
  );

  const { data: userList } = useLoadData(userAPI.getUserList, []);

  // TODO: we'll probably eventually want to pull the user display names as part
  // of the datagrid data from the backend instead of joining data in the frontend
  const getUserDisplayName = React.useCallback(
    (userId: string) => {
      return (
        userList?.find(({ id }) => id === userId)?.displayName ?? 'Unassigned'
      );
    },
    [userList],
  );

  const onAssignDocuments = React.useCallback(
    async (assigneeId: string, documentIds: string[]) => {
      // Build a list of docs to re-assign
      const toSave = documentList
        ?.filter(({ id }) => documentIds.includes(id))
        .map((doc) => ({
          ...doc,
          assignedToId: assigneeId,
        }));

      if (!toSave) {
        return;
      }

      await Promise.all(toSave.map(docAPI.saveDocument));
      await reloadDocumentList();
    },
    [docAPI.saveDocument, documentList, reloadDocumentList],
  );

  return (
    <StyledRoot className={docClassName('documents', className)}>
      <Header1>Documents</Header1>
      <DocumentDataGrid
        items={documentList ?? []}
        getUserDisplayName={getUserDisplayName}
        onAssignDocuments={onAssignDocuments}
      />
    </StyledRoot>
  );
};

export default DocumentsContainer;
