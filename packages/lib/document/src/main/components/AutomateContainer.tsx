import React from 'react';
import { styled } from '@material-ui/core/styles';
import {
  Header1,
  Rule,
  RuleDataGrid,
  useLoadData,
  useRuleAPI,
} from '@wsh/core';
import { docClassName } from '../..';

const StyledRoot = styled('div')({
  padding: 20,
  '& .wsh-core_c_rule-data-grid': {
    marginTop: 12,
  },
});

// TODO: get this from somewhere real
const ruleSetId = 'mock-document-ruleset-id';

export interface AutomateContainerProps {
  className?: string;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const emptyArray: any[] = [];

const AutomateContainer: React.FC<AutomateContainerProps> = ({ className }) => {
  const ruleAPI = useRuleAPI();
  const { data: rules, reload: reloadRules } = useLoadData(ruleAPI.getRules, [
    ruleSetId,
  ]);

  const onRuleChange = React.useCallback(
    async (rule: Rule) => {
      await ruleAPI.saveRule(ruleSetId, rule);
      await reloadRules(ruleSetId);
    },
    [reloadRules, ruleAPI],
  );

  return (
    <StyledRoot className={docClassName('automate-container', className)}>
      <Header1>Automate</Header1>
      <RuleDataGrid
        ruleSetId={ruleSetId}
        rules={rules ?? emptyArray}
        onRuleChange={onRuleChange}
      />
    </StyledRoot>
  );
};

export default AutomateContainer;
