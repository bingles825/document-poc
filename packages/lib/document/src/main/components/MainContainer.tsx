import React from 'react';
import { Route, useRouteMatch } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { MainNavigation, MainNavigationProps } from '@wsh/core';
import { docClassName } from '../..';
import {
  AutomateContainer,
  DocumentsContainer,
  ProcessingCenterContainer,
} from '.';

const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexDirection: 'row',
    '& > :not(:first-child)': {
      flexGrow: 1,
    },
  },
});

const createDocumentIconData = (
  parentRouteUrl: string,
): MainNavigationProps['itemProps'] => [
  {
    icon: 'mainnavdenialsresolutioncenter',
    label: 'Processing Center',
    to: `${parentRouteUrl}`,
    exact: true,
  },
  {
    icon: 'mainnavdocuments',
    label: 'Documents',
    to: `${parentRouteUrl}/documents`,
  },
  // {
  //   icon: 'mainnavdocumentcontent',
  //   label: 'Content Manager',
  //   to: 'content-manager',
  // },
  {
    icon: 'mainnavautomate',
    label: 'Automate',
    to: `${parentRouteUrl}/automate`,
  },
  // { icon: 'mainnavteam', label: 'Team Manager', to: '/team-manager' },
];

export interface MainContainerProps {
  className?: string;
}

const MainContainer: React.FC<MainContainerProps> = ({ className }) => {
  const classes = useStyles();

  const { path, url } = useRouteMatch();
  console.log(path, url);

  const documentIconData = React.useMemo(
    () => createDocumentIconData(url),
    [url],
  );

  return (
    <div className={docClassName('main-container', classes.root, className)}>
      <MainNavigation itemProps={documentIconData} />
      <Route
        path={`${path}`}
        exact={true}
        component={ProcessingCenterContainer}
      />
      <Route path={`${path}/documents`} component={DocumentsContainer} />
      <Route path={`${path}/automate`} component={AutomateContainer} />
    </div>
  );
};

export default MainContainer;
