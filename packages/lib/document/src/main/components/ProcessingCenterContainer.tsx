import React from 'react';
import {
  colors,
  DocumentEditor,
  hintText,
  Icon,
  sortByNumericString,
  useAlert,
  useLoadData,
  useUserSession,
} from '@wsh/core';
import { makeStyles } from '@material-ui/core/styles';
import {
  Document,
  DocumentMetaEditor,
  DocumentQueue,
  DocumentStatus,
  Meta,
} from '../../document';
import {
  docClassName,
  DocumentMessage,
  useDocumentAPI,
  useMetaAPI,
} from '../..';
import { useDocumentMessageAPI } from '..';

const equalFlexColumn = {
  flexGrow: 1,
  flexBasis: 0,
  width: 0, // This prevents from forcing container to grow
} as const;

const useStyles = makeStyles({
  root: {
    display: 'flex',
  },
  noSelection: {
    ...equalFlexColumn,
    ...hintText,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    gap: 10,
  },
  documentQueue: {
    borderRight: `1px solid ${colors.border.normal}`,
    width: 240,
    flex: 'none',
  },
  documentEditor: {
    ...equalFlexColumn,
  },
  metaEditor: {
    borderLeft: `1px solid ${colors.border.normal}`,
    ...equalFlexColumn,
  },
});

export interface ProcessingCenterContainerProps {
  className?: string;
}

const ProcessingCenterContainer: React.FC<ProcessingCenterContainerProps> = ({
  className,
}) => {
  const classes = useStyles();

  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const userId = useUserSession().userId!;

  const [selectedDocument, setSelectedDocument] =
    React.useState<Document | null>(null);

  const documentId = selectedDocument?.id ?? '';

  const metaAPI = useMetaAPI();
  const documentAPI = useDocumentAPI();
  const documentMessageAPI = useDocumentMessageAPI();

  const { showAlert, Alert } = useAlert();

  const { data: config } = useLoadData(metaAPI.getMetaConfig, []);
  const { data: documentList, reload: reloadDocumentList } = useLoadData(
    documentAPI.getDocumentList,
    [],
  );

  const { data: messages, reload: reloadMessageList } = useLoadData(
    documentMessageAPI.getMessageList,
    [documentId],
    {
      pause: !documentId,
    },
  );

  const { data: meta } = useLoadData(metaAPI.getMeta, [documentId], {
    pause: !documentId,
  });

  const onSaveMeta = React.useCallback(
    async (metaList: Meta[]) => {
      await metaAPI.saveMetaList(metaList);

      showAlert('Machine learning initiated');
    },
    [metaAPI, showAlert],
  );

  const onSaveDocument = React.useCallback(
    async (document: Document) => {
      await documentAPI.saveDocument(document);
      await reloadDocumentList();

      // Document is being reassigned
      if (document.assignedToId !== selectedDocument?.assignedToId) {
        showAlert(
          `Document ${selectedDocument?.documentNumber ?? ''} reassigned${
            document.status === DocumentStatus['Needs Review']
              ? ' for review'
              : ''
          }`,
        );
        setSelectedDocument(null);
      }
      // Document has been marked as complete
      else if (document.status === DocumentStatus.Completed) {
        showAlert(
          `Document ${selectedDocument?.documentNumber ?? ''} sent to EHR`,
        );
        setSelectedDocument(null);
        // Document property change
      } else {
        showAlert('Machine learning initiated');
      }
    },
    [documentAPI, reloadDocumentList, selectedDocument, showAlert],
  );

  const onCreateDocumentMessage = React.useCallback(
    async (message: Omit<DocumentMessage, 'fromId'>) => {
      await documentMessageAPI.saveMessage({
        ...message,
        fromId: userId,
      });
      await reloadMessageList(documentId);
    },
    [documentMessageAPI, userId, reloadMessageList, documentId],
  );

  const documentListSorted = React.useMemo(() => {
    return documentList
      ? documentList
          .filter(({ status }) => status !== DocumentStatus.Completed)
          .sort(sortByNumericString('id', 'descending'))
      : null;
  }, [documentList]);

  // If user changes, clear selected document
  React.useEffect(() => {
    setSelectedDocument(null);
  }, [userId]);

  return (
    <div
      className={docClassName(
        'processing-center-container',
        classes.root,
        className,
      )}>
      {
        <DocumentQueue
          className={classes.documentQueue}
          documents={documentListSorted}
          onSelectDocument={setSelectedDocument}
          selectedDocument={selectedDocument}
          userId={userId}
        />
      }
      {selectedDocument ? (
        <>
          <DocumentEditor
            className={classes.documentEditor}
            documentPath={selectedDocument.documentPath}
          />
          {config && meta && messages ? (
            <DocumentMetaEditor
              className={classes.metaEditor}
              config={config}
              document={selectedDocument}
              meta={meta}
              messages={messages}
              onSaveDocument={onSaveDocument}
              onSaveMeta={onSaveMeta}
              onCreateDocumentMessage={onCreateDocumentMessage}
            />
          ) : (
            <div className={classes.metaEditor}></div>
          )}
        </>
      ) : (
        <div className={classes.noSelection}>
          <Icon
            icon="mainnavdenialsresolutioncenter"
            size={72}
            fillColor={colors.text.hint.color}
          />
          <span>No Item Selected</span>
        </div>
      )}

      <Alert level="success" autoHideDuration={3500} />
    </div>
  );
};

export default ProcessingCenterContainer;
