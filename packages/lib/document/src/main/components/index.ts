export { default as AutomateContainer } from './AutomateContainer';
export type { AutomateContainerProps } from './AutomateContainer';

export { default as DocumentsContainer } from './DocumentsContainer';
export type { DocumentsContainerProps } from './DocumentsContainer';

export { default as MainContainer } from './MainContainer';
export type { MainContainerProps } from './MainContainer';

export { default as ProcessingCenterContainer } from './ProcessingCenterContainer';
export type { ProcessingCenterContainerProps } from './ProcessingCenterContainer';
