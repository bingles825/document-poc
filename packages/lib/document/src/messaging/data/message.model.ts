export interface DocumentMessage {
  id?: string;
  documentId: string;
  subject: string;
  toId: string;
  fromId: string;
  content: string;
  createdAt: string;
}
