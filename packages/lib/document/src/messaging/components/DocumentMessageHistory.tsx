import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  actionLabel,
  bodyText,
  colors,
  displayISOstrAsDateTime,
  Header2,
  Icon,
  IconButton,
  useIncrementDecrementCallback,
  useLoadData,
  useUserAPI,
} from '@wsh/core';
import { docClassName, DocumentMessage } from '../..';

const useStyles = makeStyles({
  root: {
    ...colors.panel.action,
    display: 'flex',
    flexDirection: 'row',
    padding: 12,
    gap: 10,
  },
  main: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
  },
  actionLabel: {
    ...actionLabel('bold'),
  },
  header: {
    ...actionLabel('bold'),
    color: colors.text.default.color,
    display: 'flex',
    flexDirection: 'row',
    padding: '6px 0 9px',
    gap: 3,
    '& > *': {
      display: 'flex',
      flexDirection: 'column',
    },
    '& > :first-child': {
      alignItems: 'flex-end',
    },
    '& > :nth-child(2)': {
      flexGrow: 1,
    },
  },
  nav: {
    ...actionLabel('normal'),
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    gap: 6,
    color: colors.text.default.color,
    '& > :first-child': {
      ...actionLabel('bold'),
      flexGrow: 1,
    },
  },
  timeStamp: {
    ...colors.text.accent,
  },
  content: {
    ...bodyText(500),
  },
});

export interface DocumentMessageHistoryProps {
  className?: string;
  messages: DocumentMessage[];
}

const DocumentMessageHistory: React.FC<DocumentMessageHistoryProps> = ({
  className,
  messages,
}) => {
  const classes = useStyles();

  const [messageI, setMessageI] = React.useState<number>(messages.length - 1);

  const [onPrev, onNext] = useIncrementDecrementCallback(setMessageI, {
    by: 1,
    min: 0,
    max: messages.length - 1,
  });

  const selectedMessage = messages.length ? messages[messageI] : null;

  const userAPI = useUserAPI();

  const { data: fromUser } = useLoadData(
    userAPI.getUser,
    [selectedMessage?.fromId ?? ''],
    {
      pause: !selectedMessage,
    },
  );
  const { data: toUser } = useLoadData(
    userAPI.getUser,
    [selectedMessage?.toId ?? ''],
    {
      pause: !selectedMessage,
    },
  );

  return (
    <div
      className={docClassName(
        'document-message-history',
        classes.root,
        className,
      )}>
      <Icon icon="alert" fillColor="auto" size={28} />
      <div className={classes.main}>
        <span className={classes.actionLabel}>Action Required</span>
        <Header2>{selectedMessage?.subject}</Header2>
        <div className={classes.header}>
          <div>
            <span>From:</span>
            <span>To:</span>
          </div>
          <div>
            <span>{fromUser?.displayName}</span>
            <span>{toUser?.displayName}</span>
          </div>
          <div className={classes.timeStamp}>
            {displayISOstrAsDateTime(selectedMessage?.createdAt)}
          </div>
        </div>
        <div className={classes.nav}>
          <span>Comments</span>
          <span>
            {messageI + 1} of {messages.length}
          </span>
          <IconButton
            disabled={messageI <= 0}
            icon="next"
            buttonSize={24}
            size={16}
            flipH
            onClick={onPrev}
          />
          <IconButton
            disabled={messageI >= messages.length - 1}
            icon="next"
            buttonSize={24}
            size={16}
            onClick={onNext}
          />
        </div>
        <div className={classes.content}>{selectedMessage?.content}</div>
      </div>
    </div>
  );
};

export default DocumentMessageHistory;
