/**
 * Script for building manifests for .svg files and copying them to dist
 * directory.
 */

const fs = require('fs');
const { join, sep } = require('path');

const assetRootDir = join(
  __dirname,
  '..',
  'packages',
  'lib',
  'core',
  'src',
  'assets',
);

const subDirectories = ['generalPurpose', 'logo', 'mainNavigation'];

for (const subDir of subDirectories) {
  buildManifestsAndCopySVGFiles(assetRootDir, subDir);
}

/**
 * Build index.ts manifest for .svg files and copy .svg files to dist directory.
 */
function buildManifestsAndCopySVGFiles(assetRootDir, subDir) {
  const assetDir = join(assetRootDir, subDir);
  const indexPath = join(assetDir, 'index.ts');
  const content = buildIndexContent(assetDir);

  fs.writeFileSync(indexPath, content);

  copySVG(assetDir);
}

/**
 * Build content for an index.ts manifest for .svg files in a given directory
 */
function buildIndexContent(assetDir) {
  const svgFileNames = fs
    .readdirSync(assetDir)
    .filter((fileName) => /\.svg$/.test(fileName));

  const lines = svgFileNames.map((fileName) => {
    const componentName = fileName
      .replace('.svg', '')
      .replace(/([\s-_][a-zA-Z])/g, ($1) => {
        return $1.toUpperCase().replace(/[ -_]/, '');
      })
      .replace(/\s/g, '')
      .replace(/^([a-z])/, ($1) => $1.toUpperCase())
      .replace(/[\s-_]/g, '');

    return `export { default as ${componentName}Icon } from './${fileName}'`;
  });

  return lines.join('\n');
}

/**
 * Copy .svg files from given directory to corresponding dist directory
 */
function copySVG(assetDir) {
  const svgFileNames = fs
    .readdirSync(assetDir)
    .filter((fileName) => /\.svg$/.test(fileName));

  // source code is under /src/assets/xxx, target is under /dist/assets/xxx
  const targetDir = assetDir.replace(`${sep}src${sep}`, `${sep}dist${sep}`);

  fs.mkdirSync(targetDir, { recursive: true });

  for (const fileName of svgFileNames) {
    const sourceFilePath = join(assetDir, fileName);
    const targetFilePath = join(targetDir, fileName);
    fs.copyFileSync(sourceFilePath, targetFilePath);
  }
}
